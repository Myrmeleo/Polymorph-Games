package com.example.polymorphgames.alloutdefense.gameentity

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.RotateDrawable
import androidx.appcompat.content.res.AppCompatResources
import com.example.polymorphgames.R
import com.example.polymorphgames.common.CustomMath
import kotlin.math.PI


/*
The boundaries of the level. Each portal is the origin point for one of the alien types.
portals act like walls for the player and any aliens not having the same or opposite direction
Param:
    scale: multiplier for drawing dimensions
    posX: the x-coordinate (center of sprite)
    posY: the y-coordinate (remember that on android screens, up is negative, down is positive)
 */
class Portal(override val size : Int, override var posX : Double, override var posY : Double,
             var direction : Direction
             ) : GameEntity() {

    //dimensions of the sprite, not of the portal as actually displayed (see draw())
    val length = size
    val width = size
    override var rotationAngle = when(direction) {
        Direction.UP -> PI
        Direction.DOWN -> 0.0
        Direction.LEFT -> 0.5 * PI
        Direction.RIGHT -> 1.5 * PI
    }.toDouble()

    /*
    Draw this portal
     */
    override fun draw(canvas : Canvas, context : Context) {
        //make a rotatable wrapper object to contain the sprite
        val sprite = AppCompatResources.getDrawable(context, R.drawable.drawable_rotate_wrapper)!!
                as RotateDrawable
        //rotation is set through level, with a range from 0 to 10000; convert radians to level
        sprite.level = CustomMath.angleToLevel(rotationAngle)
        sprite.setBounds(0, 0, width, length)
        //each direction corresponds to a different sprite
        if (direction == Direction.UP) {
            sprite.drawable = AppCompatResources.getDrawable(context, R.drawable.aod_north_portal)!!
            //Because the game boundaries are large, we need to draw the sprite repeatedly to line up the screen
            for (i in 0..(canvas.width + sprite.bounds.width()) step sprite.bounds.height()) {
                sprite.setBounds(i, 0, i + sprite.bounds.width(), sprite.bounds.height())
                sprite.draw(canvas)
            }
        }
        else if (direction == Direction.DOWN) {
            sprite.drawable = AppCompatResources.getDrawable(context, R.drawable.aod_south_portal)!!
            for (i in 0..(canvas.width + sprite.bounds.width()) step sprite.bounds.width()) {
                sprite.setBounds(i, canvas.height - sprite.bounds.height(), i + sprite.bounds.width(), canvas.height)
                sprite.draw(canvas)
            }
        }
        else if (direction == Direction.LEFT) {
            sprite.drawable = AppCompatResources.getDrawable(context, R.drawable.aod_west_portal)!!
            for (i in 0..(canvas.height + sprite.bounds.height()) step sprite.bounds.height()) {
                sprite.setBounds(0, i - sprite.bounds.height(), sprite.bounds.width(), i)
                sprite.draw(canvas)
            }
        }
        else {
            sprite.drawable = AppCompatResources.getDrawable(context, R.drawable.aod_east_portal)!!
            for (i in 0..(canvas.height + sprite.bounds.height()) step sprite.bounds.height()) {
                sprite.setBounds(canvas.width - sprite.bounds.width(), i - sprite.bounds.height(), canvas.width, i)
                sprite.draw(canvas)
            }
        }
    }

    /*
    Communicate this portal's properties, for debugging
     */
    override fun toString() : String {
        return "Portal : ($posX $posY $direction)"
    }
}