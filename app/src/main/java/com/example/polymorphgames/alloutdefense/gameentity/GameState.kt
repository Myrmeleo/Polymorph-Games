package com.example.polymorphgames.alloutdefense.gameentity

/*
A GameModel has one of these states, to manage the actions taken in every cycle:
ONGOING: Game objects are moving; physics and animations are calculated
WIN: Game has concluded in a win
LOSE: Game has concluded in a loss
 */
enum class GameState {
    ONGOING, WIN, LOSE
}