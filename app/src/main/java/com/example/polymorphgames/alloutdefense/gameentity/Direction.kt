package com.example.polymorphgames.alloutdefense.gameentity


/*
A Direction has a different meaning depending on the type of game object it is assigned to
For aliens, it is the direction they are moving towards after spawn
For portals, it is the side of the screen they are located on
eg. An alien with Direction of UP will be moving towards the portal with Direction of UP
 */
enum class Direction {
    UP, DOWN, LEFT, RIGHT
}

