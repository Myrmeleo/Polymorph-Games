package com.example.polymorphgames.alloutdefense

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

/*
Widget with a canvas that displays all the game elements and their animations.
Param:
    context: the context of the view
    attributes: the attributes of the view
 */
class GameScreen (context: Context, attributes: AttributeSet? = null) :
    View(context, attributes) {

    //the GameController this depends on
    var controller : GameController? = null

    /*
    Detect touch
     */
    override fun onTouchEvent(ev: MotionEvent): Boolean {
        if (controller == null) return true
        val x = ev.x
        val y = ev.y
        when (ev.action) {
            MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> {
                //delegate handling of click event to controller, which has exclusive access to model
                controller!!.processTouch(x, y)
            }
        }
        return true
    }

    /*
    Draw
     */
    override fun onDraw(canvas: Canvas?) {
        if (controller == null) return
        super.onDraw(canvas)
        if (canvas == null) return
        //delegate drawing of the model to the controller, which has exclusive access to model
        controller!!.processDraw(canvas)
    }
}
