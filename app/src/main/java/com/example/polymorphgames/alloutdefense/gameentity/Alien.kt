package com.example.polymorphgames.alloutdefense.gameentity

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.RotateDrawable
import androidx.appcompat.content.res.AppCompatResources
import com.example.polymorphgames.R
import com.example.polymorphgames.common.CustomMath
import kotlin.math.PI


/*
The enemies. The player must repel these back to their origin portals.
One alien type for each direction, with an origin portal for each.
ALiens initially move towards the portal opposite of their origin portal.
Successfully making it past that portal results in a strike for the player.
Param:
    scale: multiplier for drawing dimensions
    posX: the x-coordinate (center of sprite)
    posY: the y-coordinate (remember that on android screens, up is negative, down is positive)
    velocityX: horizontal component of movement
    velocityY: vertical component of movement
    direction: Direction of the Portal the Alien is trying to go past
    state: The starting state of the Alien (see AlienState enum)
 */
class Alien(override val size : Int, override var posX : Double, override var posY : Double,
            var velocityX : Double, var velocityY : Double,
            var direction : Direction, var state: AlienState
            ) : GameEntity() {

    val radius : Int = size / 2
    val mass : Double = 1.0
    override var rotationAngle = when(direction) {
        Direction.UP -> 0
        Direction.DOWN -> PI
        Direction.LEFT -> 1.5 * PI
        Direction.RIGHT -> 0.5 * PI
    }.toDouble()

    /*
    Shift position by adding velocity components to simulate movement
     */
    fun move() {
        posX += velocityX
        posY += velocityY
    }

    /*
    calculate new angle of rotation
     */
    fun rotate() {
        rotationAngle = CustomMath.measureRadClockwiseAngle(Pair(0.0, 1.0),
            Pair(velocityX, velocityY))
    }

    /*
    Draw this alien
     */
    override fun draw(canvas : Canvas, context : Context) {
        //make a rotatable wrapper object to contain the sprite
        val sprite = AppCompatResources.getDrawable(context, R.drawable.drawable_rotate_wrapper)!!
                as RotateDrawable
        //each direction corresponds to a different sprite
        sprite.drawable = when(direction) {
            Direction.UP -> AppCompatResources.getDrawable(context, R.drawable.aod_south_alien)!!
            Direction.DOWN -> AppCompatResources.getDrawable(context, R.drawable.aod_north_alien)!!
            Direction.LEFT -> AppCompatResources.getDrawable(context, R.drawable.aod_east_alien)!!
            Direction.RIGHT -> AppCompatResources.getDrawable(context, R.drawable.aod_west_alien)!!
        }
        //each alien is highlighted underneath with the color of the portal it's initially moving to
        val highlight = when(direction) {
            Direction.UP -> AppCompatResources.getDrawable(context,
                R.drawable.aod_south_alien_highlight
            )!!
            Direction.DOWN -> AppCompatResources.getDrawable(context,
                R.drawable.aod_north_alien_highlight
            )!!
            Direction.LEFT -> AppCompatResources.getDrawable(context,
                R.drawable.aod_east_alien_highlight
            )!!
            Direction.RIGHT -> AppCompatResources.getDrawable(context,
                R.drawable.aod_west_alien_highlight
            )!!
        }
        //sprite goes on top of the highlight
        highlight.setBounds((posX - radius * 1.25).toInt(), (posY - radius * 1.25).toInt(),
            (posX + radius * 1.25).toInt(), (posY + radius * 1.25).toInt())
        highlight.draw(canvas)
        //rotation is set through level, with a range from 0 to 10000; convert radians to level
        sprite.level = CustomMath.angleToLevel(rotationAngle)
        sprite.setBounds((posX - radius).toInt(), (posY - radius).toInt(),
            (posX + radius).toInt(), (posY + radius).toInt())
        sprite.draw(canvas)
    }

    /*
    Communicate this ALien's properties, for debugging
     */
    override fun toString() : String {
        return "Alien : ($posX $posY $velocityX $velocityY $direction $state)"
    }
}

