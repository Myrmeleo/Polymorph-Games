package com.example.polymorphgames.alloutdefense.gameentity


/*
An Alien is always in one of these states:
INCOLLISION: The alien is currently colliding with something. In this state, collisions have no effect.
    Used to prevent register of the same collision event multiple processing cycles in a row
MOVING: Collisions have effects.
 */
enum class AlienState {
    INCOLLISION, MOVING
}