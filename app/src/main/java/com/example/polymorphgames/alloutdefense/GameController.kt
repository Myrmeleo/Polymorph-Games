package com.example.polymorphgames.alloutdefense

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import com.example.polymorphgames.R
import com.example.polymorphgames.alloutdefense.gameentity.GameState
import com.example.polymorphgames.common.UpdateTimer

/*
Tells the View and Activity objects what to display to the user
Param: the context
 */
class GameController (val context: Context) {

    //what we're displaying right now
    var part = PartToDisplay.INTRO

    //amount of time the game runs for, in milliseconds
    var gameLength = 180000L

    //number of times per second the timer ticks, and hence this controller updates
    val frameRate = context.resources.getInteger(R.integer.aod_draw_rate)
    val frameInterval = 1000L / frameRate
    //timer that notifies this to update on every frame
    lateinit var frameTimer: UpdateTimer<GameController>

    //the activity that displays info to the UI
    val activity: AODActivity = context as AODActivity

    //the game state data
    lateinit var gameModel: GameModel

    //view object to manipulate
    lateinit var gameDisplay : GameScreen

    //dimensions of screen (used for handling touch and restarting)
    var screenWidth : Int = 0
    var screenHeight : Int = 0

    /*
    Called when the game screen has been initialized and set to its appropriate size
    Here is where we do the lazy init of the model
    Initializing model before this will lead to inaccurate recordings of View dimensions
    Game is not visible before this point, so start timer now
    Param:
        w: the width
        h: the height
        window: the object that passed the dimensions
     */
    fun prepareModel(w : Int, h : Int) {
        screenWidth = w
        screenHeight = h
        gameModel = GameModel(w.toDouble(), h.toDouble(), gameLength, context)
        frameTimer = UpdateTimer(frameInterval, this, {tickUpdate()},
            context.resources.getInteger(R.integer.aod_timer_pool_size))
    }

    //tell GameView to update the UI widgets to display the scores and time
    fun notifyUI() {
        activity.updateTimeText(gameModel.gameLength)
        activity.updatePointsText(gameModel.points)
        activity.updateStrikesText(gameModel.strikes)
    }

    /*
    Execute functions upon each tick of the timer
     */
    fun tickUpdate() {
        if (part == PartToDisplay.GAME) {
            //animate game
            gameDisplay.invalidate()
            //update UI
            notifyUI()
            //change screen to display if the game is over
            if (gameModel.gameState == GameState.WIN) {
                cleanup()
                part = PartToDisplay.WIN
                activity.switchVisibleUI()
            } else if (gameModel.gameState == GameState.LOSE) {
                cleanup()
                part = PartToDisplay.LOSE
                activity.switchVisibleUI()
            }
        }
    }

    /*
    handle a touch event passed from a View
    Param: touch coordinates
     */
    fun processTouch(x : Float, y : Float) {
        //leave intro and start game
        if (part == PartToDisplay.INTRO) {
            part = PartToDisplay.GAME
            activity.switchVisibleUI()
        }
        //move the player
        else if (part == PartToDisplay.GAME) gameModel.trackCursor(x, y)
        //retry or quit - depends on touch coordinates
        else if (part == PartToDisplay.LOSE) {
            if (x <= screenWidth / 2) {
                activity.restart()
            } else {
                activity.exit()
            }
        } else {
            activity.exit()
        }
    }

    /*
    handle a draw request passed from a View
    Param: the canvas to draw on
     */
    fun processDraw(canvas: Canvas) {
        if (part == PartToDisplay.GAME) gameModel.draw(canvas, context)
        else if (part == PartToDisplay.LOSE || part == PartToDisplay.WIN) canvas.drawColor(Color.TRANSPARENT)
    }

    /*
    cleanup when the game is being exited
     */
    fun cleanup() {
        frameTimer.shutdown()
        gameModel.cleanup()
    }

    /*
    return the stats
     */
    fun getGameStats() : Pair<Int, Int> {
        return Pair(gameModel.points, gameModel.strikes)
    }

    /*
    reset this controller for game restart
     */
    fun restart() {
        cleanup()
        part = PartToDisplay.INTRO
        frameTimer = UpdateTimer(frameInterval, this, {tickUpdate()},
            context.resources.getInteger(R.integer.aod_timer_pool_size))
    }
}
