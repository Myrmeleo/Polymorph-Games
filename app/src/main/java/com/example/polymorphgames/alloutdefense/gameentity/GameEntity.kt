package com.example.polymorphgames.alloutdefense.gameentity

import android.content.Context
import android.graphics.Canvas


/*
Any object in the game that can interact with another
 */
abstract class GameEntity {
    //a dimension of measurement for the entity's size in pixels; exact meaning varies by subclass
    abstract val size : Int
    //components of position on screen
    abstract var posX : Double
    abstract var posY : Double
    //angle of rotation for the entity in radians - 0 means it points up
    abstract var rotationAngle : Double
    abstract fun draw(canvas : Canvas, context : Context)
}