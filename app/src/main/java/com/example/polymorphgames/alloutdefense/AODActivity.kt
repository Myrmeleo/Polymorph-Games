package com.example.polymorphgames.alloutdefense

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
//import com.example.polymorphgames.AllOutState
import com.example.polymorphgames.R
import com.google.android.gms.tasks.Task
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase


/*
Activity for the game
Controls what the player sees in UI elements related to gameplay
 */
class AODActivity : AppCompatActivity() {

    //the object that tells this what data to display
    lateinit var controller: GameController
    //the view widget that displays the game objects that comprise the gameplay
    lateinit var gameDisplay : GameScreen
    
    //individual layout components
    //displays intro
    lateinit var introText : TextView
    //displays lose screen
    lateinit var loseText : TextView
    //displays win screen
    lateinit var winText : TextView
    //contains the TextViews below
    lateinit var infoBar : LinearLayout
    //displays remaining time
    lateinit var timerText : TextView
    //displays accumulated points as tracked by the GameModel
    lateinit var pointsText : TextView
    //displays accumulated strikes as tracked by the GameModel
    lateinit var strikesText : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.aod_game_screen)
        //Grab Intent Content
//        var bundledInfo = getIntent().getExtras()
//        var allOutState: AllOutState
//        if(bundledInfo != null) {
//            allOutState = bundledInfo . getSerializable ("extra_object") as AllOutState
//        }
        //perform late init
        controller = GameController(this)
        gameDisplay = findViewById(R.id.gameScreen)
        introText = findViewById(R.id.introText)
        loseText = findViewById(R.id.loseText)
        winText = findViewById(R.id.winText)
        infoBar = findViewById(R.id.gameInfo)
        timerText = findViewById(R.id.timerText)
        pointsText = findViewById(R.id.pointsText)
        strikesText = findViewById(R.id.strikesText)
        //set view properties
        gameDisplay.controller = controller
        controller.gameDisplay = gameDisplay
        switchVisibleUI()
    }

    /*
    called whenever the app is being exited or switched out
     */
    override fun onPause() {
        super.onPause()
        controller.cleanup()
    }

    /*
    Show the appropriate UI components as determined by the controller
     */
    fun switchVisibleUI() {
        when (controller.part) {
            PartToDisplay.INTRO -> {
                infoBar.visibility = View.INVISIBLE
                introText.visibility = View.VISIBLE
                winText.visibility = View.INVISIBLE
                loseText.visibility = View.INVISIBLE
            }
            PartToDisplay.GAME -> {
                //reset game state
                controller.prepareModel(gameDisplay.measuredWidth, gameDisplay.measuredHeight)
                infoBar.visibility = View.VISIBLE
                introText.visibility = View.INVISIBLE
            }
            PartToDisplay.WIN -> {
                gameDisplay.invalidate()
                infoBar.visibility = View.INVISIBLE
                winText.visibility = View.VISIBLE
            }
            PartToDisplay.LOSE -> {
                gameDisplay.invalidate()
                infoBar.visibility = View.INVISIBLE
                loseText.visibility = View.VISIBLE
            }
        }
    }

    /*
    Display remaining in-game time until game is over
    Param: remaining time in milliseconds
     */
    fun updateTimeText(remaining : Long) {
        var seconds = remaining / 1000
        val minutes = (seconds / 60).toString()
        seconds %= 60
        val secondsFormatted = seconds.toString().padStart(2, '0')
        timerText.setText("$minutes:$secondsFormatted")
    }

    /*
    Display number of points
    Param: point count
     */
    fun updatePointsText(value : Int) {
        pointsText.setText("$value pts")
    }

    /*
    Display number of strikes
    Param: strike count
     */
    fun updateStrikesText(value : Int) {
        strikesText.setText("$value strikes")
    }

    /*
    Restart the game
     */
    fun restart() {
        controller.cleanup()
        val finalStats = controller.getGameStats()
        val finalScore = finalStats.first
        val finalStrikes = finalStats.second

        // Call to Firebase Cloud Function to Add Score
        setAllOutScoreByUser(finalScore)

        //remake the controller
        controller.restart()
        switchVisibleUI()
    }

    /*
    Exit the game
     */
    fun exit() {
        controller.cleanup()
        val finalStats = controller.getGameStats()
        val finalScore = finalStats.first
        val finalStrikes = finalStats.second

        // Call to Firebase Cloud Function to Add Score
        setAllOutScoreByUser(finalScore)

        // Finish Activity
        this.finish()
    }

    // Taken from ScoresViewModel in User Interface
    private fun setAllOutScoreByUser(score: Int): Task<String> {
        val data = hashMapOf(
            "game" to "allOutDefence",
            "score" to score,
            "push" to true
        )
        return Firebase.functions
            .getHttpsCallable("updateScore")
            .call(data)
            .continueWith { task ->
                // This continuation runs on either success or failure, but if the task
                // has failed then result will throw an Exception which will be
                // propagated down.
                val result = task.result?.data as String
                result
            }
    }
}
