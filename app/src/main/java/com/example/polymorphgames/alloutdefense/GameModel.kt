package com.example.polymorphgames.alloutdefense

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.RotateDrawable
import androidx.appcompat.content.res.AppCompatResources
import com.example.polymorphgames.R
import com.example.polymorphgames.alloutdefense.gameentity.*
import com.example.polymorphgames.common.CustomMath
import com.example.polymorphgames.common.UpdateTimer
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.math.*


/*
Records the gamestate of the game. Maintains info on all game objects, and creates/deletes them.
Updates game object properties regularly.
Param:
    viewSizeX: width of the gameView on which this model is drawn
    viewSizeY: height of the gameView on which this model is drawn
    gameLength: duration of level (milliseconds)
    sharedDataLock: mutex for synchronous operation on data with other threads
    context: Context of the gameView representing this
 */
class GameModel(val viewSizeX : Double, val viewSizeY : Double,
                var gameLength : Long, context : Context) {

    //number of times per second the timer ticks, and hence this model updates
    val cycleRate = context.resources.getInteger(R.integer.aod_processing_rate)
    //a cycle is the time between ticks
    val cycleInterval : Long = 1000L / cycleRate

    //the timer that ticks with each frame, calling on this model to update
    var gameTimer : UpdateTimer<GameModel>

    /*
    calculate how to scale the sprites so that they take up an equal proportion of the gameView
        on all devices
     */
    val scaledSpriteSize = (min(viewSizeX, viewSizeY) / context.resources.getInteger(
        R.integer.aod_sprite_to_view_shortest_dim_ratio)
            ).toInt()

    //the game objects
    var player : Player = Player(scaledSpriteSize,viewSizeX / 2, viewSizeY / 2, 0.0, 0.0)
    //thread-safe collections for drawing and modifying
    var aliens : CopyOnWriteArrayList<Alien> = CopyOnWriteArrayList()
    var portals : CopyOnWriteArrayList<Portal> = CopyOnWriteArrayList()
    /*
    The background will be drawn by way of a 2D matrix of tiles. A tile is a pair of int and double
    The int is a reference to a drawable, the double is the rotation angle of the sprite
    Outer list layer is rows, inner layer is columns. [i, j] is the tile in the ith row and jth column.
     */
    var backTiles : List<List<Pair<Int, Double>>>

    //variables used in the alien spawning algorithm.
    //max alien count on screen
    val totalAlienLimit = 5
    //max count of aliens originating from a top or bottom portal
    val upDownAlienLimit = if (viewSizeX > viewSizeY) 2 else 3
    //max number of cycles (timer ticks) the model can go without a spawn (guaranteed spawn if exceeded)
    var maxAlienSpawnDelay = 150
    val alienSpawnChance = 0.015f
    var cyclesSinceLastAlienSpawn = maxAlienSpawnDelay
    //count of aliens originating from a top or bottom portal
    //there is no numLeftRightALiens - that value is implied by this value and length of aliens list
    var numUpDownAliens = 0

    //coordinates of the last known touch event
    var cursorX : Double = 0.0
    var cursorY : Double = 0.0

    /*
    Player's score
    Points awarded for redirecting aliens back to their origin portals
    Strikes given for allowing an alien to pass the portal opposite of its origin
     */
    var points = 0
    var strikes = 0
    val strikeLimit = 10

    var gameState = GameState.ONGOING

    init {
        //make the portals for each side of the screen
        //coordinates assigned -1 are irrelevant and never used for calculations
        portals.add(Portal((scaledSpriteSize * 1.25).toInt(), -1.0, 0.0, Direction.UP))
        portals.add(Portal((scaledSpriteSize * 1.25).toInt(),-1.0, viewSizeY, Direction.DOWN))
        portals.add(Portal((scaledSpriteSize * 1.25).toInt(),0.0, -1.0, Direction.LEFT))
        portals.add(Portal((scaledSpriteSize * 1.25).toInt(),viewSizeX, -1.0, Direction.RIGHT))
        gameTimer = UpdateTimer(cycleInterval, this, { tickUpdate() },
            context.resources.getInteger(R.integer.aod_timer_pool_size))
        backTiles = BackgroundFactory.operate(this)
    }

    /*
    Execute functions upon each tick of the timer
    Param: remaining time in milliseconds
     */
    fun tickUpdate() {
        //decrement the time and end the game once it hits 0
        gameLength = max(0L, gameLength - cycleInterval)
        if (gameLength <= 0L) endGame()
        raiseDifficulty(gameLength)
        processCollisions()
        moveObjects()
        spawnObjects()
        acceleratePlayerMovement(cursorX, cursorY)
    }

    /*
    make the game harder by tweaking variables
    Param: remaining time in milliseconds
     */
    fun raiseDifficulty(remainingTime : Long) {
        //lower max time between spawns
        //currently, lowest spawn delay is 60 cycles
        maxAlienSpawnDelay = (150 - (gameLength - remainingTime) / 2000).toInt()
    }

    //move all game objects
    fun moveObjects() {
        player.move()
        for (p in aliens) {
            p.move()
        }
    }

    //check for collisions between every pair of objects in the game, and make them react
    fun processCollisions() {
        //check each alien; descending order to allow deletion from list while iterating through list
        for (i in aliens.size-1 downTo 0) {
            val a = aliens[i]
            /*
            check if the alien has collided with anything so far; update as we go through
            if false at the end of the iteration,
                an alien with a state of INCOLLISION will be changed to MOVING
            if true at the end of the iteration,
                an alien with a state of INCOLLISION will not be changed to MOVING
             */
            var colliding = false
            //check player
            if (SpriteCollision.collides(a, player)) {
                colliding = true
                //An alien that is INCOLLISION will not cause any effects
                if (a.state == AlienState.INCOLLISION) continue
                SpriteCollision.bounce(a, player)
            }
            //check the other aliens
            for (j in i-1 downTo 0) {
                val b = aliens[j]
                if (SpriteCollision.collides(a, b)) {
                    colliding = true
                    if (a.state == AlienState.INCOLLISION || b.state == AlienState.INCOLLISION) continue
                    SpriteCollision.bounce(a, b)
                }
            }
            //check the portals
            for (w in portals) {
                if (SpriteCollision.collides(a, w)) {
                    colliding = true
                    if (a.state == AlienState.INCOLLISION) continue
                    SpriteCollision.bounce(a, w)
                }
                else if (SpriteCollision.passesThrough(a, w)) {
                    colliding = true
                    if (a.state == AlienState.INCOLLISION) continue
                    //update scores; the value to be updated depends on where the alien went
                    if (a.direction == w.direction) {
                        strikes = min(strikes + 1, strikeLimit)
                        //end game if it goes too high
                        if (strikes >= strikeLimit) {
                            endGame()
                        }
                    }
                    else {
                        points += 1
                    }
                    //remove the alien from the list, since it's no longer in the game
                    when (a.direction) {
                        Direction.UP, Direction.DOWN -> numUpDownAliens -= 1
                        else -> {}
                    }
                    aliens.remove(a)
                }
            }
            //an INCOLLISION alien is marked as MOVING when it's not colliding with anything
            if (!colliding && a.state == AlienState.INCOLLISION) {
                a.state = AlienState.MOVING
            }
            //mark so that the same collision won't cause bounces again next cycle
            else if (colliding) {
                a.state = AlienState.INCOLLISION
            }
        }
        //check player collision with each portal
        for (w in portals) {
            if (SpriteCollision.collides(player, w)) {
                SpriteCollision.slide(player, w)
            }
        }
    }

    /*
    Spawn in new aliens at random times
     */
    fun spawnObjects() {
        if (aliens.size >= totalAlienLimit) return
        //Get random value between 0 and 1, and it must be no more than the spawn chance
        val spawnRoll = Math.random()
        //the roll and cyclesSinceLastSpawn determine if we spawn
        if (spawnRoll <= alienSpawnChance || cyclesSinceLastAlienSpawn >= maxAlienSpawnDelay) {
            val newAlien = AlienFactory.operate(this)
            aliens.add(newAlien)
            if (newAlien.direction == Direction.UP || newAlien.direction == Direction.DOWN) {
                numUpDownAliens += 1
            }
            cyclesSinceLastAlienSpawn = 0
        }
        else {
            cyclesSinceLastAlienSpawn += 1
        }
    }

    /*
    Move the player object towards the coordinates
    Movement speed scales with distance from the coords
     */
    fun acceleratePlayerMovement(targetX : Double, targetY : Double) {
        val accelX = abs(targetX - player.posX).pow(1.1) * sign(targetX - player.posX) / 5
        val accelY = abs(targetY - player.posY).pow(1.1) * sign(targetY - player.posY) / 5
        player.velocityX = accelX
        player.velocityY = accelY
        player.rotate()
    }

    /*
    Record the position of the cursor the player put on the screen
     */
    fun trackCursor(x : Float, y : Float) {
        cursorX = x.toDouble()
        cursorY = y.toDouble()
    }

    /*
    draw the game
     */
    fun draw(canvas : Canvas, context : Context) {
        //draw the (back)ground
        val sprite = AppCompatResources.getDrawable(context, R.drawable.drawable_rotate_wrapper)!!
                as RotateDrawable
        for (row in backTiles.indices) {
            for (col in backTiles[0].indices) {
                sprite.drawable = AppCompatResources.getDrawable(context, backTiles[row][col].first)
                sprite.level = CustomMath.angleToLevel(backTiles[row][col].second)
                sprite.setBounds(col * scaledSpriteSize, row * scaledSpriteSize,
                    (col + 1) * scaledSpriteSize, (row + 1) * scaledSpriteSize)
                sprite.draw(canvas)
            }
        }
        //draw game objects
        for (w in portals) {
            w.draw(canvas, context)
        }
        for (a in aliens) {
            a.draw(canvas, context)
        }
        player.draw(canvas, context)
    }

    /*
    end the game
    change the gamestate to the apprpriate value based on state variables
     */
    fun endGame() {
        //stop processing
        gameTimer.shutdown()
        //conditions for winning
        if (strikes < strikeLimit) {
            gameState = GameState.WIN
        } else {
            gameState = GameState.LOSE
        }
    }

    /*
    clean up on end of game
     */
    fun cleanup() {
        gameTimer.shutdown()
    }
}