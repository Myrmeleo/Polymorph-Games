package com.example.polymorphgames.alloutdefense.gameentity

import com.example.polymorphgames.alloutdefense.GameModel
import kotlin.math.cos
import kotlin.math.sin


/*
Implementation of AbstractFactory class for making the Alien game object
 */
class AlienFactory private constructor() {
    companion object {
        fun operate(model: GameModel): Alien {
            return createRandomAlien(model)
        }

        /*
        create and return an alien with random properties
        Param: a model whose properties are used to determine alien properties
         */
        private fun createRandomAlien(model: GameModel): Alien {
            //properties of the new alien
            val posX: Double
            val posY: Double
            val velocity: Double
            val velocityX: Double
            val velocityY: Double
            val direction: Direction
            //decide if the alien will be going up/dpwn or left/right
            val directionRoll = Math.random() * (model.totalAlienLimit - model.aliens.size)
            //an alien's path of travel has a 0.5PI angle of variance
            //  ranges 45 degrees left or right of the line perpendicular to its origin portal
            val angle = Math.random() * Math.PI / 2 - Math.PI / 4
            if (directionRoll < model.upDownAlienLimit - model.numUpDownAliens) {
                //starting x coordinate is between 20% and 80% of screen width
                posX = ((Math.random() * 3 + 1) * model.viewSizeX / 5)
                //starting speed is between 25% and 50% of screen height per second
                velocity = ((Math.random() * 2 + 2) * model.viewSizeY / 8) / 60
                //50-50 shot of originating from top or bottom
                val upDownRoll = Math.random()
                if (upDownRoll < 0.5f) {
                    direction = Direction.UP
                    posY = model.viewSizeY + model.scaledSpriteSize
                    velocityX = velocity * sin(angle)
                    velocityY = 0.0 - velocity * cos(angle)
                } else {
                    direction = Direction.DOWN
                    posY = 0.0 - model.scaledSpriteSize
                    velocityX = velocity * sin(angle)
                    velocityY = velocity * cos(angle)
                }
                //same as the above, but for left/right
            } else {
                posY = ((Math.random() * 3 + 1) * model.viewSizeY / 5)
                velocity = ((Math.random() * 2 + 2) * model.viewSizeX / 8) / 60
                val leftRightRoll = Math.random()
                if (leftRightRoll < 0.5f) {
                    direction = Direction.LEFT
                    posX = model.viewSizeX + model.scaledSpriteSize
                    velocityX = 0.0 - velocity * cos(angle)
                    velocityY = velocity * sin(angle)
                } else {
                    direction = Direction.RIGHT
                    posX = 0.0 - model.scaledSpriteSize
                    velocityX = velocity * cos(angle)
                    velocityY = velocity * sin(angle)
                }
            }
            /*
            A new alien will be assigned a state of INCOLLISION, because it spawns behind its
                origin portal, and we don't want it to be immediately marked as RETURNED
             */
            val state: AlienState = AlienState.INCOLLISION
            return Alien(model.scaledSpriteSize, posX, posY, velocityX, velocityY, direction, state)
        }
    }
}