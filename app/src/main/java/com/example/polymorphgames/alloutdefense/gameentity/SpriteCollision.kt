package com.example.polymorphgames.alloutdefense.gameentity

import com.example.polymorphgames.common.CustomMath
import kotlin.math.abs


/*
Handles polygons between 2 game objects
Calculates and sets new movement velocities on the objects using object properties
 */
class SpriteCollision {
    companion object {

        /*
        Upon collision of alien with the player, calculate and set resulting velocity of this alien
        Player does not move
         */
        fun bounce(a : Alien, b : Player) {
            //fetch post collision velocity
            val (vx1_prime, vy1_prime, vx2_prime, vy2_prime) =
                CustomMath.getElasticVelocitiesPostCollision(a.mass, a.posX, a.posY, a.velocityX, a.velocityY,
                    b.mass, b.posX, b.posY, b.velocityX, b.velocityY)
            //set component values using trigonometry; player velocity is not set because the player is unmoved by collisions
            a.velocityX = vx1_prime
            a.velocityY = vy1_prime
            a.rotate()
        }

        /*
        Upon collision of alien with another alien, calculate and set resulting velocities of both
        */
        fun bounce(a : Alien, b : Alien) {
            val (vx1_prime, vy1_prime, vx2_prime, vy2_prime) =
                CustomMath.getElasticVelocitiesPostCollision(a.mass, a.posX, a.posY, a.velocityX, a.velocityY,
                    b.mass, b.posX, b.posY, b.velocityX, b.velocityY)
            a.velocityX = vx1_prime
            a.velocityY = vy1_prime
            a.rotate()
            b.velocityX = vx2_prime
            b.velocityY = vy2_prime
            b.rotate()
        }

        /*
        Upon collision of alien with a portal; reflect the movement path
        Angle of incidence is angle of reflection
        */
        fun bounce(a : Alien, b : Portal) {
            when (b.direction) {
                Direction.UP -> a.velocityY = abs(a.velocityY)
                Direction.DOWN -> a.velocityY = 0 - abs(a.velocityY)
                Direction.LEFT -> a.velocityX = abs(a.velocityX)
                Direction.RIGHT -> a.velocityX = 0 - abs(a.velocityX)
            }
            a.rotate()
        }

        /*
        Determine if an alien has come into contact with a player
         */
        fun collides(a : Alien, b : Player) : Boolean {
            val distance = CustomMath.magnitude(Pair((a.posX - b.posX), (a.posY - b.posY)))
            return distance <= a.radius + b.radius
        }

        /*
        Determine if an alien has come into contact with another alien
         */
        fun collides(a : Alien, b : Alien) : Boolean {
            val distance = CustomMath.magnitude(Pair((a.posX - b.posX), (a.posY - b.posY)))
            return distance <= a.radius + b.radius
        }

        /*
        Determine if an alien has come into contact with a portal
         */
        fun collides(a : Alien, b : Portal) : Boolean {
            //alien can only collide with the portals to its left and right of the one it came from
            //  not the one it came from or the one opposite of it
            return when (a.direction) {
                Direction.UP, Direction.DOWN -> (b.direction == Direction.LEFT && a.posX - a.radius <= b.posX) ||
                        (b.direction == Direction.RIGHT && a.posX + a.radius >= b.posX)
                Direction.LEFT, Direction.RIGHT -> (b.direction == Direction.UP && a.posY - a.radius <= b.posY) ||
                        (b.direction == Direction.DOWN && a.posY + a.radius >= b.posY)
            }
        }

        /*
        Determine if a player has come into contact with a portal
         */
        fun collides(a : Player, b : Portal): Boolean {
            return when (b.direction) {
                Direction.UP -> a.posY - a.radius <= b.posY
                Direction.DOWN -> a.posY + a.radius >= b.posY
                Direction.LEFT -> a.posX - a.radius <= b.posX
                Direction.RIGHT -> a.posX + a.radius >= b.posX
            }
        }


        /*
        Determine if an alien has moved past a portal (either the one it was aiming for, or the one it came from)
         */
        fun passesThrough(a : Alien, b : Portal) : Boolean {
            //check if this is the portal it was aiming for
            if (a.direction == b.direction) {
                return when (a.direction) {
                    Direction.UP -> a.posY + a.radius <= b.posY
                    Direction.DOWN -> a.posY - a.radius >= b.posY
                    Direction.LEFT -> a.posX + a.radius <= b.posX
                    Direction.RIGHT -> a.posX - a.radius >= b.posX
                }
            }
            //check if this is the portal it came from
            else
            {
                return when (a.direction) {
                    Direction.UP -> b.direction == Direction.DOWN && a.posY - a.radius >= b.posY
                    Direction.DOWN -> b.direction == Direction.UP && a.posY + a.radius <= b.posY
                    Direction.LEFT -> b.direction == Direction.RIGHT && a.posX - a.radius >= b.posX
                    Direction.RIGHT -> b.direction == Direction.LEFT && a.posX + a.radius <= b.posX
                }
            }
        }

        /*
        When a player is in contact with a portal, do not allow it to move through
        However, it can still move parallel to the portal (hence 'sliding')
         */
        fun slide(a : Player, b : Portal) {
            if (b.direction == Direction.UP) {
                a.velocityY = Math.max(a.velocityY, 0.0)
            }
            else if (b.direction == Direction.DOWN) {
                a.velocityY = Math.min(a.velocityY, 0.0)
            }
            else if (b.direction == Direction.LEFT) {
                a.velocityX = Math.max(a.velocityX, 0.0)
            }
            else {
                a.velocityX = Math.min(a.velocityX, 0.0)
            }
        }
    }
}