package com.example.polymorphgames.alloutdefense.gameentity

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.RotateDrawable
import androidx.appcompat.content.res.AppCompatResources
import com.example.polymorphgames.R
import com.example.polymorphgames.common.CustomMath


/*
Controlled by the player of the game.
The player must collide with aliens to send them back towards their origin portals
Param:
    scale: multiplier for drawing dimensions
    posX: the x-coordinate (center of sprite)
    posY: the y-coordinate (remember that on android screens, up is negative, down is positive)
    velocityX: horizontal component of movement
    velocityY: vertical component of movement
 */
class Player(override val size : Int, override var posX : Double, override var posY : Double,
             var velocityX : Double, var velocityY : Double,
             ) : GameEntity() {

    val radius : Int = size / 2
    val mass : Double = 2.0
    override var rotationAngle = 0.0

    /*
    Shift position by adding velocity components to simulate movement
     */
    fun move() {
        posX += velocityX
        posY += velocityY
    }

    /*
    calculate new angle of rotation
     */
    fun rotate() {
        rotationAngle = CustomMath.measureRadClockwiseAngle(Pair(0.0, 1.0),
            Pair(velocityX, velocityY))
    }

    /*
    Draw this player
     */
    override fun draw(canvas : Canvas, context : Context) {
        //make a rotatable wrapper object to contain the sprite
        val sprite = AppCompatResources.getDrawable(context, R.drawable.drawable_rotate_wrapper)!!
                as RotateDrawable
        sprite.drawable = AppCompatResources.getDrawable(context, R.drawable.aod_game_player)!!
        val highlight = AppCompatResources.getDrawable(context, R.drawable.aod_player_highlight)!!
        //sprite goes on top of the highlight
        highlight.setBounds((posX - radius * 1.25).toInt(), (posY - radius * 1.25).toInt(),
            (posX + radius * 1.25).toInt(), (posY + radius * 1.25).toInt())
        highlight.draw(canvas)
        //rotation is set through level, with a range from 0 to 10000; convert radians to level
        sprite.level = CustomMath.angleToLevel(rotationAngle)
        sprite.setBounds((posX - radius).toInt(), (posY - radius).toInt(),
            (posX + radius).toInt(), (posY + radius).toInt())
        sprite.draw(canvas)
    }

    /*
    Communicate this player's properties, for debugging
     */
    override fun toString(): String {
        return "Player : ($posX $posY $velocityX $velocityY)"
    }
}

