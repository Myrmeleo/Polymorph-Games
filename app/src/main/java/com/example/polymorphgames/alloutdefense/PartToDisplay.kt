package com.example.polymorphgames.alloutdefense

/*
Track what we're displaying on the screen - the intro screens, the game, or the outro screens
    INTRO: plot and instructions
    GAME: gameplay
    GAMEOVER: game over screen
    WIN: win screen
    EXIT: don't display anything, leave the game
 */
enum class PartToDisplay {
    INTRO, GAME, LOSE, WIN
}