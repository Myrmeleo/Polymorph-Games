package com.example.polymorphgames.alloutdefense.gameentity

import com.example.polymorphgames.R
import com.example.polymorphgames.alloutdefense.GameModel
import kotlin.math.PI
import kotlin.math.ceil


/*
Implementation of AbstractFactory class for making the background for the game
 */
class BackgroundFactory private constructor() {
    companion object {
        fun operate(model: GameModel): List<List<Pair<Int, Double>>> {
            return createRandomBackground(model)
        }

        /*
            Create and return a 2D matrix of tiles. A tile is a pair of int and double
            The int is a reference to a drawable, the double is the rotation angle of the sprite
            Outer list layer is rows, inner layer is columns. [i, j] is the tile in the ith row and jth column.
            Param: the model to hold the background - properties used to determine matrix dimensions
         */
        private fun createRandomBackground(model: GameModel): List<List<Pair<Int, Double>>> {
            val matrix : ArrayList<ArrayList<Pair<Int, Double>>> = ArrayList()
            //make the background matrix
            for (row in 0 until ceil(model.viewSizeY / model.scaledSpriteSize).toInt()) {
                matrix.add(ArrayList())
                for (col in 0 until ceil(model.viewSizeX / model.scaledSpriteSize).toInt()) {
                    //pick a sprite with random chance
                    val spriteRef : Int
                    val spriteRoll = Math.random()
                    if (spriteRoll <= 0.3) {
                        spriteRef = R.drawable.aod_grass_tile_1
                    } else if (spriteRoll <= 0.6) {
                        spriteRef = R.drawable.aod_grass_tile_2
                    } else if (spriteRoll <= 0.9) {
                        spriteRef = R.drawable.aod_grass_tile_3
                    } else {
                        spriteRef = R.drawable.aod_dirt_tile
                    }
                    //pick an angle of rotation with random chance
                    val angle : Double
                    val angleRoll = Math.random()
                    if (angleRoll <= 0.25) {
                        angle = 0.0
                    } else if (spriteRoll <= 0.25) {
                        angle = 0.5 * PI
                    } else if (spriteRoll <= 0.25) {
                        angle = PI
                    } else {
                        angle = 1.5 * PI
                    }
                    matrix[row].add(Pair(spriteRef, angle))
                }
            }
            return matrix
        }
    }
}