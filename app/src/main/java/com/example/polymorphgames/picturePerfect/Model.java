package com.example.polymorphgames.picturePerfect;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.polymorphgames.R;

public class Model {

    private Bitmap drawing;

    private int gridWidth;
    private int gridHeight;
    private boolean[][] outlineGrid;

    private int brushRadius = 20;

    private int[] palette;
    private int curColour;

    private int lastX = 0;
    private int lastY = 0;
    private boolean wasDrawingPrev = false;

    private DrawingStrategy curDrawingStrategy;
    private DrawingStrategy[] brushes = {new CircleLineDrawingStrategy(),
            new SquareLineDrawingStrategy()};
    private int curDrawingStrategyIndex = 0;

    public Model(int width, int height, int[] palette, Bitmap drawing,
                 boolean[][] outlineGrid){
        this.gridWidth = width;
        this.gridHeight = height;
        this.palette = palette;
        this.curColour = palette[0];
        this.curDrawingStrategy = brushes[curDrawingStrategyIndex];
        this.drawing = drawing;
        this.outlineGrid = new boolean[gridWidth][gridHeight];
        for (int y = 0; y < this.gridHeight; ++y) {
            for (int x = 0; x < this.gridWidth; ++x) {
                this.outlineGrid[x][y] = outlineGrid[x][y];
            }
        }
    }

    // Used to determine if the current brushstroke should continue from the previous
    // coordinate or if it should start a new brushstroke/line.
    public void setWasDrawingPrev(boolean flag) {
        wasDrawingPrev = flag;
    }

    // Sets the current colour to use.
    public void setCurColour(int id){
        curColour = palette[id];
    }
    // Swaps the current brushstroke shape.
    public void swapBrush(){
        curDrawingStrategyIndex = curDrawingStrategyIndex+1 == brushes.length ? 0 :
                curDrawingStrategyIndex+1;
        curDrawingStrategy = brushes[curDrawingStrategyIndex];
    }

    public void setPixel(int x, int y){
        drawing.setPixel(x, y, Color.BLACK);
    }

    public void setLastX(int x){
        lastX = x;
    }

    public void setLastY(int y){
        lastY = y;
    }

    //--------------------------------Getter Methods-----------------------------------

    // Sends the icon of the current brush
    public Bitmap getBrushIcon(){
        return curDrawingStrategy.getBrushBmp();
    }

    public int getBrushRadius(){
        return brushRadius;
    }

    public boolean getWasDrawingPrev() {
        return wasDrawingPrev;
    }

    // Gets the current colour to use.
    public int getCurColour(){
        return curColour;
    }

    public Bitmap getDrawing(){
        return drawing;
    }

    public DrawingStrategy getCurDrawingStrategy(){
        return curDrawingStrategy;
    }

    public int getLastX(){
        return lastX;
    }

    public int getLastY(){
        return lastY;
    }
    public int getGridWidth(){
        return gridWidth;
    }

    public int getGridHeight(){
        return gridHeight;
    }

    public boolean getCoordinate(int x, int y){
        return outlineGrid[x][y];
    }
}