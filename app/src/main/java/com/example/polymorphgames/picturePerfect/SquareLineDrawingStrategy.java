package com.example.polymorphgames.picturePerfect;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class SquareLineDrawingStrategy implements DrawingStrategy {

    // Draw a line with Squares at each end (helps fill in jagged
    // corners)
    @Override
    public boolean drawLine(Canvas drawingCanvas, Paint brush,
                            int lastX, int lastY, int nextX, int nextY,
                            int brushRadius, boolean wasDrawingPrev) {

        // If the user's finger was on the screen previously, draw a line from the prev touch
        // coordinates to the current touch coordinates.
        if (wasDrawingPrev) {
            drawingCanvas.drawRect(lastX-brushRadius,
                    lastY-brushRadius, lastX+brushRadius,
                    lastY+brushRadius, brush);
            drawingCanvas.drawLine(lastX, lastY, nextX, nextY, brush);
        }
        wasDrawingPrev = true;
        drawingCanvas.drawRect(nextX-brushRadius,
                nextY-brushRadius, nextX+brushRadius,
                nextY+brushRadius, brush);
        return wasDrawingPrev;
    }

    // The brush icon should simply be a black square.
    @Override
    public Bitmap getBrushBmp() {
        Bitmap bmp = Bitmap.createBitmap(bmpWidth, bmpHeight, Bitmap.Config.ARGB_8888);
        Canvas drawingCanvas = new Canvas(bmp);
        Paint brush = new Paint();
        brush.setColor(Color.BLACK);
        drawingCanvas.drawRect(bmpWidth/4, bmpHeight/4, 3*bmpWidth/4,
                3*bmpHeight/4, brush);
        return bmp;
    }
}
