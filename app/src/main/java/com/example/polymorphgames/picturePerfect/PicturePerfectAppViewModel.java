package com.example.polymorphgames.picturePerfect;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Pair;

import androidx.databinding.BaseObservable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class PicturePerfectAppViewModel extends BaseObservable {

    private Model model;

    //Stores actions for view.
    private MutableLiveData<Command> mCommand = new MutableLiveData<>();

    // Used to send commands to the View.
    public LiveData<Command> getCommand() {
        return mCommand;
    }

    // constructor of ViewModel class. Ensures the model class is populated correctly.
    public PicturePerfectAppViewModel
    (int width, int height, Bitmap bmp, int[] palette) {

        /* Scans every pixel of the blank drawing to determine the pixels representing
           the black outline. Any colour that is gray or darker is counted as part of
           the outline and is added to a 2D array the size of the drawing canvas.
           Conversely, the bitmap is sharpened to only black/white by filling in
           these specific pixels with black (instead of a shade of grey) and ensures
           every other colour is white.
         */

        Bitmap drawing = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        boolean[][] outlineGrid = new boolean[width][height];
        if (bmp != null) {
            for (int y = 0; y < height; ++y) {
                for (int x = 0; x < width; ++x) {
                    if (bmp.getPixel(x, y) <= Color.GRAY) {
                        outlineGrid[x][y] = true;
                        drawing.setPixel(x, y, Color.rgb(0, 0, 0));
                    } else {
                        outlineGrid[x][y] = false;
                        drawing.setPixel(x, y, Color.rgb(255, 255, 255));
                    }
                }
            }
        }

        model = new Model(width, height, palette, drawing, outlineGrid);
    }

    //------------------------Methods interacting with the Model:-------------------------

    public void draw(int touchX, int touchY) {
        Canvas drawingCanvas = new Canvas(model.getDrawing());
        Paint brush = new Paint();
        brush.setColor(model.getCurColour());
        brush.setStrokeWidth(model.getBrushRadius()*2);

        // Handles drawing the brush stroke, regardless of the brush shape
        boolean wasDrawingPrev = model.getCurDrawingStrategy().drawLine(
                drawingCanvas,
                brush,
                model.getLastX(),
                model.getLastY(),
                touchX,
                touchY,
                model.getBrushRadius(),
                model.getWasDrawingPrev());

        model.setWasDrawingPrev(wasDrawingPrev);

        // Ensures the black outline stays intact
        int leftBound  = Math.max(Math.min(model.getLastX(), touchX)-model.getBrushRadius(), 0);
        int rightBound = Math.min(Math.max(model.getLastX(), touchX)+model.getBrushRadius(),
                model.getGridWidth());
        int upperBound = Math.max(Math.min(model.getLastY(), touchY)-model.getBrushRadius(), 0);
        int lowerBound = Math.min(Math.max(model.getLastY(), touchY)+model.getBrushRadius(),
                model.getGridHeight());

        // After every brushstroke, the integrity of the original outline must be
        // maintained. The 2D array of the outline is traversed, updating the picture
        // with the outline wherever appropriate.
        for (int y = upperBound; y < lowerBound; ++y){
            for (int x = leftBound; x < rightBound; ++x){
                if (model.getCoordinate(x, y)){
                    model.setPixel(x, y);
                }
            }
        }

        model.setLastX(touchX);
        model.setLastY(touchY);
        sendDrawing(model.getDrawing());
    }

    public void setWasDrawingPrev(boolean flag) {
        model.setWasDrawingPrev(flag);
    }

    public void onBrushSwapButtonClicked(){
        model.swapBrush();
        sendBrushIcon(getBrushIcon());
    }

    public Bitmap getBrushIcon(){
        return model.getBrushIcon();
    }

    public void onPaletteButtonClicked(int id) {
        model.setCurColour(id);
        sendColour(model.getCurColour());
    }

    //---------------------Methods interacting with the ViewModel:------------------------

    public void sendDrawing(Bitmap bmp){
        mCommand.setValue(new Command(Command.UPDATE_DRAWING, bmp));
    }

    public void sendColour(int col) {
        mCommand.setValue(new Command(Command.UPDATE_COLOUR, col));
    }

    public void sendBrushIcon(Bitmap bmp){
        mCommand.setValue(new Command(Command.UPDATE_BRUSH_ICON, bmp));
    }

}