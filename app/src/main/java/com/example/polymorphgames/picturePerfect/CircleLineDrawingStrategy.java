package com.example.polymorphgames.picturePerfect;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class CircleLineDrawingStrategy implements DrawingStrategy {

    // Draw a line with circles at each end (the standard brushtype,
    // suitable for most tasks).
    @Override
    public boolean drawLine(Canvas drawingCanvas, Paint brush,
                         int lastX, int lastY, int nextX, int nextY,
                         int brushRadius, boolean wasDrawingPrev) {

        // If the user's finger was on the screen previously, draw a line from the prev touch
        // coordinates to the current touch coordinates.
        if (wasDrawingPrev) {
            drawingCanvas.drawCircle(lastX, lastY, brushRadius, brush);
            drawingCanvas.drawLine(lastX, lastY, nextX, nextY, brush);
        }
        wasDrawingPrev = true;
        drawingCanvas.drawCircle(nextX, nextY, brushRadius, brush);
        return wasDrawingPrev;
    }

    // The brush icon should simply be a black circle.
    @Override
    public Bitmap getBrushBmp() {
        Bitmap bmp = Bitmap.createBitmap(bmpWidth, bmpHeight, Bitmap.Config.ARGB_8888);
        Canvas drawingCanvas = new Canvas(bmp);
        Paint brush = new Paint();
        brush.setColor(Color.BLACK);
        drawingCanvas.drawCircle(bmpWidth/2, bmpHeight/2,
                bmpWidth/3, brush);
        return bmp;
    }
}
