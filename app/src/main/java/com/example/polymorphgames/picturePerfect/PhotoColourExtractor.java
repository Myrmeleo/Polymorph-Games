package com.example.polymorphgames.picturePerfect;

import static com.example.polymorphgames.picturePerfect.MainDrawingActivity.COLOUR_ARRAY_BUNDLE_ID;
import static com.example.polymorphgames.picturePerfect.MainDrawingActivity.GRID_HEIGHT;
import static com.example.polymorphgames.picturePerfect.MainDrawingActivity.GRID_WIDTH;
import static com.example.polymorphgames.picturePerfect.StartScreenActivity.COLOURED_IMAGES;
import static com.example.polymorphgames.picturePerfect.StartScreenActivity.IMAGE_INDEX_CODE;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import com.example.polymorphgames.R;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;

public class PhotoColourExtractor extends AppCompatActivity {

    private ListenableFuture<ProcessCameraProvider> cameraProviderFuture;
    private static final int CAMERA_REQUEST = 100;
    private View.OnTouchListener mTouchListener;

    private int curColourIndex = 0;

    private TextView timerView;
    private TextView redoButton;
    private TextView confirmButton;
    private TextView cameraShutterButton;
    private ImageView takenPic;
    private ImageView targetImage;
    private PreviewView preview;
    private TextView header;
    private Bitmap curPreview;
    private int curCol;
    private int totalTimerSeconds = 60;
    private int imgIndex = 0;
    private CountDownTimer timer;
    private boolean timerDone = false;

    private int[] customPalette = {0, 0, 0, 0};
    private int[] paletteIds = {R.id.col0, R.id.col1, R.id.col2, R.id.col3};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_camera_preview);

        // Get the index of the random drawing chosen at the start
        Bundle bundledImgIndex = getIntent().getExtras();
        if(bundledImgIndex != null) {
            imgIndex = (int) bundledImgIndex.getSerializable(IMAGE_INDEX_CODE);
        }

        timerView = findViewById(R.id.timer);
        redoButton = findViewById(R.id.redoButton);
        confirmButton = findViewById(R.id.confirmButton);
        cameraShutterButton = findViewById(R.id.cameraShutterButton);
        takenPic = findViewById(R.id.imgBox);
        targetImage = findViewById(R.id.targetImg);
        preview = findViewById(R.id.previewBox);
        header = findViewById(R.id.headerTextView);

        // Request for camera use if it was previously denied for this app
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this,
                    new String[] {Manifest.permission.CAMERA}, CAMERA_REQUEST);
        }

        // Bind the camera preview to the appropriate PreviewView in the current content view
        cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                bindPreview(cameraProvider);
            } catch (ExecutionException | InterruptedException e) {
                // No errors need to be handled for this Future.
                // This should never be reached.
            }
        }, ContextCompat.getMainExecutor(this));

        // Create a timer that ends the activity and moves on to the main drawing phase when
        // depleted.
        timer = new CountDownTimer(totalTimerSeconds*1000, 1000) {
            public void onTick(long millisUntilFinished) {
                timerView.setText(""+millisUntilFinished/1000);
            }

            public void onFinish() {
                timerView.setText("Time's Up!");
                timerDone = true;
                startDrawingActivity();
            }
        }.start();

        // Target image the user must eventually colour is displayed at the bottom right
        // of the screen
        targetImage.setImageDrawable(getDrawable(COLOURED_IMAGES[imgIndex]));

        // Set the preview window to be the standard grid width and height so that the
        // design is uniform across the game.
        ViewGroup.LayoutParams params = preview.getLayoutParams();
        params.width = GRID_WIDTH;
        params.height = GRID_HEIGHT;
        preview.setLayoutParams(params);

        // Once a picture is taken, take the colour of the pixel of the picture that is
        // currently being touched by the user and set the colour accordingly in the
        // specific colour box in the palette.
        mTouchListener = new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int x = (int) event.getX();
                int y = (int) event.getY();

                if (x < GRID_WIDTH && x >= 0 && y < GRID_HEIGHT && y >= 0){
                    curCol = curPreview.getPixel(x, y);
                    TextView b = findViewById(paletteIds[curColourIndex]);
                    b.setBackgroundColor(curCol);
                }

                return true;
            }
        };

        // The taken pic and the preview take up the same space, but they are not both
        // displayed at the same time. Only one or the other should be displayed at a time.
        takenPic.setOnTouchListener(mTouchListener);
        params = takenPic.getLayoutParams();
        params.width = GRID_WIDTH;
        params.height = GRID_HEIGHT;
        takenPic.setLayoutParams(params);

        // Once the user wants to take a picture, automatically update the current colour
        // palette box to be the same colour as the pixel in the middle of the picture.
        // Also, swap the preview and takenPic views.
        cameraShutterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closePreview();
                curCol = curPreview.getPixel(GRID_WIDTH/2, GRID_HEIGHT/2);
                TextView b = findViewById(paletteIds[curColourIndex]);
                b.setBackgroundColor(curCol);
            }
        });

        // Used to redo the last taken pic if the user is unhappy with it. Swap the takenPic
        // and preview views.
        redoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView b = findViewById(paletteIds[curColourIndex]);
                b.setBackgroundColor(Color.WHITE);
                openPreview();
            }
        });

        // Used to add the selected colour to the final palette. If the user has reached 4
        // colours, the main drawing phase is started. Otherwise, swap the takenPic and
        // preview views.
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customPalette[curColourIndex] = curCol;
                curColourIndex++;
                if (curColourIndex == 4 && !timerDone){
                    timer.cancel();
                    startDrawingActivity();
                }
                openPreview();
            }
        });

    }

    // Since the PreviewView and the Imageview containing the most recent picture take up
    // the same spot, they must swap visibilities whenever appropriate. These 2 functions
    // help do that cleanly.
    void openPreview(){
        takenPic.setVisibility(View.GONE);
        preview.setVisibility(View.VISIBLE);
        redoButton.setVisibility(View.GONE);
        confirmButton.setVisibility(View.GONE);
        cameraShutterButton.setVisibility(View.VISIBLE);
        header.setText(R.string.startSnapping);
    }

    void closePreview(){
        redoButton.setVisibility(View.VISIBLE);
        confirmButton.setVisibility(View.VISIBLE);
        cameraShutterButton.setVisibility(View.GONE);
        preview.setVisibility(View.GONE);
        takenPic.setVisibility(View.VISIBLE);
        curPreview = preview.getBitmap();
        takenPic.setImageBitmap(preview.getBitmap());
        header.setText(R.string.extractColours);
    }

    // Helps bind the camera preview to the appropriate PreviewView.
    void bindPreview(@NonNull ProcessCameraProvider cameraProvider) {
        Preview preview = new Preview.Builder()
                .build();

        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();

        PreviewView pv = findViewById(R.id.previewBox);

        preview.setSurfaceProvider(pv.getSurfaceProvider());

        Camera camera = cameraProvider.bindToLifecycle((LifecycleOwner)this, cameraSelector, preview);
    }

    // Starts the main drawing phase with the custom palette created by the user from this
    // phase.
    void startDrawingActivity(){
        Intent myIntent = new Intent(confirmButton.getContext(), MainDrawingActivity.class);
        Bundle b = new Bundle();
        b.putSerializable(COLOUR_ARRAY_BUNDLE_ID, customPalette);
        b.putSerializable(IMAGE_INDEX_CODE, imgIndex);
        myIntent.putExtras(b);
        startActivity(myIntent);
        this.finish();
    }

    // Closes the activity if back is pressed, since this indicates that the user wishes
    // to quit.
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        timer.cancel();
        this.finish();
    }
}
