package com.example.polymorphgames.picturePerfect;

/* Drawings retrieved from:
        https://www.shutterstock.com/image-vector/printable-coloring-page-robot-face-pattern-1926779261
        http://clipart-library.com/clipart/19-kc8nMkEcr.htm
        https://www.coloringme.com/wp-content/uploads/2016/07/Coloring-Pages-of-Robots.jpg
        http://clipart-library.com/clipart/19-pi58e7Bi9.htm
        https://easydrawingguides.com/how-to-draw-a-mecha/
        https://robotswithcoffee.com/2022/03/01/robot-handbag/

 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.polymorphgames.R;

import java.util.Random;

public class StartScreenActivity extends AppCompatActivity{

    public static final int[] BLANK_IMAGES = {
            R.drawable.mecha0,
            R.drawable.mecha1,
            R.drawable.mecha2,
            R.drawable.mecha3,
            R.drawable.mecha4,
            R.drawable.mecha5
    };

    public static final int[] COLOURED_IMAGES = {
            R.drawable.mecha_coloured0,
            R.drawable.mecha_coloured1,
            R.drawable.mecha_coloured2,
            R.drawable.mecha_coloured3,
            R.drawable.mecha_coloured4,
            R.drawable.mecha_coloured5
    };

    public static final String IMAGE_INDEX_CODE = "5";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_start_screen);

        // Start a new game using the classic colours
        Button classicStartButton = findViewById(R.id.classicPaletteStartButton);
        classicStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), StoryActivity.class);
                Bundle b = new Bundle();
                b.putSerializable(StoryActivity.CHOICE, StoryActivity.CLASSIC);
                myIntent.putExtras(b);
                startActivity(myIntent);
            }
        });

        // Start a new game using the camera to take pictures to fill the colour palette.
        Button cameraStartButton = findViewById(R.id.cameraStartButton);
        cameraStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), StoryActivity.class);
                Bundle b = new Bundle();
                b.putSerializable(StoryActivity.CHOICE, StoryActivity.CUSTOM);
                myIntent.putExtras(b);
                startActivity(myIntent);
            }
        });
    }

    // Closes the activity if back is pressed, since this indicates that the user wishes
    // to quit.
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}