package com.example.polymorphgames.picturePerfect;

import static com.example.polymorphgames.picturePerfect.StartScreenActivity.IMAGE_INDEX_CODE;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.polymorphgames.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;
import com.google.firebase.ktx.Firebase;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class FinalScoreActivity extends AppCompatActivity{

    public static final String SCORE_CODE = "score";
    private float score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_score);

        Bundle bundleWithScore = getIntent().getExtras();
        if(bundleWithScore != null) {
            score = (float) bundleWithScore.getSerializable(SCORE_CODE);
        }

        // If they can match up to 85% of the colours, we consider that a perfect score.
        score /= 0.85;
        setPerfectScoreByUser(Math.round(score));
        TextView scoreText = findViewById(R.id.scoreText);
        scoreText.setText("Congratulations! Your final score is "+Math.round(score)+"%");

        // Returns the user to the game menu, where they can start a new game or head back
        // to the main game launcher menu.
        Button menuButton = findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
    // Taken from ScoresViewModel in User Interface
    private Task<String> setPerfectScoreByUser(int score)
    {
        Map<String, Object> data = new HashMap<>();
        data.put("game", "picturePerfect");
        data.put("score", score);
        data.put("push", true);

        return FirebaseFunctions.getInstance()
                .getHttpsCallable("updateScore")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        String result = (String) task.getResult().getData();
                        return result;
                    }
                });
    }
}