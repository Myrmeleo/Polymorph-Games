package com.example.polymorphgames.picturePerfect;

import static com.example.polymorphgames.picturePerfect.FinalScoreActivity.SCORE_CODE;
import static com.example.polymorphgames.picturePerfect.StartScreenActivity.BLANK_IMAGES;
import static com.example.polymorphgames.picturePerfect.StartScreenActivity.COLOURED_IMAGES;
import static com.example.polymorphgames.picturePerfect.StartScreenActivity.IMAGE_INDEX_CODE;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.example.polymorphgames.R;
import com.example.polymorphgames.databinding.ActivityMainDrawingBinding;

public class MainDrawingActivity extends AppCompatActivity {

    private View.OnTouchListener mTouchListener;
    private PicturePerfectAppViewModel mViewModel;
    private TextView timerView;
    private CountDownTimer timer;
    private boolean timerDone = false;
    private ImageView imgBox;
    private Bitmap curBmp;
    private int totalTimerSeconds = 60;
    private int imgIndex = 0;
    public static final int GRID_WIDTH = 1000;
    public static final int GRID_HEIGHT = 1200;
    public static final String COLOUR_ARRAY_BUNDLE_ID = "colours";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Palette of colours and their corresponding views
        int[] palette = {Color.rgb(222,24,100),
                Color.rgb(76,186,175),
                Color.rgb(255,255,255),
                Color.rgb(114,139,255),
                Color.rgb(192,105,204)};
        int[] paletteIds = {R.id.col0, R.id.col1, R.id.col2, R.id.col3, R.id.col4};

        // If the user chose the camera route, extract colours from the intent bundle
        Bundle bundledInfo = getIntent().getExtras();
        if(bundledInfo != null) {
            int[] customPalette = (int[]) bundledInfo.getSerializable(COLOUR_ARRAY_BUNDLE_ID);
            int customPaletteIndex = 0;
            imgIndex = (int) bundledInfo.getSerializable(IMAGE_INDEX_CODE);
            if (customPalette != null) {
                for (int i = 0; i < palette.length; ++i) {

                    // This space is occupied by the white/eraser colour by default due to
                    // aesthetic purposes (it looks cleaner to have the eraser right above
                    // the changeBrush button).
                    if (i == 2) {
                        continue;
                    }

                    // This will overwrite the classic palette with as many colours as the user
                    // was able to capture in the camera activity, if any. If the user was not able
                    // to capture 4 colours, the rest of the colours are not changed from the
                    // classic palette.
                    if (customPalette[customPaletteIndex] != 0) {
                        palette[i] = customPalette[customPaletteIndex];
                        customPaletteIndex++;
                    }
                }
            }
        }

        // Scale the blank drawing to the correct scale to fit on to the main imageBox view.
        BitmapFactory.Options bmpOptions = new BitmapFactory.Options();
        bmpOptions.outWidth = GRID_WIDTH;
        bmpOptions.outHeight = GRID_HEIGHT;
        bmpOptions.inScaled = false;
        Bitmap origBmp = BitmapFactory.decodeResource(getResources(), BLANK_IMAGES[imgIndex],
                bmpOptions);
        origBmp = Bitmap.createScaledBitmap(origBmp, GRID_WIDTH, GRID_HEIGHT, false);
        curBmp = origBmp;

        // Attach the ViewModel
        ActivityMainDrawingBinding activityMainDrawingBinding =
                DataBindingUtil.setContentView (this, R.layout.activity_main_drawing);
        PicturePerfectAppViewModel mViewModel =
                new PicturePerfectAppViewModel(GRID_WIDTH, GRID_HEIGHT, origBmp, palette);
        activityMainDrawingBinding.setViewModel(mViewModel);
        activityMainDrawingBinding.executePendingBindings();

        // Handles any commands sent by the ViewModel
        mViewModel.getCommand().observe(this, new Observer<Command>() {
            @Override
            public void onChanged(@Nullable final Command cmd) {
                if(cmd != null){
                    handleCommand(cmd);
                }
            }
        });

        mTouchListener = new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int x = (int) event.getX();
                int y = (int) event.getY();

                // Sends any touch coordinates to the ViewModel so that it can decide what
                // action to perform.
                if (x < GRID_WIDTH && x >= 0 && y < GRID_HEIGHT && y >= 0){
                    mViewModel.draw(x, y);
                }

                // When the user takes their finger off the canvas, the ViewModel is notified.
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Log.i("TAG", "touched down");
                        break;
                    case MotionEvent.ACTION_MOVE:
                        Log.i("TAG", "moving: (" + x + ", " + y + ")");
                        break;
                    case MotionEvent.ACTION_UP:
                        Log.i("TAG", "touched up");
                        mViewModel.setWasDrawingPrev(false);
                        break;
                }

                return true;
            }
        };

        // Sets the time limit, at which point the user will be shown their score.
        timerView = findViewById(R.id.timer);
        timer = new CountDownTimer(totalTimerSeconds*1000, 1000) {

            public void onTick(long millisUntilFinished) {
                timerView.setText(""+millisUntilFinished/1000);
            }

            public void onFinish() {
                timerDone = true;
                startScoreActivity();
                timerView.setText("Done");
            }
        }.start();

        // Set the blank image as the starting image on the canvas
        imgBox = findViewById(R.id.imgBox);
        imgBox.setOnTouchListener(mTouchListener);
        ViewGroup.LayoutParams params = imgBox.getLayoutParams();
        params.width = GRID_WIDTH;
        params.height = GRID_HEIGHT;
        imgBox.setLayoutParams(params);
        imgBox.setImageBitmap(origBmp);

        // Set the target coloured image in the bottom right corner
        ImageView targetImage = findViewById(R.id.targetImg);
        targetImage.setImageDrawable(getDrawable(COLOURED_IMAGES[imgIndex]));

        // Set the colours of the palette accordingly
        for (int i = 0; i < paletteIds.length; ++i){
            Button b = findViewById(paletteIds[i]);
            b.setBackgroundColor(palette[i]);
        }

        // Set the brushSwap button accordingly
        ImageButton brushSwap = findViewById(R.id.brushSwap);
        brushSwap.setImageBitmap(mViewModel.getBrushIcon());

        // Set the current colour box accordingly
        LinearLayout curColBox = findViewById(R.id.curColourBox);
        curColBox.setBackgroundColor(palette[0]);

        // If the user wishes to end the activity early, they can do so using the Done button
        TextView done = findViewById(R.id.doneButton);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!timerDone) {
                    timer.cancel();
                    startScoreActivity();
                }
            }
        });
    }

    // Updates the view components appropriately based on the command sent by the ViewModel.
    private void handleCommand(@NonNull final Command cmd) {
        switch (cmd.getCommandValue()){
            case Command.UPDATE_DRAWING:
                ImageView iv = findViewById(R.id.imgBox);
                curBmp = cmd.getBitmap();
                iv.setImageBitmap(curBmp);
                break;
            case Command.UPDATE_COLOUR:
                LinearLayout curColBox = findViewById(R.id.curColourBox);
                curColBox.setBackgroundColor(cmd.getColour());
                break;
            case Command.UPDATE_BRUSH_ICON:
                ImageView brushSwap = findViewById(R.id.brushSwap);
                brushSwap.setImageBitmap(cmd.getBitmap());
                break;
        }
    }

    // Calculate the score and start the last phase, displaying the user's score
    private void startScoreActivity(){

        // Create a scaled version of the target coloured drawing to compare against
        BitmapFactory.Options bmpOptions = new BitmapFactory.Options();
        bmpOptions.outWidth = GRID_WIDTH;
        bmpOptions.outHeight = GRID_HEIGHT;
        bmpOptions.inScaled = false;
        Bitmap targetBmp = BitmapFactory.decodeResource(getResources(), COLOURED_IMAGES[imgIndex],
                bmpOptions);
        targetBmp = Bitmap.createScaledBitmap(targetBmp, GRID_WIDTH, GRID_HEIGHT, false);

        // Compare the colour of a pixel in the user's drawing with the target coloured drawing to
        // see the difference. With a max difference of 765 (see getColDiff()), it seems fair to
        // award the point to the player if they get to within 1/6th of the way there. So, if the
        // difference between colours is less than 128, give them the point for that pixel. This is
        // done for every pixel in the final drawing that is not black or white. In other words,
        // The user is not judged for anything outside of what's filled in between the black
        // outlines.
        float score = 0;
        int maxScore = 0;
        for (int x = 0; x < GRID_WIDTH; ++x){
            for (int y = 0; y < GRID_HEIGHT; ++y){
                if (targetBmp.getPixel(x, y) != Color.WHITE && targetBmp.getPixel(x, y) != Color.BLACK) {
                    maxScore++;
                    int diff = getColDiff(targetBmp.getPixel(x, y), curBmp.getPixel(x, y));
                    if (diff < 128) {
                        score++;
                    }
                }
            }
        }

        score = score/maxScore*100;

        // Send the score and start the score display activity
        Intent myIntent = new Intent(imgBox.getContext(), FinalScoreActivity.class);
        Bundle b = new Bundle();
        b.putSerializable(SCORE_CODE, score);
        myIntent.putExtras(b);
        startActivity(myIntent);
        this.finish();
    }

    // Get the total difference between two colours. The total difference in this case is the sum
    // of the difference between the R, G, and B values of 2 colours. This would mean that the
    // max difference would be between black and white, which would be 255+255+255 = 765
    private int getColDiff(int a, int b){
        int redDiff = Math.abs(Color.red(a) - Color.red(b));
        int blueDiff = Math.abs(Color.blue(a) - Color.blue(b));
        int greenDiff = Math.abs(Color.green(a) - Color.green(b));
        return redDiff+blueDiff+greenDiff;
    }

    // Closes the activity if the user presses the back button, indicating that the user wishes
    // to quit the current game.
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        timer.cancel();
        this.finish();
    }
}