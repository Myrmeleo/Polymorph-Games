package com.example.polymorphgames.picturePerfect;

import static com.example.polymorphgames.picturePerfect.StartScreenActivity.IMAGE_INDEX_CODE;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.polymorphgames.R;

import java.util.Random;

public class StoryActivity extends AppCompatActivity{

    public static final String CHOICE = "choice";
    public static final int CLASSIC = 0;
    public static final int CUSTOM = 1;

    private String[] stories = {
            "You've just transported most of the schematics for the Mechas, however some were " +
                    "destroyed after an encounter with an alien scout. Fortunately, you managed " +
                    "to make backups, but their colours became corrupted. Using the colours you " +
                    "have on hand, reconstruct the schematics to the best of your abilities!",
            "You've just transported most of the schematics for the Mechas, however some were " +
                    "destroyed after an encounter with an alien scout. Fortunately, you managed " +
                    "to make backups, but their colours became corrupted. Even worse, you only " +
                    "have the colours in your surrounding environment to use in the drawing. Go " +
                    "around and take 4 pictures containing the colours you'll need to use. " +
                    "Select a pixel with the colour you want from each photo you take and then " +
                    "confirm. Make sure to pay attention to the colours you will need in the " +
                    "final drawing and to build a suitable colour palette!"};

    private int choice = 0;
    private int imgIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_story);

        // Pick a random drawing to test the user with
        imgIndex = new Random().nextInt(StartScreenActivity.BLANK_IMAGES.length);

        // Get the choice the player made in the previous screen (classic or camera/custom palette).
        Bundle bundleWithChoice = getIntent().getExtras();
        if(bundleWithChoice != null) {
            choice = (int) bundleWithChoice.getSerializable(CHOICE);
        }

        // Set appropriate story
        TextView story = findViewById(R.id.story);
        story.setText(stories[choice]);

        // Start the appropriate game path depending on the player's choice (classic mode or
        // camera/custom palette mode).
        Button startButton = findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (choice == CLASSIC) {
                    Intent myIntent = new Intent(view.getContext(), MainDrawingActivity.class);
                    Bundle b = new Bundle();
                    b.putSerializable(IMAGE_INDEX_CODE, imgIndex);
                    myIntent.putExtras(b);
                    startActivity(myIntent);
                }
                else if (choice == CUSTOM){
                    Intent myIntent = new Intent(view.getContext(), PhotoColourExtractor.class);
                    Bundle b = new Bundle();
                    b.putSerializable(IMAGE_INDEX_CODE, imgIndex);
                    myIntent.putExtras(b);
                    startActivity(myIntent);
                }
            }
        });
    }
}