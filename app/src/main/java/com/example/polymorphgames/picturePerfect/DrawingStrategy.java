package com.example.polymorphgames.picturePerfect;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

// Incorporates the strategy pattern to incorporate various brushtypes.
public interface DrawingStrategy{
    // Dimensions of the brush icon
    int bmpWidth = 150;
    int bmpHeight = 150;

    // Method to draw the appropriate line
    boolean drawLine(Canvas drawingCanvas, Paint brush,
                         int lastX, int lastY, int nextX, int nextY,
                         int size, boolean wasDrawingPrev);
    // Method to retrieve the appropriate icon for the brushtype
    Bitmap getBrushBmp();
}
