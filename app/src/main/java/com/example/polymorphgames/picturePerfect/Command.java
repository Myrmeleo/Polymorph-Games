package com.example.polymorphgames.picturePerfect;

import android.graphics.Bitmap;

// Used by the ViewModel to send commands to the View
public class Command {
        public static final int UPDATE_DRAWING = 1;
        public static final int UPDATE_COLOUR = 2;
        public static final int UPDATE_BRUSH_ICON = 3;
        private final int mCommand;
        private Bitmap mBmp;
        private int mColour;

        public Command(int command, Bitmap bmp) {
            mCommand = command;
            mBmp = bmp;
        }

        public Command(int command, int col) {
            mCommand = command;
            mColour = col;
        }

        public int getCommandValue() {
            return mCommand;
        }

        public Bitmap getBitmap() {
            return mBmp;
        }

        public int getColour() {
            return mColour;
        }

    }
