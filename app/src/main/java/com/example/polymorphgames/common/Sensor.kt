package com.example.polymorphgames.common

import android.content.Context
import android.content.Context.SENSOR_SERVICE
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.lifecycle.MutableLiveData

data class SensorData(
    var type: Int = 0,
    var x: Float = 0f,
    var y: Float = 0f,
    var z: Float = 0f
)

class SensorEvent : SensorEventListener{
    private object SensorEventConstants{
        const val ACCURACY: Float = 100f // keep 2 decimal points
    }

    private var mutable: MutableLiveData<SensorData?>?
    private var data: SensorData?
    private val sensorType: Int

    // Assigns data to either mutable or data depending on how
    // the object was constructed
    private val assign: (mutable: MutableLiveData<SensorData?>?,
                         data: SensorData?,
                         event: SensorEvent,
                         type: Int)->Unit

    constructor(mutable: MutableLiveData<SensorData?>, sensorType: Int){
        this.mutable = mutable
        this.data = null
        this.sensorType = sensorType

        assign = { mutable: MutableLiveData<SensorData?>?,
                   data: SensorData?,
                   event: SensorEvent,
                   type: Int ->
            mutable!!.postValue(
                SensorData(
                type,
                roundFloat(event.values[0]),
                roundFloat(event.values[1]),
                roundFloat(event.values[2])
            )
            )
        }
    }

    constructor(data: SensorData, sensorType: Int){
        this.mutable = null
        this.data = data
        this.sensorType = sensorType

        assign = { mutable: MutableLiveData<SensorData?>?,
                   data: SensorData?,
                   event: SensorEvent,
                   type: Int ->
            data!!.type = type
            data.x = roundFloat(event.values[0])
            data.y = roundFloat(event.values[1])
            data.z = roundFloat(event.values[2])
        }
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val type = event?.sensor?.type

        if(type == sensorType){
            assign(mutable, data, event, type)
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    private fun roundFloat(num: Float): Float{
        return (num * SensorEventConstants.ACCURACY).toInt() / SensorEventConstants.ACCURACY
    }
}

class SensorDataManager private constructor(){
    companion object {
        fun register(context: Context, sensorType: Int, data: MutableLiveData<SensorData?>) {
            val manager = context.getSystemService(SENSOR_SERVICE) as SensorManager
            val sensor = manager.getDefaultSensor(sensorType)
            val event = SensorEvent(data, sensorType)

            manager.registerListener(event, sensor, SensorManager.SENSOR_DELAY_UI)
        }

        // I make no promises regarding race conditions, use at your own risk
        fun register(context: Context, sensorType: Int, data: SensorData) {
            val manager = context.getSystemService(SENSOR_SERVICE) as SensorManager
            val sensor = manager.getDefaultSensor(sensorType)
            val event = SensorEvent(data, sensorType)

            manager.registerListener(event, sensor, SensorManager.SENSOR_DELAY_UI)
        }
    }
}