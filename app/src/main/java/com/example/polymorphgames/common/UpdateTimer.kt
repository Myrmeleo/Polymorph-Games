package com.example.polymorphgames.common

import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.TimeUnit

/*
Timer that will periodically call a function on some generic Observer object
The tick function that is periodically called belongs to the Observer
Param:
    period: time between ticks (in milliseconds)
    ob: object to be called on once per period
    tick: function to be called on ob
    corePoolSize: fixed number of threads in pool
 */
class UpdateTimer <Ob>(period : Long, val ob : Ob, val tick: Ob.() -> Unit,
                       corePoolSize : Int) : ScheduledThreadPoolExecutor(corePoolSize) {

    init {
        scheduleAtFixedRate({ ob.tick() }, 0L, period, TimeUnit.MILLISECONDS)
    }
}