package com.example.polymorphgames.common

import kotlin.math.PI
import kotlin.math.atan2
import kotlin.math.pow
import kotlin.math.sqrt

class CustomMath private constructor(){
    companion object{
        fun dot(a: Pair<Float,  Float>, b: Pair<Float, Float>): Float{
            return a.first * b.first + a.second * b.second
        }

        fun projectUnitOffset(a: Pair<Float, Float>, b: Pair<Float, Float>, offset: Pair<Float, Float>): Pair<Float, Float>{
            val dp = dot(a = Pair(first = offset.first + a.first, second = offset.second + a.second), b = b)
            return Pair<Float, Float>(first = dp * b.first, second = dp * b.second)
        }

        fun unitNorm(a: Pair<Float, Float>, b: Pair<Float, Float>): Pair<Float, Float>{
            val norm = Pair<Float, Float>(
                -(b.second - a.second),
                b.first - a.first
            )

            return normalize(norm)
        }

        /*
        Convert an angle in radians to a level (as defined in the Drawable class)
        The level of a RotateDrawable dictates its angle of rotation. Range is [0, 10000]
        Level 10000 is equal to angle of 2pi (which is equal to angle of 0)
         */
        fun angleToLevel(radang: Double) : Int {
            val result = ((radang % (2 * PI) * 10000 / (2 * PI)).toInt())
            //A level of 0 for some reason results in a 180 rotation - so make it 10000
            return when (result) {
                0 -> 10000
                else -> result
            }
        }

        fun dot(a: Pair<Double,  Double>, b: Pair<Double, Double>) : Double{
            return a.first * b.first + a.second * b.second
        }

        /*
        Calculate the determinant of 2 vectors - the determinant of a matrix
            where the vectors are columns
        Param: the vectors
        Return: the determinant
         */
        fun det(a: Pair<Float,  Float>, b: Pair<Float, Float>) : Float {
            return a.first * b.second - a.second * b.first
        }

        fun det(a: Pair<Double,  Double>, b: Pair<Double, Double>) : Double {
            return a.first * b.second - a.second * b.first
        }

        /*
        Calculate the resulting trajectories and velocities of 2 bodies in a perfectly elastic collision
        Param: mass, position components (x and y), and velocity components of objects 1 and 3 respectively
        Return: velocity components of the objects after collision
         */
        fun getElasticVelocitiesPostCollision(m1 : Double, px1 : Double, py1 : Double, vx1 : Double,
                                              vy1 : Double, m2 : Double, px2 : Double, py2 : Double,
                                              vx2 : Double, vy2 : Double) : Array<Double> {

            val pos1_min_pos2 = subtract(Pair(px1, py1), Pair(px2, py2))
            val pos2_min_pos1 = subtract(Pair(px2, py2), Pair(px1, py1))
            val vel1_min_vel2 = subtract(Pair(vx1, vy1), Pair(vx2, vy2))
            val vel2_min_vel1 = subtract(Pair(vx2, vy2), Pair(vx1, vy1))

            val vx1_prime = vx1 - 2f * m2 / (m1 + m2) * dot(vel1_min_vel2, pos1_min_pos2) /
                    dot(pos1_min_pos2, pos1_min_pos2) * pos1_min_pos2.first
            val vy1_prime = vy1 - 2f * m2 / (m1 + m2) * dot(vel1_min_vel2, pos1_min_pos2) /
                    dot(pos1_min_pos2, pos1_min_pos2) * pos1_min_pos2.second
            val vx2_prime = vx2 - 2f * m1 / (m1 + m2) * dot(vel2_min_vel1, pos2_min_pos1) /
                    dot(pos2_min_pos1, pos2_min_pos1) * pos2_min_pos1.first
            val vy2_prime = vy2 - 2f * m1 / (m1 + m2) * dot(vel2_min_vel1, pos2_min_pos1) /
                    dot(pos2_min_pos1, pos2_min_pos1) * pos2_min_pos1.second

            return arrayOf(vx1_prime, vy1_prime, vx2_prime, vy2_prime)
        }

        fun magnitude(a: Pair<Float, Float>) : Float{
            return sqrt(x = a.first.pow(2) + a.second.pow(2))
        }

        fun magnitude(a: Pair<Double, Double>) : Double{
            return sqrt(x = a.first.pow(2) + a.second.pow(2))
        }

        /*
        Calculate the clockwise angle between 2 vectors
        Param: the vectors
        Return: the angle in radians
         */
        fun measureRadClockwiseAngle(a: Pair<Double, Double>, b: Pair<Double, Double>) : Double {
            val det = det(a, b)
            val dot = dot(a, b)
            var angle = atan2(det, dot)
            //since sprites are drawn pointing up but positive y is down, rotate half a circle
            angle += PI
            return angle
        }

        fun normalize(a: Pair<Float, Float>) : Pair<Float, Float>{
            val len = magnitude(a)
            return a.copy(
                a.first / len,
                a.second / len
            )
        }

        fun round(num: Float, dec: Int): Float{
            val mult = 10f.pow(dec)
            val add = if (num < 0) -0.5 else 0.5

            return ((num * mult) + add).toInt() / mult
        }

        /*
        subtract vector b from vector a
        Param: the vectors as pairs of doubles
        Return: the resulting vector
         */
        fun subtract(a: Pair<Double,  Double>, b: Pair<Double, Double>) : Pair<Double, Double> {
            return Pair(a.first - b.first, a.second - b.second)
        }
    }
}
