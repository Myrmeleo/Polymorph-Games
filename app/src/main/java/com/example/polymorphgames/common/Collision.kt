package com.example.polymorphgames.common

import com.example.polymorphgames.R

data class CollisionData(
    val collide: Boolean = false,
    val floor: Boolean = false,
    val ceil: Boolean = false,
    val wall: Boolean = false,
    val vector: Pair<Float, Float> = Pair(0f, 0f),
)

class Collision private constructor(){
    companion object{

        // Only detects polygon intersections
        // -- Separating Axis Theorem --
        // Based on: https://www.youtube.com/watch?v=IELWpIGtjRg
        fun collide(movable: Polygon, immovable: Polygon): CollisionData{
            val norms =  ArrayList<Pair<Float, Float>>()
            norms.addAll(movable.norms)
            norms.addAll(immovable.norms)


            var minTranslateVector: Pair<Float, Float>? = null

            norms.forEach {
                var movableMin = CustomMath.projectUnitOffset(movable.corners[0], it, movable.offset)
                var movableMinMagnitude = CustomMath.magnitude(movableMin)
                var movableMax = movableMin
                var movableMaxMagnitude = movableMinMagnitude

                var immovableMin = CustomMath.projectUnitOffset(immovable.corners[0], it, immovable.offset)
                var immovableMinMagnitude = CustomMath.magnitude(immovableMin)
                var immovableMax = immovableMin
                var immovableMaxMagnitude = immovableMinMagnitude


                for(i in IntRange(1, movable.corners.lastIndex)){
                    val projected = CustomMath.projectUnitOffset(movable.corners[i], it, movable.offset)
                    val projectedMagnitude = CustomMath.magnitude(projected)


                    if(projectedMagnitude < movableMinMagnitude){
                        movableMin = projected
                        movableMinMagnitude = projectedMagnitude
                    }
                    if(movableMaxMagnitude < projectedMagnitude){
                        movableMax = projected
                        movableMaxMagnitude = projectedMagnitude
                    }
                }

                for(i in IntRange(1, immovable.corners.lastIndex)){
                    val projected = CustomMath.projectUnitOffset(immovable.corners[i], it, immovable.offset)
                    val projectedMagnitude = CustomMath.magnitude(projected)

                    if(projectedMagnitude < immovableMinMagnitude){
                        immovableMin = projected
                        immovableMinMagnitude = projectedMagnitude
                    }

                    if (immovableMaxMagnitude < projectedMagnitude){
                        immovableMax = projected
                        immovableMaxMagnitude = projectedMagnitude
                    }
                }

                if(movableMaxMagnitude < immovableMinMagnitude ||
                    immovableMaxMagnitude < movableMinMagnitude){
                    return CollisionData()
                }

                val overlapX: Float
                val overlapY: Float
                if (movableMaxMagnitude < immovableMaxMagnitude){
                    overlapX = immovableMin.first - movableMax.first
                    overlapY = immovableMin.second - movableMax.second
                }
                else{
                    overlapX = immovableMax.first - movableMin.first
                    overlapY = immovableMax.second - movableMin.second
                }

                val translateVector = Pair(overlapX, overlapY)
                if(minTranslateVector == null) {
                    minTranslateVector = translateVector
                }
                if(CustomMath.magnitude(translateVector) < CustomMath.magnitude(minTranslateVector!!)){
                    minTranslateVector = translateVector
                }
            }

            if(minTranslateVector!!.first == 0f && minTranslateVector!!.second == 0f){
                return CollisionData()
            }

            val floor = minTranslateVector!!.first == 0f && minTranslateVector!!.second < 0f
            val ceil = minTranslateVector!!.second > 0f
            val wall = minTranslateVector!!.first != 0f && minTranslateVector!!.second == 0f
            return CollisionData(collide = true, floor = floor, ceil = ceil, wall = wall, vector = minTranslateVector!!)
        }

        fun collide(movable: Polygon, immovables: ArrayList<Polygon>): CollisionData{
            val list = ArrayList<CollisionData>()
            immovables.forEach {
                val ret = collide(movable, it)
                if(ret.collide){
//                    System.out.println("movable\n${movable.corners}\n${movable.offset}")
//                    System.out.println("immovable\n${it.corners}\n${it.offset}")
//                    System.out.println("immovable.norms = ${it.norms}\n\n")
                    list.add(ret)
                }
            }

            if(list.isEmpty()){
                return CollisionData()
            }

            val len = list.size
            var vectorX = 0f
            var vectorY = 0f
            var floor = false
            var ceil = false
            var wall = false
            list.forEach {
                vectorX += it.vector.first
                vectorY += it.vector.second
                floor = floor || it.floor
                ceil = ceil || it.ceil
                wall = wall || it.wall
            }
            
            return CollisionData(
                collide = true,
                floor = floor,
                ceil = ceil,
                wall = wall,
                vector = Pair(vectorX/len, vectorY/len),
            )
        }
    }
}
