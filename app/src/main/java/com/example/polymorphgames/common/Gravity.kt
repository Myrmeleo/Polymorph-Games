package com.example.polymorphgames.common

class Gravity private constructor() {
    companion object{
        const val g = 9.81f

        fun velocity(deltaTime: Float): Float{
            return 6 * g * deltaTime
        }
    }
}