package com.example.polymorphgames.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp


typealias DP = Float

abstract class Drawable(
    var offset: Pair<DP, DP>,
    val dimen: Pair<DP, DP>,
    var resourceId: Int,
    val descriptor: String,
){

    @Composable open fun Draw(redrawFlag: Boolean?){
        Image(
            painter = painterResource(id = resourceId),
            contentDescription = descriptor,
            contentScale = ContentScale.FillBounds,
            modifier = Modifier
                .width(dimen.first.dp)
                .height(dimen.second.dp)
                .offset(x = offset.first.dp, y = offset.second.dp)
        )
    }
}

