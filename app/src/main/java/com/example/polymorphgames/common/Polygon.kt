package com.example.polymorphgames.common

abstract class Polygon(
    offset: Pair<DP, DP>,
    dimen: Pair<DP, DP>,
    resourceId: Int,
    descriptor: String,
    val corners: ArrayList<Pair<DP, DP>> = ArrayList(), // assume first corner is topleft
    val norms: ArrayList<Pair<DP, DP>> = ArrayList(),
): Drawable(offset, dimen, resourceId, descriptor){
    init{
        calculateNorms()
    }

    fun translate(diffX: DP, diffY: DP){
        offset = Pair(offset.first + diffX, offset.second + diffY)
    }

    protected fun calculateNorms(){
        val last = corners.lastIndex

        if(last == -1){
            return
        }

        for(i in 0 until last){
            norms.add(
                CustomMath.unitNorm(
                a = corners[i+1],
                b = corners[i]
            ))
        }

        norms.add(
            CustomMath.unitNorm(
            a = corners[last],
            b = corners[0]
        ))
    }

    fun serializer(){

    }
}
