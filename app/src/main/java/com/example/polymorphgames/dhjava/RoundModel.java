package com.example.polymorphgames.dhjava;

public class RoundModel {
    int numRed;
    int numBlue;
    int numGreen;
    int numPurple;
    int roundNumber;

    public RoundModel (int r, int b, int g, int p, int round){
        numBlue = b;
        numGreen = g;
        numPurple = p;
        numRed = r;
        roundNumber = round;
    }
}
