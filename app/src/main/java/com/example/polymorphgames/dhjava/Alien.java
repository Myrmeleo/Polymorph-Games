package com.example.polymorphgames.dhjava;

import android.widget.ImageView;

enum Type {
    RED,
    GREEN,
    BLUE,
    PURPLE
}

public class Alien {
    Type alienType;
    float size;
    int hp;
    int gold;
    int speed;
    int x;
    int y;
    ImageView alienImage;

    public Alien (Type AlienType, float Size, int hp, int gold, int Speed, int X, int Y, ImageView AlienImage) {
        alienType = AlienType;
        this.hp = hp;
        this.gold = gold;
        size = Size;
        speed = Speed;
        x = X;
        y = Y;
        alienImage = AlienImage;
    }
}
