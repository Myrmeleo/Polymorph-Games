package com.example.polymorphgames.dhjava;

import android.widget.ImageView;

enum TowerType {
    NONE,
    LASER,
    BULLET,
    PLASMA
}

public class Tower {
    TowerType towerType;
    float size;
    int cost;
    int fireRate;
    int x;
    int y;
    ImageView towerImage;

    public Tower (TowerType type, float Size, int cost, int rate, int X, int Y, ImageView image) {
        towerType = type;
        size = Size;
        this.cost = cost;
        fireRate = rate;
        x = X;
        y = Y;
        towerImage = image;
    }
}
