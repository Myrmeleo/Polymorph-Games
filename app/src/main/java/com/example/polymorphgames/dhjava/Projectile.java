package com.example.polymorphgames.dhjava;

import android.widget.ImageView;

enum ProjectileType {
    LASER,
    BULLET,
    PLASMA
}

public class Projectile {
        ProjectileType projectileType;
        float size;
        int speed;
        int x;
        int y;
        int xStep;
        int yStep;
        int distanceTravelled;
        int dmg;
        ImageView projectileImage;

        public Projectile (ProjectileType type, float Size, int Speed, int X, int Y, int xStep, int yStep, int dmg, ImageView ProjectileImage) {
            projectileType = type;
            size = Size;
            speed = Speed;
            x = X;
            y = Y;
            this.xStep = xStep;
            this.yStep = yStep;
            this.dmg = dmg;
            distanceTravelled = 0;
            this.projectileImage = ProjectileImage;
        }
}
