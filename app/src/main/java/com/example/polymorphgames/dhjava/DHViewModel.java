package com.example.polymorphgames.dhjava;

import android.content.Context;
import android.graphics.Rect;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.example.polymorphgames.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DHViewModel {
    /*
    Todo
    - point system
    */

    Context mainActivityContext;
    ArrayList <Alien> allAliens;
    ArrayList <Alien> removedAliens;
    ArrayList <Tower> allTowers;
    ArrayList <Projectile> allProjectiles;
    ArrayList <Projectile> removedProjectiles;
    ArrayList <RoundModel> rounds;
    int playerHealth = 20;
    int playerGold = 300;
    int range = 500;
    Tower selectedTower;
    int highScore;
    int screenWidth;

    public DHViewModel(Context context, int screenWidth) {
        mainActivityContext = context;
        allAliens = new ArrayList<Alien>();
        removedAliens = new ArrayList<Alien>();
        allTowers = new ArrayList<Tower>();
        allProjectiles = new ArrayList<Projectile>();
        removedProjectiles = new ArrayList<Projectile>();
        rounds = new ArrayList<RoundModel>();
        selectedTower = new Tower (TowerType.NONE, 0, 0, 0, 0, 0, null);
        this.screenWidth = screenWidth;
    }

    //This will create a projectile object at the tower fire rate and set its information
    public void createAllProjectiles (int counter) {

        Alien firstAlien;
        int currentHighestX = 0;
        int hype;
        int adjustment;
        ProjectileType type = ProjectileType.BULLET;

        //This will figure out which alien to shoot the projectile at
        for (Tower tower : allTowers) {
            firstAlien = null;
            currentHighestX = 0;
            for (Alien alien : allAliens) {
                if (checkDistance(alien.x, alien.y, tower.x, tower.y) <= range) {
                    if (alien.x > currentHighestX) {
                        firstAlien = alien;
                        currentHighestX = alien.x;
                    }
                }
            }

            //This will create a projectile object at the tower fire rate
            if ((counter % (100 - tower.fireRate)) == 0) {
                if (firstAlien != null) {
                    //hype = ((int) Math.sqrt((int) Math.pow(Math.abs(firstAlien.x - tower.x), 2) + (int) Math.pow(Math.abs(firstAlien.y - tower.y), 2)));
                    hype = checkDistance(firstAlien.x, firstAlien.y, tower.x, tower.y);

                    adjustment = 40;
                    if (firstAlien.x > tower.x + 50) {
                        adjustment = 80;
                    }
                    int xStep = (20 * (((firstAlien.x + adjustment) - tower.x)) / hype );
                    int yStep = (20 * (((firstAlien.y) - tower.y)) /  hype);
                    if(tower.towerType == TowerType.BULLET){
                        type = ProjectileType.BULLET;
                    }
                    else if(tower.towerType == TowerType.LASER){
                        type = ProjectileType.LASER;
                    }
                    else if(tower.towerType == TowerType.PLASMA){
                        type = ProjectileType.PLASMA;
                    }

                    createProjectile(type, 2, 8, tower.x, tower.y, xStep, yStep);
                }

            }

        }
    }

    //This is a helper function that will calculate distance between two points
    public int checkDistance (int x1, int y1, int x2, int y2) {
        return ((int) Math.sqrt((int) Math.pow(Math.abs(x2 - x1), 2) + (int) Math.pow(Math.abs(y2 - y1), 2)));
    }

    //This function will create a projectile object and set its information
    public void createProjectile (ProjectileType projectileType, float size, int speed, int X, int Y, int xStep, int yStep) {
        ImageView projectileImage = new ImageView(mainActivityContext);
        int dmg = 0;

        // These if statements will assign the correct image to
        // the view and set their respective damage factor
        if (projectileType == ProjectileType.BULLET) {
            projectileImage.setImageResource(R.drawable.bullet);
            dmg = 3;
        }
        else if (projectileType == ProjectileType.LASER) {
            projectileImage.setImageResource(R.drawable.laserball);
            dmg = 1;
        }
        else if (projectileType == ProjectileType.PLASMA) {
            projectileImage.setImageResource(R.drawable.plasmaball);
            dmg = 10;
        }

        //Setting the information of the image view
        projectileImage.setAdjustViewBounds(true);
        projectileImage.setX(X);
        projectileImage.setY(Y);
        projectileImage.setScaleX(size);
        projectileImage.setScaleY(size);
        projectileImage.setRotation(90);
        projectileImage.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        //create the projectile object and add the projectile to the array
        Projectile newProjectile = new Projectile (projectileType, size, speed, X, Y, xStep, yStep, dmg, projectileImage);
        allProjectiles.add(newProjectile);
    }

    //This function will create an alien object and set its information
    public void createAlien (Type alienType, float size, int speed, int X, int Y) {
        ImageView alienImage = new ImageView(mainActivityContext);
        int hp = 0;
        int gold = 0;

        //These if statements will assign the correct image to the view
        //as well as the hp and gold of each alien
        if (alienType == Type.RED) {
            alienImage.setImageResource(R.drawable.magenta_alien);
            hp = 10;
            gold = 10;
        }
        else if (alienType == Type.GREEN) {
            alienImage.setImageResource(R.drawable.teal_alien);
            hp = 5;
            gold = 20;
        }
        else if (alienType == Type.BLUE) {
            alienImage.setImageResource(R.drawable.ultramarine_alien);
            hp = 40;
            gold = 30;

        }
        else if (alienType == Type.PURPLE) {
            alienImage.setImageResource(R.drawable.violet_alien);
            hp = 500;
            gold = 100;

        }

        //Setting the information of the image view
        alienImage.setAdjustViewBounds(true);
        alienImage.setX(X);
        alienImage.setY(Y);
        alienImage.setScaleX(size);
        alienImage.setScaleY(size);
        alienImage.setRotation(90);
        alienImage.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        //create the alien object and add the alien to the array
        Alien newAlien = new Alien (alienType, size, hp, gold, speed, X, Y, alienImage);
        allAliens.add(newAlien);
    }

    //This function will create a tower object and set its information
    public void createTower(TowerType towerType, float size, int X, int Y) {
        ImageView towerImage = new ImageView(mainActivityContext);
        int fireRate = 80;
        int cost = 0;

        //These if statements will assign the correct image to the view
        //as well as the fire rates and costs of towers
        if (towerType == TowerType.NONE) {
            return;
        }
        else if (towerType == TowerType.LASER) {
            towerImage.setImageResource(R.drawable.npc_archer);
            fireRate = 70;
            cost = 100;
        }
        else if (towerType == TowerType.BULLET) {
            towerImage.setImageResource(R.drawable.npc_breacher);
            fireRate = 90;
            cost = 300;
        }
        else if (towerType == TowerType.PLASMA) {
            towerImage.setImageResource(R.drawable.npc_priest);
            fireRate = 50;
            cost = 500;
        }

        //Setting the information of the image view
        towerImage.setAdjustViewBounds(true);
        towerImage.setX(X);
        towerImage.setY(Y);
        towerImage.setScaleX(size);
        towerImage.setScaleY(size);
        towerImage.setRotation(0);
        towerImage.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        playerGold -= cost;

        //create the tower object and add the tower to the array
        Tower newTower = new Tower (towerType, size, cost, fireRate, X, Y, towerImage);
        allTowers.add(newTower);
    }

    //This will calculate the new position of the alien and the logic
    //for the player losing health points
    public void updateAllAlienPositions() {
        for (int i = 0; i < allAliens.size(); i++) {
            if (allAliens.get(i).x > screenWidth + 100) {
                playerHealth--;
                removedAliens.add(allAliens.get(i));
                allAliens.remove(i);
                i--;
            }
            else {
                allAliens.get(i).x = allAliens.get(i).x + allAliens.get(i).speed;
                allAliens.get(i).alienImage.setX(allAliens.get(i).x);
            }
        }
    }

    //This will calculate the new position of all the projectiles and the logic
    //for the max distance a projectile can travel
    public void updateAllProjectiles () {
        for (int i = 0; i < allProjectiles.size(); i++) {

            allProjectiles.get(i).x += allProjectiles.get(i).xStep;
            allProjectiles.get(i).y += allProjectiles.get(i).yStep;
            allProjectiles.get(i).distanceTravelled += ((int) Math.sqrt((int) Math.pow(Math.abs(allProjectiles.get(i).xStep), 2) + (int) Math.pow(Math.abs(allProjectiles.get(i).yStep), 2)));

            if (allProjectiles.get(i).distanceTravelled > range) {
                removedProjectiles.add(allProjectiles.get(i));
                allProjectiles.remove(i);
                i--;
            }
            else{
                allProjectiles.get(i).projectileImage.setX (allProjectiles.get(i).x);
                allProjectiles.get(i).projectileImage.setY (allProjectiles.get(i).y);
            }
        }
    }

    //In this function it will check the collision of the projectiles with the aliens
    //When an alien dies the player gets gold for killing the alien
    public void checkCollision () {
        for (int i = 0; i < allProjectiles.size(); i++) {
            for (int k = 0; k < allAliens.size(); k++) {
                if(helperCollision(allProjectiles.get(i).projectileImage, allAliens.get(k).alienImage)){
                    allAliens.get(k).hp -= allProjectiles.get(i).dmg;
                    if(allAliens.get(k).hp <= 0){
                        playerGold += allAliens.get(k).gold;
                        removedAliens.add(allAliens.get(k));
                        allAliens.remove(k);
                    }
                    removedProjectiles.add(allProjectiles.get(i));
                    allProjectiles.remove(i);
                    i--;
                    break;
                }
            }
        }
    }

    //This function will help check for collision for image views
    public boolean helperCollision(ImageView img1, ImageView img2) {
        Rect img1Rect = new Rect();
        img1.getHitRect(img1Rect);
        Rect img2Rect = new Rect();
        img2.getHitRect(img2Rect);
        return img1Rect.intersect(img2Rect);
    }

    //Create the round objects that tells you how much of each alien in a round
    public void setupAllRounds() {
        rounds.add(new RoundModel(5,0,0,0,0));
        rounds.add(new RoundModel(1,2,2,0,1));
        rounds.add(new RoundModel(3,3,3,0,2));
        rounds.add(new RoundModel(5,3,3,0,3));
        rounds.add(new RoundModel(0,0,0,1,4));
    }

    //Create the aliens that will be spawned in a round
    public void runRound(int roundNum) {
        RoundModel thisRound = rounds.get(roundNum);
        int counter = 0;
        int speedModifier = 2;

        //creating all of the aliens and where they start
        for (int i = 0; i < thisRound.numBlue; i++) {
            createAlien(Type.BLUE, 4, 1 * speedModifier, -140 * counter, 515);
            counter++;
        }
        counter++;
        for (int i = 0; i < thisRound.numRed; i++) {
            createAlien(Type.RED, 4, 2* speedModifier, -140 * counter, 515);
            counter++;
        }
        counter++;

        for (int i = 0; i < thisRound.numGreen; i++) {
            createAlien(Type.GREEN, 4, 4* speedModifier, -140 * counter, 515);
            counter++;
        }
        counter++;
        for (int i = 0; i < thisRound.numPurple; i++) {
            createAlien(Type.PURPLE, 4, 1, -140 * counter, 515);
            counter++;
        }
    }
    // Taken from ScoresViewModel in User Interface
    public Task<String> setAlienDefenceScoreByUser(int score)
    {
        Map<String, Object> data = new HashMap<>();
        data.put("game", "alienDefence");
        data.put("score", score);
        data.put("push", true);

        return FirebaseFunctions.getInstance()
                .getHttpsCallable("updateScore")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        String result = (String) task.getResult().getData();
                        return result;
                    }
                });
    }
}
