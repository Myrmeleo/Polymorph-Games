package com.example.polymorphgames.dhjava;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.os.Handler;
import android.widget.TextView;

import com.example.polymorphgames.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;

import java.util.HashMap;
import java.util.Map;

public class AlienDefenceActivity extends AppCompatActivity implements View.OnTouchListener {

    ConstraintLayout constraintLayout;
    DHViewModel viewModel;
    Handler handler = new Handler();
    int currentRound = 0;
    int lastRound = 5;
    TextView playerHP;
    TextView playerGold;
    TextView alien1;
    TextView alien2;
    TextView alien3;
    boolean startScreenFlag = true;
    TextView startStory;
    ImageView backgroundMap2;
    DisplayMetrics displayMetrics;
    int height;
    int width;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (startScreenFlag) {
            startScreenFlag = false;
            backgroundMap2.setImageAlpha(0);
            startStory.setText("");

        }
        if(viewModel.playerGold >= viewModel.selectedTower.cost){
            if(viewModel.selectedTower.towerType != TowerType.NONE){
                viewModel.createTower(viewModel.selectedTower.towerType, 2, (int)event.getX(), (int)event.getY());
                constraintLayout.addView(viewModel.allTowers.get(viewModel.allTowers.size()-1).towerImage);
            }
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        viewModel = new DHViewModel(this, width);

        // Background Image
        constraintLayout = new ConstraintLayout(this);
        ImageView backgroundMap = new ImageView(this);
        backgroundMap.setImageResource(R.drawable.updatedmap);
        backgroundMap.setScaleType(ImageView.ScaleType.FIT_XY);

        backgroundMap.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        constraintLayout.addView(backgroundMap);

        viewModel.setupAllRounds();

        playerHP = new TextView(this);
        playerHP.setText("❤ " + viewModel.playerHealth);
        playerHP.setTextSize(25);
        constraintLayout.addView(playerHP);

        playerGold= new TextView(this);
        playerGold.setText("\uD83D\uDFE1 " + viewModel.playerGold);
        playerGold.setTextSize(25);
        playerGold.setY(80);
        constraintLayout.addView(playerGold);

        alien1 = new TextView(this);
        alien1.setText("100");
        alien1.setTextSize(25);
        alien1.setY(150);
        alien1.setX(width - 155);
        constraintLayout.addView(alien1);

        alien2 = new TextView(this);
        alien2.setText("300");
        alien2.setTextSize(25);
        alien2.setY(150);
        alien2.setX(width - 320);
        constraintLayout.addView(alien2);

        alien3 = new TextView(this);
        alien3.setText("500");
        alien3.setTextSize(25);
        alien3.setY(150);
        alien3.setX(width - 490);
        constraintLayout.addView(alien3);

        ImageButton laserTower = new ImageButton(this);
        laserTower.setImageResource(R.drawable.npc_archer);
        laserTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);
        laserTower.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        laserTower.setId(View.generateViewId());
        laserTower.setScaleX((float)1.2);
        laserTower.setScaleY((float)1.2);
        laserTower.setX(width - 175);
        laserTower.setY(10);
        constraintLayout.addView(laserTower);

        ImageButton BulletTower = new ImageButton(this);
        BulletTower.setImageResource(R.drawable.npc_breacher);
        BulletTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);
        BulletTower.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        BulletTower.setId(View.generateViewId());
        BulletTower.setScaleX((float)1.2);
        BulletTower.setScaleY((float)1.2);
        BulletTower.setX(width - 345);
        BulletTower.setY(10);
        constraintLayout.addView(BulletTower);

        ImageButton PlasmaTower = new ImageButton(this);
        PlasmaTower.setImageResource(R.drawable.npc_priest);
        PlasmaTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);
        PlasmaTower.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        PlasmaTower.setId(View.generateViewId());
        PlasmaTower.setScaleX((float)1.2);
        PlasmaTower.setScaleY((float)1.2);
        PlasmaTower.setX(width - 515);
        PlasmaTower.setY(10);
        constraintLayout.addView(PlasmaTower);

        Button nextRound = new Button(this);
        nextRound.setText("Next Round");
        nextRound.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        nextRound.setId(View.generateViewId());
        nextRound.setX(width - 300);
        nextRound.setY(880);
        constraintLayout.addView(nextRound);

        constraintLayout.setOnTouchListener(this);

        nextRound.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (currentRound + 1 > lastRound) {
                    backgroundMap2.setImageAlpha(255);
                    startStory.setText("Congratulations on successfully thwarting the initial alien invasion. Unfortunately, your mecha army was destroyed, and you are the sole survivor...");
                    viewModel.setAlienDefenceScoreByUser(viewModel.highScore);
                }
                else {
                    nextRound.setEnabled(false);
                    viewModel.runRound(currentRound);
                    addAllAliensToMap(constraintLayout);
                    currentRound++;
                }

            }
        });

        laserTower.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(viewModel.selectedTower.towerType == TowerType.LASER){
                    viewModel.selectedTower.towerType = TowerType.NONE;
                    viewModel.selectedTower.cost = 0;
                    laserTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);
                }
                else{
                    viewModel.selectedTower.towerType = TowerType.LASER;
                    viewModel.selectedTower.cost = 100;
                    laserTower.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    BulletTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);
                    PlasmaTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);

                }
            }
        });

        BulletTower.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if(viewModel.selectedTower.towerType == TowerType.BULLET){
                    viewModel.selectedTower.towerType = TowerType.NONE;
                    viewModel.selectedTower.cost = 0;
                    BulletTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);
                }
                else{
                    viewModel.selectedTower.towerType = TowerType.BULLET;
                    viewModel.selectedTower.cost = 300;
                    BulletTower.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    laserTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);
                    PlasmaTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);

                }
            }
        });

        PlasmaTower.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(viewModel.selectedTower.towerType == TowerType.PLASMA){
                    viewModel.selectedTower.towerType = TowerType.NONE;
                    viewModel.selectedTower.cost = 0;
                    PlasmaTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);
                }
                else{
                    viewModel.selectedTower.towerType = TowerType.PLASMA;
                    viewModel.selectedTower.cost = 500;
                    PlasmaTower.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    laserTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);
                    BulletTower.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);
                }
            }
        });

        // Background Image
        backgroundMap2 = new ImageView(this);
        backgroundMap2.setImageResource(R.drawable.updatedmap);
        backgroundMap2.setScaleType(ImageView.ScaleType.FIT_XY);
        backgroundMap2.setZ(10);

        backgroundMap2.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        backgroundMap2.setColorFilter(Color.WHITE);
        constraintLayout.addView(backgroundMap2);

        startStory= new TextView(this);
        startStory.setText("After distributing the schematics to Earth's secret service, it is your job to now construct the mechas. The secret service agency, Darkbot has intel regarding the location of the initial alien attack. Your objective is to thwart the alien invasion with the army of mechas... (Tap anywhere to continue)");
        startStory.setTextSize(25);
        startStory.setY(80);
        startStory.setZ(11);
        constraintLayout.addView(startStory);


        setContentView(constraintLayout);

        MyRunnable obj = new MyRunnable(nextRound, viewModel, handler, playerHP, playerGold, constraintLayout, backgroundMap2, startStory, currentRound, lastRound);
        handler.post(obj);
    }

    private void addAllAliensToMap(ConstraintLayout constraintLayout) {
        for (int i = 0; i < viewModel.allAliens.size(); i++) {
            constraintLayout.addView(viewModel.allAliens.get(i).alienImage);
        }
    }
}

class MyRunnable implements Runnable {

    Button button;
    DHViewModel viewModel;
    Handler handler;
    TextView playerHP;
    TextView playerGold;
    ConstraintLayout constraintLayout;
    int counter = 0;
    int currentRound;
    int lastRound;
    boolean done = false;

    TextView screen;
    ImageView bgView;

    public MyRunnable(Button myButton, DHViewModel viewModel, Handler handler, TextView playerHP , TextView playerGold, ConstraintLayout constraintLayout, ImageView bgView, TextView screen, int currentRound, int lastRound) {
        button = myButton;
        this.viewModel = viewModel;
        this.handler = handler;
        this.playerHP = playerHP;
        this.playerGold = playerGold;
        this.constraintLayout = constraintLayout;
        this.bgView = bgView;
        this.screen = screen;
        this.currentRound = currentRound;
        this.lastRound = lastRound;
    }

    public void addAllProjectilesToMap(ConstraintLayout constraintLayout) {
        for (int i = 0; i < viewModel.allProjectiles.size(); i++) {
            constraintLayout.removeView(viewModel.allProjectiles.get(i).projectileImage);
            constraintLayout.addView(viewModel.allProjectiles.get(i).projectileImage);
        }
    }

    public void removeObjects(ConstraintLayout constraintLayout){
        for (int i = 0; i < viewModel.removedAliens.size(); i++) {
            constraintLayout.removeView(viewModel.removedAliens.get(i).alienImage);
        }
        viewModel.removedAliens.clear();
        for (int i = 0; i < viewModel.removedProjectiles.size(); i++) {
            constraintLayout.removeView(viewModel.removedProjectiles.get(i).projectileImage);
        }
        viewModel.removedProjectiles.clear();
    }

    @Override
    public void run() {
        handler.postDelayed(this, 10);

        viewModel.updateAllAlienPositions();
        viewModel.createAllProjectiles(counter);
        viewModel.updateAllProjectiles();
        viewModel.checkCollision();

        addAllProjectilesToMap(constraintLayout);
        removeObjects(constraintLayout);

        playerHP.setText("❤ " + viewModel.playerHealth);
        playerGold.setText("\uD83D\uDFE1 " + viewModel.playerGold);

        if (viewModel.allAliens.size() == 0 && !button.isEnabled()) {
            button.setEnabled(true);
            if(currentRound + 1 > lastRound){
                button.setText("Finish");
            }
            viewModel.playerGold += 100;
        }
        if (viewModel.playerHealth == 0 && !done){
            bgView.setImageAlpha(255);
            screen.setText("You Died...");
            viewModel.setAlienDefenceScoreByUser(viewModel.highScore);
            done = true;
        }

        viewModel.highScore = viewModel.playerHealth * 100;

        counter++;
    }
}
