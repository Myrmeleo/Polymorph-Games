package com.example.polymorphgames.ui.screens
import androidx.compose.runtime.Composable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.mutableStateOf

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.polymorphgames.ui.viewModels.UserViewModel

var signUpToggle  = mutableStateOf(false)
@Composable
fun LoginScreen(viewModel: UserViewModel) {

    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .padding(start = 24.dp, top = 24.dp, end = 24.dp, bottom = 24.dp)
            .fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            fontWeight = FontWeight.Bold,
            fontSize = 30.sp,
            text = if(signUpToggle.value) "Sign Up" else "Login"
        )
        if(signUpToggle.value){
            SignUpForm(viewModel = viewModel)
        }
        else {
            LoginForm(viewModel = viewModel)
        }
    }
}

@Composable
fun LoginForm(viewModel: UserViewModel) {
    EmailField(viewModel)
    PasswordField(viewModel)
    Spacer(modifier = Modifier.height(8.dp))
    ButtonLogin(viewModel)
    Spacer(modifier = Modifier.height(4.dp))
    ButtonSignUpToggle()
}

@Composable
fun SignUpForm(viewModel: UserViewModel) {
    UserNameField(viewModel)
    FirstNameField(viewModel)
    LastNameField(viewModel)
    EmailField(viewModel)
    PasswordField(viewModel)
    Spacer(modifier = Modifier.height(8.dp))
    ButtonCreateUser(viewModel)
    Spacer(modifier = Modifier.height(4.dp))
    ButtonSignUpToggle()
}
@Composable
fun UserNameField(viewModel: UserViewModel) {
    val userName = viewModel.userName.value
    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        value = userName,
        label = { Text(text = "Username") },
        onValueChange = { viewModel.setUserName(it) }
    )
}

@Composable
fun FirstNameField(viewModel: UserViewModel) {
    val firstName = viewModel.firstName.value
    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        value = firstName,
        label = { Text(text = "First Name") },
        onValueChange = { viewModel.setFirstName(it) }
    )
}

@Composable
fun LastNameField(viewModel: UserViewModel) {
    val lastName = viewModel.lastName.value
    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        value = lastName,
        label = { Text(text = "Last Name") },
        onValueChange = { viewModel.setLastName(it) }
    )
}

@Composable
fun EmailField(viewModel: UserViewModel) {
    val email = viewModel.email.value
    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        value = email,
        label = { Text(text = "Email") },
        onValueChange = { viewModel.setUserEmail(it) }
    )
}

@Composable
fun PasswordField(viewModel: UserViewModel) {
    val password = viewModel.password.value
    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        visualTransformation = PasswordVisualTransformation(),
        value = password,
        label = { Text(text = "Password") },
        onValueChange = { viewModel.setPassword(it) }
    )
}
@Composable
fun ButtonLogin(viewModel: UserViewModel) {
    Button(
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp),
        enabled = viewModel.isValid(false),
        content = { Text(text = "Login") },
        onClick = { viewModel.signInWithEmailAndPassword() }
    )
}
@Composable
fun ButtonCreateUser(viewModel: UserViewModel) {
    Button(
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp),
        enabled = viewModel.isValid(true),
        content = { Text(text = "Create") },
        onClick = { viewModel.createUser() }
    )
}

@Composable
fun ButtonSignUpToggle() {
    Button(
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp),
        content = { Text(text = if (signUpToggle.value) "Login Here" else "Sign Up Here") },
        onClick = { signUpToggle.value = !signUpToggle.value }
    )
}
