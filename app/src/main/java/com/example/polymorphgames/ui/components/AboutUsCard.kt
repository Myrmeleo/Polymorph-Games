package com.example.polymorphgames.ui.components

import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

@Composable
fun AboutUsCard() {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Card(elevation = 10.dp, modifier = Modifier.padding(10.dp)){
            Text(text = "Polymorph Games is a gaming platform that strives for extreme fun. This" +
                    " project was developed by Michael DaRocha, Ricky Li," +
                    " Kawal Deol, Shanay Save, Shadman Hassan, and Leo Huang. " +
                    "Please be attentive at all times while playing these games. Have fun and don't get too addicted.",
                modifier = Modifier.padding(10.dp)
            )
        }

    }
}