package com.example.polymorphgames.ui.viewModels

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch
import java.util.regex.Pattern

//private lateinit var functions: FirebaseFunctions
//functions = Firebase.functions

class UserViewModel : ViewModel() {
    private val _isLoggedIn = mutableStateOf(false)
    val isLoggedIn: State<Boolean> = _isLoggedIn
    private val _error = mutableStateOf("")
    val error: State<String> = _error
    private val _email = mutableStateOf("")
    val email: State<String> = _email
    private val _password = mutableStateOf("")
    val password: State<String> = _password
    private val _firstName = mutableStateOf("")
    val firstName: State<String> = _firstName
    private val _lastName = mutableStateOf("")
    val lastName: State<String> = _lastName
    private val _userName = mutableStateOf("")
    val userName: State<String> = _userName
    private val _confirmEmail = mutableStateOf("")
    val confirmEmail: State<String> = _confirmEmail
    private val _newPassword = mutableStateOf("")
    val newPassword: State<String> = _newPassword
    private val _confirmPassword = mutableStateOf("")
    val confirmPassword: State<String> = _confirmPassword
    private val _isColorBlindSetting = mutableStateOf(false)
    val colorBlindSetting: State<Boolean> = _isColorBlindSetting

    private var firebaseUser: FirebaseUser? = null
    fun setFirstName(firstName: String) {
        _firstName.value = firstName
    }
    fun setLastName(lastName:String) {
        _lastName.value = lastName
    }
    // Setters
    fun setUserEmail(email: String) {
        _email.value = email
    }
    fun setPassword(password: String) {
        _password.value = password
    }
    fun setError(error: String) {
        _error.value = error
    }
    fun setNewPassword(str: String) {
        _newPassword.value = str
    }
    fun setConfirmPassword(str: String) {
        _confirmPassword.value = str
    }
    fun setConfirmEmail(str: String) {
        _confirmEmail.value = str
    }
    fun setUserName(str: String) {
        _userName.value = str
    }
    fun setColorBlindSetting(setting: Boolean) {
        _isColorBlindSetting.value = setting
        updateUserSettings()
    }
    init {
        _isLoggedIn.value = getCurrentUser() != null
    }
    fun createUser()  {
        _error.value = ""
        Firebase.auth.createUserWithEmailAndPassword(email.value, password.value)
            .addOnCompleteListener { task ->
                signInCompletedTask(task)
                createUserInFirestore()
                    .addOnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            val e = task.exception
                            if (e is FirebaseFunctionsException) {
                                val code = e.code
                                val details = e.details
                            }
                        }
                    }

            }
    }
    fun createUserInFirestore(): Task<String> {
        // Create the arguments to the callable function.
        val data = hashMapOf(
            "email" to email.value,
            "first_name" to firstName.value,
            "last_name" to lastName.value,
            "user_name" to userName.value,
            "push" to true
        )

        return Firebase.functions
            .getHttpsCallable("updateUser")
            .call(data)
            .continueWith { task ->
                // This continuation runs on either success or failure, but if the task
                // has failed then result will throw an Exception which will be
                // propagated down.
                val result = task.result?.data as String
                result
            }
    }
    fun updateUserProfile(): Task<String> {
        // Create the arguments to the callable function.

        firebaseUser!!.updateEmail(_email.value)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("Email", "User email address updated.")
                }
            }
        firebaseUser!!.updatePassword(_confirmPassword.value)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("Password", "User password updated.")
                }
            }


        val data = hashMapOf(
            "first_name" to firstName.value,
            "last_name" to lastName.value,
            "user_name" to userName.value,
            "email" to email.value,
            "password" to confirmPassword.value,
            "user_name" to userName.value,
            "push" to true
        )

        return Firebase.functions
            .getHttpsCallable("updateUser")
            .call(data)
            .continueWith { task ->
                // This continuation runs on either success or failure, but if the task
                // has failed then result will throw an Exception which will be
                // propagated down.
                val result = task.result?.data as String
                result
            }
    }
    fun updateUserSettings(): Task<String> {
        // Create the arguments to the callable function.
        val data = hashMapOf(
            "colour_blind" to colorBlindSetting.value,
            "push" to true
        )
        return Firebase.functions
            .getHttpsCallable("updateUser")
            .call(data)
            .continueWith { task ->
                // This continuation runs on either success or failure, but if the task
                // has failed then result will throw an Exception which will be
                // propagated down.
                val result = task.result?.data as String
                result
            }
    }
    fun signInWithEmailAndPassword() = viewModelScope.launch {
        try {
            _error.value = ""
            Firebase.auth.signInWithEmailAndPassword(email.value, password.value)
                .addOnCompleteListener { task -> signInCompletedTask(task) }
        } catch (e: Exception) {
            _error.value = e.localizedMessage ?: "Unknown error"
//            Log.d(TAG, "Sign in fail: $e")
        }
    }
    private fun signInCompletedTask(task: Task<AuthResult>) {
        if (task.isSuccessful) {
            // TODO success action and error message
//            Log.d(TAG, "SignInWithEmail:success")
        } else {
            _error.value = task.exception?.localizedMessage ?: "Unknown error"
            // If sign in fails, display a message to the user.
//            Log.w(TAG, "SignInWithEmail:failure", task.exception)
        }
        viewModelScope.launch {
            _isLoggedIn.value = getCurrentUser() != null
        }
    }
    fun getCurrentUserInfo(): Task<String> {
        // Create the arguments to the callable function.
        val data = hashMapOf(
            "push" to true
        )
//        Firebase.functions.useEmulator("localhost", 5000)
        return Firebase.functions
            .getHttpsCallable("getUserInfo")
            .call(data)
            .continueWith { task ->
                // This continuation runs on either success or failure, but if the task
                // has failed then result will throw an Exception which will be
                // propagated down.
                Log.i("TAG1", "HERE")
                val res = task.result?.data as Map<String, Object>
                _email.value = res.getValue("email") as String
                _userName.value = res.getValue("user_name") as String
                _firstName.value = res.getValue("first_name") as String
                _lastName.value = res.getValue("last_name") as String
                _isColorBlindSetting.value = res.getValue("colour_blind") as Boolean
//                val user_name = res.getValue("user_name") as String
//                Log.i("TAG4", user_name)
                val result = "200"
                result
            }
    }
    private fun getCurrentUser() : FirebaseUser? {
        val user = Firebase.auth.currentUser
        firebaseUser = Firebase.auth.currentUser
//        Log.d(TAG, "user display name: ${user?.displayName}, email: ${user?.email}")
        clearImportantFields()

        Log.i("BEFORE", "BEFPORE")
        getCurrentUserInfo().addOnCompleteListener { task->
            if(!task.isSuccessful) {
                Log.i("FFError", "HERE")
                val e = task.exception
                println(e)
                if (e is FirebaseFunctionsException){
                    val details = e.details
                    Log.i("FFError2", "HERE")
                }
            }
        }
        return user
    }
    fun isValidLogin() : Boolean {
        if (email.value.isBlank() || password.value.isBlank()) {
            return false
        }
        return true
    }
    fun isValid(signUp: Boolean) : Boolean {
        if (email.value.isBlank() || password.value.isBlank()) {
            return false
        }
        // Email Validation
        val emailPattern = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        )
        if (!emailPattern.matcher(email.value).matches()){
            return false
        }

        // Password Validation
        val uppercase = Pattern.compile("[A-Z]")
        val lowercase = Pattern.compile("[a-z]")
        val digit = Pattern.compile("[0-9]")
        if(!(lowercase.matcher(password.value).find() and uppercase.matcher(password.value).find()
                    and digit.matcher(password.value).find() and (password.value.length >= 8))){
            return false
        }

        // First and Last Name Validation
        val namePattern = Pattern.compile("^[A-Za-z][A-Za-z_]{1,29}$")
        val userNamePattern = Pattern.compile("^[A-Za-z0-9][A-Za-z0-9_]{1,29}$")
        if (signUp and !(namePattern.matcher(firstName.value).matches() and
                    namePattern.matcher(lastName.value).matches())){
            return false
        }
        if (signUp and !userNamePattern.matcher(userName.value).matches()){
            return false
        }
        if(_isLoggedIn.value) {
            if(!emailPattern.matcher(confirmEmail.value).matches()){
                return false
            }
            if(!(lowercase.matcher(newPassword.value).find() and uppercase.matcher(newPassword.value).find()
                        and digit.matcher(newPassword.value).find() and (newPassword.value.length >= 8))){
                return false
            }
            if(!(newPassword.value == confirmPassword.value)){
                return false
            }
            if (!userNamePattern.matcher(userName.value).matches()){
                return false
            }
        }
        return true
    }
    fun isValidSignUp() : Boolean {
        if (email.value.isBlank() || password.value.isBlank() ||
            firstName.value.isBlank() || lastName.value.isBlank()) {
            return false
        }
        return true
    }
    fun signOut() = viewModelScope.launch {
        Firebase.auth.signOut()
        clearAllFields()
        _isLoggedIn.value = false
    }

    private fun clearAllFields() {
        _email.value = ""
        _confirmEmail.value = ""
        _password.value = ""
        _newPassword.value = ""
        _confirmPassword.value = ""
        _userName.value = ""
        _firstName.value = ""
        _lastName.value = ""
    }

    private fun clearImportantFields() {
        _password.value = ""
        _newPassword.value = ""
        _confirmPassword.value = ""
    }
}