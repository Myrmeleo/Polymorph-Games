package com.example.polymorphgames.ui.viewModels

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.launch
import java.util.regex.Pattern

class RatingsViewModel :ViewModel() {
    private val _catastropheRating = mutableStateOf(0.0)
    val catastropheRating: State<Double> = _catastropheRating
    private val _picturePerfectRating = mutableStateOf(0.0)
    val picturePerfectRating: State<Double> = _picturePerfectRating
    private val _alienDefenceRating = mutableStateOf(0.0)
    val alienDefenceRating: State<Double> = _alienDefenceRating
    private val _allOutDefenceRating = mutableStateOf(0.0)
    val allOutDefenceRating: State<Double> = _allOutDefenceRating

    private val _catastropheUserRating = mutableStateOf(0.0)
    val catastropheUserRating: State<Double> = _catastropheUserRating
    private val _allOutDefenceUserRating = mutableStateOf(0.0)
    val allOutDefenceUserRating: State<Double> = _allOutDefenceUserRating
    private val _picturePerfectUserRating = mutableStateOf(0.0)
    val picturePerfectUserRating: State<Double> = _picturePerfectUserRating
    private val _alienDefenceUserRating = mutableStateOf(0.0)
    val alienDefenceUserRating: State<Double> = _alienDefenceUserRating

    fun getRatings(): Task<String> {
        // get ratings from firestore
        // set ratings
        val data = hashMapOf(
            "push" to true
        )
//        Firebase.functions.useEmulator("localhost", 5000)
        Log.i("Rating", "Before Rating Works")
        return Firebase.functions
            .getHttpsCallable("getRatings")
            .call(data)
            .continueWith { task ->
                // This continuation runs on either success or failure, but if the task
                // has failed then result will throw an Exception which will be
                // propagated down.
                Log.i("Rating", "Before Result Check")
                val res = task.result?.data as Map<String, Object>
                val catString = res.getValue("catastrophe").toString()
                val ADString = res.getValue("alienDefence").toString()
                val AODString = res.getValue("allOutDefence").toString()
                val picString = res.getValue("picturePerfect").toString()
                Log.i("Rating", picString)
                _catastropheRating.value = catString.toDouble()
                _alienDefenceRating.value = ADString.toDouble()
                _allOutDefenceRating.value = AODString.toDouble()
                _picturePerfectRating.value = picString.toDouble()
                Log.i("Rating", "Get Rating Works")
                val result = "200"
                result
            }
    }

    fun getRatingByGame(game: String): State<Double> {
        if(game == "catastrophe") {
            return catastropheRating
        }
        else if (game == "alienDefence") {
            return alienDefenceRating
        }
        else if (game == "allOutDefence") {
            return allOutDefenceRating
        }
        else if (game == "picturePerfect") {
            return picturePerfectRating
        }
        else {
            return mutableStateOf(0.0)
        }
    }

    fun setRatingByGame(rating: Double, game: String) {
        if(game == "catastrophe") {
            _catastropheUserRating.value = rating
        }
        else if (game == "alienDefence") {
            _alienDefenceUserRating.value = rating
        }
        else if (game == "allOutDefence") {
            _allOutDefenceUserRating.value = rating
        }
        else if (game == "picturePerfect") {
            _picturePerfectUserRating.value = rating
        }
    }

    fun updateRatingByUser(game: String): Task<String> {
        var rating = 5.0
        if(game == "catastrophe") {
            rating = catastropheUserRating.value
        }
        else if (game == "alienDefence") {
            rating = alienDefenceUserRating.value
        }
        else if (game == "allOutDefence") {
            rating = allOutDefenceUserRating.value
        }
        else if (game == "picturePerfect") {
            rating = picturePerfectUserRating.value
        }
        val data = hashMapOf(
            "game" to game,
            "rating" to rating,
            "push" to true
        )
        return Firebase.functions
            .getHttpsCallable("updateUserRatings")
            .call(data)
            .continueWith { task ->
                // This continuation runs on either success or failure, but if the task
                // has failed then result will throw an Exception which will be
                // propagated down.
                getRatings()
                val result = task.result?.data as String
                result
            }
    }
}