package com.example.polymorphgames.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.foundation.Image
import androidx.compose.ui.unit.dp
import android.R
import androidx.compose.foundation.clickable
import androidx.compose.foundation.hoverable
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Settings
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.example.polymorphgames.ui.screens.DrawerScreens

private val screens = listOf(
    DrawerScreens.Games,
    DrawerScreens.Profile,
    DrawerScreens.Settings,
    DrawerScreens.AboutUs
)

@Composable
fun NavSidePanel(
    modifier: Modifier = Modifier,
    onDestinationClicked: (route: String) -> Unit,
    titleChanger: (str: String) -> Unit
) {
    Column(
        modifier
            .fillMaxSize()
            .padding(top = 25.dp)
//            .padding(start = 14.dp, top = 48.dp)
    ) {
//        Image(
//            painter = painterResource(R.drawable.ic_dialog_info),
//            contentDescription = "App Icon"
//        )
        Text(
            modifier = Modifier.padding(start = 14.dp),
            text = "Polymorph Games",
//            style = MaterialTheme.typography.h4,
            fontWeight = FontWeight.Bold,
            fontSize = 30.sp
        )
        screens.forEach {
            screen ->
            Spacer(Modifier.height(24.dp))
            Divider(thickness = 4.dp, color = Color.LightGray)
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth().padding(top = 20.dp)
            ) {
                Icon(
                    imageVector = screen.buttonIcon,
                    contentDescription = "Nav Side Bar",
                    tint = Color.Black,
                    modifier = Modifier.size(40.dp).padding(start = 14.dp)
                )
                Spacer(Modifier.width(10.dp))
                Text(
                    text = screen.title,
//                    style = MaterialTheme.typography.h4,
                    fontSize = 24.sp,
                    fontWeight = FontWeight.SemiBold,
                    modifier = Modifier.clickable {
                        onDestinationClicked(screen.route)
                        titleChanger(screen.title)
                    }
                )
            }

        }
    }
}
//
//@Preview
//@Composable
//fun DrawerPreview() {
//    NavDrawer {
//        NavSidePanel()
//    }
//}
