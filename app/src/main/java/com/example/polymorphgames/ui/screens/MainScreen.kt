package com.example.polymorphgames.ui.screens

import android.content.Context
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.internal.composableLambda
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
//import androidx.navigation.*
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.NavHostController
import androidx.navigation.Navigator
import com.example.polymorphgames.R
import com.example.polymorphgames.ui.classes.StateManager
import com.example.polymorphgames.ui.components.GamesPanel
import com.example.polymorphgames.ui.components.NavSidePanel
import com.example.polymorphgames.ui.components.ProfileForm
import com.example.polymorphgames.ui.components.SettingsForm
import com.example.polymorphgames.ui.components.AboutUsCard
import com.example.polymorphgames.ui.viewModels.RatingsViewModel
import com.example.polymorphgames.ui.viewModels.ScoresViewModel
import com.example.polymorphgames.ui.viewModels.UserViewModel

import kotlinx.coroutines.launch

sealed class DrawerScreens(val title: String, val route: String, val buttonIcon: ImageVector) {
    object Games: DrawerScreens("Games", "games", Icons.Filled.Home)
    object Profile: DrawerScreens("Profile", "profile", Icons.Filled.Person)
    object Settings: DrawerScreens("Settings", "settings", Icons.Filled.Settings)
    object AboutUs: DrawerScreens("About Us", "aboutus", Icons.Filled.Info)
}

private val screens = listOf(
    DrawerScreens.Games,
    DrawerScreens.Profile,
    DrawerScreens.Settings,
    DrawerScreens.AboutUs
)
var currentTitle = mutableStateOf("Games")
var isDrawerOpen = mutableStateOf(false)
@Composable
fun MainScreen(
    stateManager: StateManager,
    logout: () -> Unit,
    viewModel: UserViewModel,
    ratingsViewModel: RatingsViewModel,
    scoresViewModel: ScoresViewModel,
    context: Context
) {
    val navController = rememberNavController()
//    val navController = rememberSavedInstanceState(
//        saver = Navigator.
//    )
    Surface(color = MaterialTheme.colors.background) {
        val drawerState = rememberDrawerState(DrawerValue.Closed)
        val scope = rememberCoroutineScope()
        val openDrawer = {
            scope.launch {
                drawerState.open()
                isDrawerOpen.value = true
            }
        }
        val closeDrawer = {
            scope.launch {
                drawerState.close()
                isDrawerOpen.value = false
            }
        }
        Scaffold(
            modifier = Modifier.fillMaxWidth(),
            topBar = { TopAppBar(
                title = {
                    Text(
                        modifier = Modifier.fillMaxWidth().padding(end = 22.dp),
                        text = currentTitle.value, color = Color.White,
                        textAlign = TextAlign.Center,
                        fontSize = 26.sp
                    ) //Here
                },
                navigationIcon = {
                    IconButton(
                        onClick = {
                            isDrawerOpen.value = !isDrawerOpen.value
                            if (isDrawerOpen.value) openDrawer() else closeDrawer()
                        }) {
                        Icon(
                            imageVector =  Icons.Filled.Menu,
                            contentDescription = "Nav Side Bar",
                            tint = Color.White,
                            modifier = Modifier.size(24.dp)
                        )
                    }
                },
                actions = {
                    IconButton(onClick = { logout() }) {
                        Icon(
                            imageVector = Icons.Filled.ExitToApp,
                            contentDescription = "Logout",
                            tint = Color.White,
                            modifier = Modifier.size(24.dp)
                        )
                    }
                },
//                backgroundColor = colorResource(id = R.color.purple_200),
                contentColor = Color.White,
                elevation = 12.dp
            ) },
            content = {
                Spacer(modifier = Modifier.height(4.dp))
                Column(
                    modifier = Modifier.fillMaxHeight()
                ) {
                    ModalDrawer(
                        drawerState = drawerState,
                        gesturesEnabled = drawerState.isOpen,
                        drawerContent = {
                            NavSidePanel(
                                onDestinationClicked = { route ->
                                    scope.launch {
                                        drawerState.close()
                                        isDrawerOpen.value = false
                                    }
                                    navController.navigate(route) {
//                            popUpToRoute = navController.graph.startDestination
                                        launchSingleTop = true
                                    }
                                },
                                titleChanger = {title ->
                                    scope.launch {
                                        currentTitle.value = title
                                    }
                                }
                            )
                        }
                    ) {
                        NavHost(
                            navController = navController,
                            startDestination = DrawerScreens.Games.route
                        ) {
                            composable(DrawerScreens.Games.route) {
//                                currentTitle.value = "Games"
                                GamesPanel(
                                    stateManager = stateManager,
                                    userViewModel = viewModel,
                                    ratingsViewModel = ratingsViewModel,
                                    scoresViewModel = scoresViewModel,
                                    context = context
                                )
                            }
                            composable(DrawerScreens.Profile.route) {
//                                currentTitle.value = "Profile"
                                ProfileForm(viewModel = viewModel)
                            }
                            composable(DrawerScreens.Settings.route){
//                                currentTitle.value = "Settings"
                                SettingsForm(viewModel = viewModel)
                            }
                            composable(DrawerScreens.AboutUs.route) {
//                                currentTitle.value = "About Us"
                                AboutUsCard()
                            }
                        }
                    }
                }
            }
        )

    }
}
