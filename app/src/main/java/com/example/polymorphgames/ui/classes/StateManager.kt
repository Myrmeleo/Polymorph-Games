package com.example.polymorphgames.ui.classes

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf

class StateManager {
    var main = mutableStateOf(false)
    var catastrophe = mutableStateOf(false)
    var alienDefence = mutableStateOf(false)
    var picturePerfect = mutableStateOf(false)
    var allOutDefence = mutableStateOf(false)

    fun getStateByTitle(title: String) : MutableState<Boolean> {
        if(title == "Catastrophe"){
            return catastrophe;
        }
        if (title == "All-Out Defence") {
            return allOutDefence;
        }
        if (title == "Picture Perfect") {
            return picturePerfect;
        }
        if (title == "Alien Defence") {
            return alienDefence;
        }
        return main;
    }
}