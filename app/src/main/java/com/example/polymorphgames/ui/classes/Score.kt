package com.example.polymorphgames.ui.classes

class Score {
    private var date = ""
    private var time = ""
    private var score = 0

    constructor(date: String, time: String, score: Int) {
        this.date = date
        this.time = time
        this.score = score
    }

    fun getDate(): String {
        return this.date
    }
    fun getTime(): String {
        return this.time
    }
    fun getScore(): Int {
        return this.score
    }
}