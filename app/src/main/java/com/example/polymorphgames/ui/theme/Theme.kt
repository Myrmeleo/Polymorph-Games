package com.example.polymorphgames.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private var DarkColorPalette = darkColors(
        primary = Purple200,
        primaryVariant = Purple700,
        secondary = Teal200
)

private var LightColorPalette = lightColors(
        primary = Purple500,
        primaryVariant = Purple700,
        secondary = Teal200
)



@Composable
fun PolymorphGamesTheme(darkTheme: Boolean = isSystemInDarkTheme(), isColourBlind: Boolean = false, content: @Composable () -> Unit) {
    val colors = if(isColourBlind){
        LightColorPalette = lightColors(
            primary = Color(222,24,100),
            primaryVariant = Color(222,24,100)
        )

        DarkColorPalette = darkColors(
            primary = Color(222,24,100),
            primaryVariant = Color(222,24,100)
        )
        if (darkTheme) {
            DarkColorPalette
        } else {
            LightColorPalette
        }
    } else {
        DarkColorPalette = darkColors(
            primary = Purple200,
            primaryVariant = Purple700,
            secondary = Teal200
        )

        LightColorPalette = lightColors(
            primary = Purple500,
            primaryVariant = Purple700,
            secondary = Teal200
        )
        if (darkTheme) {
            DarkColorPalette
        } else {
            LightColorPalette
        }
    }


    MaterialTheme(
            colors = colors,
            typography = Typography,
            shapes = Shapes,
            content = content
    )
}