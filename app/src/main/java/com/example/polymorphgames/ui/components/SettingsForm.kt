package com.example.polymorphgames.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.Checkbox
import androidx.compose.material.Divider
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.polymorphgames.ui.viewModels.UserViewModel

@Composable
fun SettingsForm(viewModel: UserViewModel) {
    Column(
        modifier = Modifier.fillMaxWidth().padding(top = 16.dp)
    ) {
        // Settings Form
        Spacer(modifier = Modifier.height(20.dp))
        ColourBlindField(viewModel)
        Divider(thickness = 1.dp)
    }
}

@Composable
fun ColourBlindField(viewModel: UserViewModel) {
    val isChecked = remember{mutableStateOf(false)}
    val isColorBlind = viewModel.colorBlindSetting
    Row(){
        Checkbox(
            modifier = Modifier.align(Alignment.CenterVertically).padding(start = 20.dp),
            checked = isColorBlind.value,
            onCheckedChange = {viewModel.setColorBlindSetting(it)}
        )
        Spacer(modifier = Modifier.width(30.dp))
        Text(
            modifier = Modifier
                .padding(16.dp).align(Alignment.CenterVertically),
            textAlign = TextAlign.End,
            fontWeight = FontWeight.ExtraBold,
            text = "Colour Blind Mode",
            fontSize = 26.sp
        )

    }

}