package com.example.polymorphgames.ui.components

import android.content.Context
import android.content.Context.APPWIDGET_SERVICE
import android.media.Rating
import android.util.AttributeSet
import android.view.View
import android.widget.RatingBar
import androidx.compose.animation.core.tween
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogWindowProvider
import androidx.core.content.ContextCompat.getSystemService
import androidx.databinding.DataBindingUtil.setContentView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import com.example.polymorphgames.R
import com.example.polymorphgames.catastrophe.game.GameScreen
import com.example.polymorphgames.ui.classes.Score
import com.example.polymorphgames.ui.classes.StateManager
import com.example.polymorphgames.ui.viewModels.UserViewModel
import org.intellij.lang.annotations.JdkConstants
import kotlin.concurrent.thread
import com.example.polymorphgames.ui.viewModels.RatingsViewModel
import com.example.polymorphgames.ui.viewModels.ScoresViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

sealed class GamesInfo(val title: String, val chapter: Int, val imageId: Int, var rating: Double,
                       val db_name: String, var scores: MutableLiveData<MutableList<Score>>) {
    object AlienDefence: GamesInfo("Alien Defence", 3, R.drawable.ad_icon,
        5.0, "alienDefence", MutableLiveData<MutableList<Score>>())
    object PicturePerfect: GamesInfo("Picture Perfect", 2, R.drawable.picperf_icon,
        5.0, "picturePerfect", MutableLiveData<MutableList<Score>>())
    object Catastrophe: GamesInfo("Catastrophe", 1, R.drawable.cat_icon,
        5.0, "catastrophe", MutableLiveData<MutableList<Score>>())
    object AllOutDefence: GamesInfo("All-Out Defence", 4, R.drawable.aod_icon,
        5.0, "allOutDefence", MutableLiveData<MutableList<Score>>())
}

private val games = listOf(
    GamesInfo.Catastrophe,
    GamesInfo.PicturePerfect,
    GamesInfo.AlienDefence,
    GamesInfo.AllOutDefence
)
private val isGameInfo = mutableStateOf(false)
private val gameSelect = mutableStateOf(games[0])
private val selectedTabIndex =  mutableStateOf(0)
private val showRatingModel = mutableStateOf(false)
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun GamesPanel(
    modifier: Modifier = Modifier,
    stateManager: StateManager,
    userViewModel: UserViewModel,
    ratingsViewModel: RatingsViewModel,
    scoresViewModel: ScoresViewModel,
    context: Context
) {
    games[0].rating = ratingsViewModel.catastropheRating.value
    games[1].rating = ratingsViewModel.picturePerfectRating.value
    games[2].rating = ratingsViewModel.alienDefenceRating.value
    games[3].rating = ratingsViewModel.allOutDefenceRating.value
    games.forEach { game ->
        game.scores = scoresViewModel.prepareScoresByGame(game.db_name)
    }
//    games[0].scores = scoresViewModel.catastropheScoresLive
//    games[1].scores = scoresViewModel.picturePerfectScoresLive
//    games[2].scores = scoresViewModel.alienDefenceScoresLive
//    games[3].scores = scoresViewModel.allOutDefenceScoresLive

    if(isGameInfo.value){
        // stateManager.getStateByTitle(game.title).value = true
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            Spacer(modifier = Modifier.height(4.dp))
            Row(modifier = Modifier.fillMaxWidth()){
                IconButton(
                    modifier = Modifier.padding(start = 6.dp),
                    onClick = { isGameInfo.value = false }
                ) {
                    Icon(
                        imageVector =  Icons.Filled.KeyboardArrowLeft,
                        contentDescription = "Exit Game Info",
                        tint = Color.Black,
                        modifier = Modifier
                            .size(35.dp)
                    )
                }
//                Spacer(modifier = Modifier.width(1.dp))
                Text(
                    modifier = Modifier.padding(top = 11.dp, end = 16.dp),
                    text = "Back",
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 18.sp
                )
//                Spacer(modifier = Modifier.height(25.dp))
                Text(
//                        modifier = Modifier.padding(12.dp),
                    text = gameSelect.value.title,
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.ExtraBold,
                    fontSize = 30.sp
                )
            }

            Spacer(modifier = Modifier.height(4.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ){
                Box(modifier = Modifier
                    .padding(start = 16.dp, end = 20.dp, bottom = 16.dp)) {
                    Column(
                        verticalArrangement = Arrangement.Top,
                        horizontalAlignment = Alignment.Start
                    ) {
                        Card(
                            modifier = Modifier
                                .width(160.dp)
                                .height(150.dp),
                            shape = RoundedCornerShape(16.dp),
                            border = BorderStroke(1.dp, Color.LightGray)
                        ){
                            Box(
                                modifier = Modifier
                                    .width(160.dp)
                                    .height(150.dp),
                            ) {
                                Image(
                                    painterResource(id = gameSelect.value.imageId),
                                    contentDescription = gameSelect.value.title,
                                    contentScale = ContentScale.FillBounds,
                                    modifier = Modifier.fillMaxSize()
                                )
                            }
                        }

                    }
                }
//                Spacer(modifier = Modifier.width(6.dp))
                Column(
                    verticalArrangement = Arrangement.SpaceEvenly,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Spacer(modifier = Modifier.height(25.dp))
                    Text(
                        modifier = Modifier
                            .padding(12.dp)
                            .clickable { showRatingModel.value = true },
                        text = "Ratings: " + (Math.round(ratingsViewModel.getRatingByGame(gameSelect.value.db_name).value * 100).toDouble() / 100),
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 22.sp
                    )
                    if(showRatingModel.value){
                        AlertDialog(
                            onDismissRequest = {
                                showRatingModel.value = false
                            },
                            title = {
                                Text("Let us know what you think?")
                            },
                            confirmButton = {
                                Button(
                                    onClick = {
                                        showRatingModel.value = false
                                        ratingsViewModel.updateRatingByUser(gameSelect.value.db_name)
                                    },
                                ) {
                                    Text("Confirm Rating")
                                }
                            },
                            dismissButton = {
                                Button(
                                    onClick = {
                                        showRatingModel.value = false
                                    },
                                ) {
                                    Text("Close")
                                }
                            },
                            text = {
                                var ratingBarID = View.generateViewId()
                                AndroidView(factory = {
                                    RatingBar(it).apply {
                                        numStars = 5
                                        rating = 0f
                                        stepSize = 0.5f
                                        id = ratingBarID
                                        onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener {
                                                ratingBar, fl, b ->
                                            ratingsViewModel.setRatingByGame(fl.toDouble(), gameSelect.value.db_name)
                                        }
                                    }
                                })
                            }
                        )
                    }
                    Spacer(modifier = Modifier.height(19.dp))
                    Button(
                        modifier = Modifier
                            .height(50.dp)
                            .width(150.dp),
                        enabled = true,
                        content = { Text(text = "Play", fontSize = 18.sp) },
                        onClick = {
                            isGameInfo.value = false
                            stateManager.getStateByTitle(gameSelect.value.title).value = true
                        }
                    )

                }
                Spacer(modifier = Modifier.width(6.dp))
            }
            Divider(thickness = 4.dp)
            TabRow(
                selectedTabIndex = selectedTabIndex.value,
                modifier = Modifier.fillMaxWidth(),
                backgroundColor = Color.White
            ) {
                listOf("Scores").forEachIndexed {
                    index, text -> Tab(
                        selected = selectedTabIndex.value == index,
                        onClick = { selectedTabIndex.value = index },
                        modifier = Modifier.height(50.dp),
                        text = { Text(text, fontSize = 20.sp) }
                    )
                }
            }
            LazyColumn(
                modifier = Modifier.fillMaxWidth()
            ) {
                if(selectedTabIndex.value == 0) {
                    items(gameSelect.value.scores.value!!.toList()) { score ->
                        Row(
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            Card(
                                modifier = Modifier.fillMaxWidth(),
                                border = BorderStroke(1.dp, Color.LightGray)
                            ){
                                Row(
                                    modifier = Modifier.fillMaxWidth(),
                                    horizontalArrangement = Arrangement.SpaceEvenly
                                ){
                                    Text(
                                        modifier = Modifier.width(50.dp),
                                        text = score.getScore().toString(),
                                        fontWeight = FontWeight.SemiBold,
                                        fontSize = 18.sp
                                    )
                                    var date = score.getDate().split(" ")
                                    Text(
                                        text = "${date[0]} ${date[1]} ${date[2]} ${date[5]}",
                                        fontWeight = FontWeight.SemiBold,
                                        fontSize = 18.sp
                                    )
                                    Text(
                                        text = "${date[3]}",
                                        fontWeight = FontWeight.SemiBold,
                                        fontSize = 18.sp
                                    )
                                }

                            }
                        }

                    }
                } else {
                    val rankings = listOf(1, 2, 3)
                    items(rankings, key = {it}) { rank ->
                        Row(
                            modifier = Modifier
                        ) {
                            Card(
                                modifier = Modifier,
                                border = BorderStroke(1.dp, Color.LightGray)
                            ){
                                Text(text = "rank")
                                Text(text = "username")
                                Text(text="highscore")
                            }
                        }

                    }
                }
            }
        }

    }
    else {
        LazyVerticalGrid(
            modifier = modifier,
            cells = GridCells.Fixed(2)
        ) {
            // Game Card for each game
            items(games) { game ->
                Card(
                    shape = RoundedCornerShape(16.dp),
                    border = BorderStroke(1.dp, Color.LightGray),
                    modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth(),
                ) {
                    Box(modifier = Modifier
//                    .height(200.dp)
                        .padding(16.dp)) {
                        Column(
                            modifier
                                .fillMaxSize()
                                .clickable {
                                    ratingsViewModel.getRatings()
                                    isGameInfo.value = true
                                    gameSelect.value = game

                                    //scoresViewModel.getScoresByGame(game.title)
                                },
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Card(
                                modifier = Modifier
                                    .width(160.dp)
                                    .height(140.dp),
                                shape = RoundedCornerShape(8.dp),
                                border = BorderStroke(1.dp, Color.LightGray)
                            ) {
                                Box(
                                    modifier = Modifier
                                        .width(160.dp)
                                        .height(140.dp),
                                ) {
                                    Image(
                                        painterResource(id = game.imageId),
                                        contentDescription = game.title,
                                        contentScale = ContentScale.FillBounds,
                                        modifier = Modifier.fillMaxSize()
                                    )
                                }
                            }

                            Spacer(modifier = Modifier.height(14.dp))
                            Text(
                                text = "Chapter: " + game.chapter,
                                textAlign = TextAlign.Center,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 20.sp
                            )
                            Text(
                                game.title,
                                textAlign = TextAlign.Center,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 16.sp
                            )
                        }

                    }
                }
            }
        }
    }
}