package com.example.polymorphgames.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import com.example.polymorphgames.ui.screens.*
import com.example.polymorphgames.ui.viewModels.UserViewModel

@Composable
fun ProfileForm(viewModel: UserViewModel) {
    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .padding(start = 24.dp, top = 24.dp, end = 24.dp, bottom = 24.dp)
            .fillMaxWidth(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        // Profile Form Here
        val spacing = 4.dp
        Spacer(modifier = Modifier.height(20.dp))
        UserNameField(viewModel)
        Spacer(modifier = Modifier.height(spacing))
        EmailField(viewModel)
        Spacer(modifier = Modifier.height(spacing))
        ConfirmEmailField(viewModel)
        Spacer(modifier = Modifier.height(spacing))
        OldPasswordField(viewModel)
        Spacer(modifier = Modifier.height(spacing))
        NewPasswordField(viewModel)
        Spacer(modifier = Modifier.height(spacing))
        ConfirmPasswordField(viewModel)
        Spacer(modifier = Modifier.height(60.dp))
        ButtonUpdateUser(viewModel)
    }
}

@Composable
fun OldPasswordField(viewModel: UserViewModel) {
    val password = viewModel.password.value
    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        visualTransformation = PasswordVisualTransformation(),
        value = password,
        label = { Text(text = "Old Password") },
        onValueChange = { viewModel.setPassword(it) }
    )
}
@Composable
fun NewPasswordField(viewModel: UserViewModel) {
    val password = viewModel.newPassword.value
    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        visualTransformation = PasswordVisualTransformation(),
        value = password,
        label = { Text(text = "New Password") },
        onValueChange = { viewModel.setNewPassword(it) }
    )
}
@Composable
fun ConfirmPasswordField(viewModel: UserViewModel) {
    val password = viewModel.confirmPassword.value
    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        visualTransformation = PasswordVisualTransformation(),
        value = password,
        label = { Text(text = "Confirm New Password") },
        onValueChange = { viewModel.setConfirmPassword(it) }
    )
}
@Composable
fun ConfirmEmailField(viewModel: UserViewModel) {
    val email = viewModel.confirmEmail.value
    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        value = email,
        label = { Text(text = "Confirm Email") },
        onValueChange = { viewModel.setConfirmEmail(it) }
    )
}
@Composable
fun ButtonUpdateUser(viewModel: UserViewModel) {
    Button(
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp),
        enabled = viewModel.isValid(true),
        content = { Text(text = "Update") },
        onClick = { viewModel.updateUserProfile() }
    )
}
