package com.example.polymorphgames.ui.viewModels

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.polymorphgames.ui.classes.Score
import com.example.polymorphgames.ui.classes.Ranking
import com.google.android.gms.tasks.Task
import com.google.firebase.Timestamp
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import com.google.type.Date
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.sql.Time
import java.util.regex.Pattern

class ScoresViewModel : ViewModel() {
    val catastropheScores = mutableListOf<Score>()
    val alienDefenceScores = mutableListOf<Score>()
    val allOutDefenceScores = mutableListOf<Score>()
    val picturePerfectScores = mutableListOf<Score>()

    val catastropheScoresLive = MutableLiveData<MutableList<Score>>()
    val alienDefenceScoresLive = MutableLiveData<MutableList<Score>>()
    val allOutDefenceScoresLive = MutableLiveData<MutableList<Score>>()
    val picturePerfectScoresLive = MutableLiveData<MutableList<Score>>()

    val catastropheRankings = mutableSetOf<Ranking>()
    val alienDefenceRankings = mutableSetOf<Ranking>()
    val allOutDefenceRankings = mutableSetOf<Ranking>()
    val picturePerfectRankings = mutableSetOf<Ranking>()

    init {
        catastropheScoresLive.value = catastropheScores
        alienDefenceScoresLive.value = alienDefenceScores
        picturePerfectScoresLive.value = picturePerfectScores
        allOutDefenceScoresLive.value = allOutDefenceScores
    }

    fun clearScores() {
        picturePerfectScores.clear()
        allOutDefenceScores.clear()
        catastropheScores.clear()
        alienDefenceScores.clear()

    }
    fun getScoresByGame(game: String): Task<String> {
        // Firestore call
        Log.i("TAG7", "HERE")
        val data = hashMapOf(
            "game" to game,
            "push" to true
        )
        return Firebase.functions
            .getHttpsCallable("getScores")
            .call(data)
            .continueWith { task ->
                // This continuation runs on either success or failure, but if the task
                // has failed then result will throw an Exception which will be
                // propagated down.
                    Log.i("TAG8", "HERE")
                    val res = task.result?.data as Map<String, Object>
                    val scores = res.getValue("scores") as List<Map<String, Object>>
                    Log.i("TAG6", scores.size.toString())
                    scores.forEach{score ->
                        Log.i("TAG11", score.getValue("timestamp").toString())
                        val timeStamp = score.getValue("timestamp") as HashMap<String, Long>
                        val seconds = timeStamp.getValue("_seconds")
                        val date = java.text.SimpleDateFormat("yyyy-MM-dd")
                        val time = java.text.SimpleDateFormat("HH:mm:ss'Z'")
                        val formatDate = java.util.Date(seconds as Long * 1000)
                        val formatTime = java.util.Date(seconds as Long * 1000)
                        time.format(formatTime)
                        date.format(formatDate)
                        val formattedTime = formatTime.toString()
                        val formattedDate = formatDate.toString()
                        val scoreVal = score.getValue("score") as Int
                        var obj = Score(formattedDate, formattedTime, scoreVal)

                        if(game == "catastrophe"){
                            if((catastropheScores.find { it.getScore() == obj.getScore() && it.getDate() == obj.getDate() }) == null) {
                                catastropheScores.add(obj)
                            }
                        }
                        else if(game == "picturePerfect"){
                            if((picturePerfectScores.find { it.getScore() == obj.getScore() && it.getDate() == obj.getDate() }) == null) {
                                picturePerfectScores.add(obj)
                            }

                        }
                        else if(game == "alienDefence") {
                            if((alienDefenceScores.find { it.getScore() == obj.getScore() && it.getDate() == obj.getDate() }) == null) {
                                alienDefenceScores.add(obj)
                            }
                        }
                        else if(game == "allOutDefence") {
                            if((allOutDefenceScores.find { it.getScore() == obj.getScore() && it.getDate() == obj.getDate() }) == null) {
                                allOutDefenceScores.add(obj)
                            }
                        }
                    }

                val result = "200"
                result
            }
    }
    fun prepareScoresByGame(game: String): MutableLiveData<MutableList<Score>> {
        getScoresByGame(game).addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.i("FFError12", "HERE")
                val e = task.exception
                println(e)
                if (e is FirebaseFunctionsException) {
                    val details = e.details
                    Log.i("FFError21", "HERE")
                }
            }
        }
        Log.i("TAG15", "HERE")
        if(game == "catastrophe"){
            return catastropheScoresLive
        }
        else if(game == "picturePerfect"){
            return picturePerfectScoresLive
        }
        else if(game == "alienDefence") {
            return alienDefenceScoresLive
        }
        else if(game == "allOutDefence") {
            return allOutDefenceScoresLive
        }
        return MutableLiveData<MutableList<Score>>()
    }

    fun setScoresByUser(game: String, score: Int): Task<String>{
        val data = hashMapOf(
            "game" to game,
            "score" to score,
            "push" to true
        )
        return Firebase.functions
            .getHttpsCallable("updateScore")
            .call(data)
            .continueWith { task ->
                // This continuation runs on either success or failure, but if the task
                // has failed then result will throw an Exception which will be
                // propagated down.
                val result = task.result?.data as String
                result
            }
    }

    fun getRankingsForCatastrophe() {
        // get rankings for catastrope
    }
    fun getRankingsForAlienDefence() {
        // get rankings for catastrope
    }
    fun getRankingsForPicturePerfect() {
        // get rankings for catastrope
    }
    fun getRankingsForAllOutDefence() {
        // get rankings for catastrope
    }
}