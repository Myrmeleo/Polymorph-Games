package com.example.polymorphgames.ui.classes

class Ranking {
    private var rank = 0
    private var userName = ""
    private var gamesPlayed = 0

    constructor(rank: Int, userName: String, games: Int){
        this.rank = rank
        this.gamesPlayed = games
        this.userName = userName
    }

    fun getRank(): Int {
        return this.rank
    }
    fun getUserName(): String {
        return this.userName
    }
    fun getGamesPlayed(): Int {
        return this.gamesPlayed
    }
}