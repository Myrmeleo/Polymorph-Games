package com.example.polymorphgames.catastrophe.drawables

import com.example.polymorphgames.common.DP
import com.example.polymorphgames.common.Polygon
import com.example.polymorphgames.R

class Terrain(
    offset: Pair<DP, DP>,
    dimen: Pair<DP, DP>,
    corners: ArrayList<Pair<DP, DP>> = ArrayList(),
    resourceId: Int,
    descriptor: String,
): Polygon(
    offset = offset,
    dimen = dimen,
    corners = corners,
    resourceId = resourceId,
    descriptor = descriptor
) {
    init {
        if(corners.isEmpty()){
            val topLeft = Pair(first = 0f, second = 0f)
            val topRight = Pair(first = dimen.first, second = 0f)
            val botLeft = Pair(first = 0f, second = dimen.second)
            val botRight = Pair(first = dimen.first, second = dimen.second)

            corners.add(topLeft)
            corners.add(topRight)
            corners.add(botRight)
            corners.add(botLeft)

            // we only call this inside the if condition b/c calculateNorms is called when Polygon
            // is initialized, thus if corners was not empty on initialization then we already have
            // the norms
            calculateNorms()
        }
    }
}