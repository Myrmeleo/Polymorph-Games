package com.example.polymorphgames.catastrophe.game

import com.example.polymorphgames.common.Polygon
import com.example.polymorphgames.catastrophe.drawables.Player
import com.example.polymorphgames.catastrophe.floors.Floor3_Schematicless
import com.example.polymorphgames.catastrophe.floors.FloorCode
import com.example.polymorphgames.common.Collision
import com.example.polymorphgames.common.CustomMath
import com.example.polymorphgames.common.Gravity
import com.example.polymorphgames.common.SensorData
import kotlin.math.abs

class GameLogic private constructor(){
    companion object{
        private val pi: Float = Math.PI.toFloat()

        fun time(): Long{
            return System.currentTimeMillis()
        }

        fun tilt(theta: Float, gyroscopeData: SensorData, deltaTime: Float): Float{
            var ret = theta + gyroscopeData.z * deltaTime
            ret = if (ret > pi / 4) pi / 4 else (if (ret < -pi / 4) -pi / 4 else ret)

            return ret
        }

        fun gravity(player: Player, deltaTime: Float){
            player.velocityY += Gravity.velocity(deltaTime)
        }

        fun walk(player: Player, theta: Float){
            if(!player.airborn && !player.anchored) {
                player.velocityX = -150 * theta
            }
        }

        fun jump(player: Player, accelerometerData: SensorData, theta: Float){
            if(!player.airborn && player.anchored){
                val x = abs(accelerometerData.x)
                val y = abs(accelerometerData.y)
                val z = abs(accelerometerData.z)

                if(x > 1f || y > 1f || z > 1f){
                    player.velocityY = -100f
                    player.velocityX = -150 * theta
                }
            }
        }

        fun applyVelocity(player: Player, deltaTime: Float){
            player.translate(deltaTime)
        }

        fun collision(player: Player, currFloor: ArrayList<Polygon>){
            player.airborn = true
            val ret = Collision.collide(player, currFloor)
            if(ret.collide) {
                val offsetX = CustomMath.round(ret.vector.first, 5)
                val offsetY = CustomMath.round(ret.vector.second, 5)

                player.offset = Pair(
                    player.offset.first + offsetX,
                    player.offset.second + offsetY
                )

                if(ret.floor){
                    player.velocityY = 0f
                    player.airborn = false
                }

                if(ret.wall && player.airborn){
                    player.velocityX = -player.velocityX
                }

                if(ret.ceil && player.airborn){
                    player.velocityY = -player.velocityY
                }
            }
        }

        fun anchor(player: Player) {
            if (player.anchored && !player.airborn) {
                player.velocityX = 0f
            }
        }

        fun floorFunction(gameState: GameState, deltaTime: Float){
            floorCodeHandler(
                gameState.floors[gameState.floorNum].floorFunction(gameState.player, gameState.WIDTH, gameState.HEIGHT),
                gameState,
                deltaTime
            )
        }

        private fun floorCodeHandler(code: FloorCode, gameState: GameState, deltaTime: Float){
            when(code){
                FloorCode.NO_EVENT -> {}
                FloorCode.UP_FLOOR -> {
                    gameState.floorNum += 1

                    gameState.prevFloorCollide = gameState.currFloorCollide
                    gameState.prevFloorNonCollide = gameState.currFloorNonCollide

                    gameState.currFloorCollide = gameState.nextFloorCollide
                    gameState.currFloorNonCollide = gameState.nextFloorNonCollide

                    val floor = gameState.floors[gameState.floorNum + 1]
                    gameState.nextFloorCollide = floor.initCollide(gameState.WIDTH, gameState.HEIGHT)
                    gameState.nextFloorNonCollide = floor.initNonCollide(gameState.WIDTH, gameState.HEIGHT)

                    gameState.player.offset = gameState.player.offset.copy(
                        second = gameState.player.offset.second + gameState.HEIGHT
                    )

                    gameState.floorRecomposeFlag.postValue(!gameState.floorRecomposeFlag.value!!)
                }
                FloorCode.DOWN_FLOOR -> {

                    gameState.floorNum -= 1

                    gameState.nextFloorCollide = gameState.currFloorCollide
                    gameState.nextFloorNonCollide = gameState.currFloorNonCollide

                    gameState.currFloorCollide = gameState.prevFloorCollide
                    gameState.currFloorNonCollide = gameState.prevFloorNonCollide

                    val floor = gameState.floors[gameState.floorNum-1]
                    gameState.prevFloorCollide = floor.initCollide(gameState.WIDTH, gameState.HEIGHT)
                    gameState.prevFloorNonCollide = floor.initNonCollide(gameState.WIDTH, gameState.HEIGHT)

                    gameState.player.offset = gameState.player.offset.copy(
                        second = gameState.player.offset.second - gameState.HEIGHT
                    )

                    gameState.floorRecomposeFlag.postValue(!gameState.floorRecomposeFlag.value!!)
                }
                FloorCode.DOOR -> {
                    if(gameState.player.hasSchematic){
                        gameState.complete = true
                    }
                }
                FloorCode.SCHEMATIC_ACQUIRED -> {
                    gameState.floors[3] = Floor3_Schematicless()
                    gameState.currFloorNonCollide.removeLast()
                    gameState.player.hasSchematic = true
                    gameState.floorRecomposeFlag.postValue(!gameState.floorRecomposeFlag.value!!)
                }
                FloorCode.LOOP_DOWN -> {
                    gameState.player.offset = gameState.player.offset.copy(
                        second = gameState.player.offset.second - gameState.HEIGHT
                    )
                }
                FloorCode.LOOP_UP -> {
                    gameState.player.offset = gameState.player.offset.copy(
                        second = gameState.player.offset.second + gameState.HEIGHT
                    )
                }
            }
        }
    }
}