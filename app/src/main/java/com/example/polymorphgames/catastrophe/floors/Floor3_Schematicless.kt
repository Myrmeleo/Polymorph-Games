package com.example.polymorphgames.catastrophe.floors

import com.example.polymorphgames.R
import com.example.polymorphgames.common.Polygon
import com.example.polymorphgames.common.Drawable
import com.example.polymorphgames.common.DP
import kotlin.collections.ArrayList

class Floor3_Schematicless: Floor() {
    override fun initCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Polygon> {
        return Floor3().initCollide(WIDTH, HEIGHT)
    }

    override fun initNonCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Drawable> {
        return super.initFillBackground(WIDTH, HEIGHT, R.drawable.md_8)
    }

}