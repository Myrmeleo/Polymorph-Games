package com.example.polymorphgames.catastrophe.game

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.pm.ActivityInfo
import android.graphics.Paint
import androidx.compose.foundation.background
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.polymorphgames.catastrophe.drawables.Player
import com.example.polymorphgames.common.Drawable
import com.example.polymorphgames.common.Polygon
import com.example.polymorphgames.ui.viewModels.ScoresViewModel

@Composable
fun CatastropheGameLaunch(flagState: MutableState<Boolean>, scoresViewModel: ScoresViewModel){
    LockScreenOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
    val viewModel = GameViewModel(LocalContext.current, LocalConfiguration.current)
    ScreenSelection(viewModel, flagState, scoresViewModel)
}

@Composable
fun ScreenSelection(viewModel: GameViewModel, flagState: MutableState<Boolean>, scoresViewModel: ScoresViewModel) {
    val recompose: Boolean? by viewModel.screenSelect.recompose.observeAsState()
    recompose // need to use in order for recomposition to occur

    if(viewModel.screenSelect.end){
        GameEnd(viewModel, scoresViewModel)
    }
    else if(viewModel.screenSelect.menu){
        GameMenu(viewModel.screenSelect, flagState)
    }
    else if(viewModel.screenSelect.intro){
        GameIntro(viewModel)
    }
    else if(viewModel.screenSelect.game){
        GameScreen(viewModel)
    }
}

@Composable
fun GameMenu(screenSelect: ScreenSelect, flagState: MutableState<Boolean>){

    Box(modifier = Modifier.fillMaxSize()) {
        Text(
            text="Episode 1: Catastrophe",
            modifier = Modifier.align(Alignment.TopCenter)
        )
        Button(
            onClick = {
                screenSelect.menu = false
                screenSelect.game = true
            },
            modifier = Modifier.align(Alignment.CenterStart)
        ){
            Text("START")
        }
        
        Button(
            onClick = { flagState.value = false },
            modifier = Modifier.align(Alignment.CenterEnd)
        ){
            Text(text = "EXIT")
        }
    }
}

@Composable
fun GameScreen(viewModel: GameViewModel){
    Box(modifier = Modifier.fillMaxSize()) {
        BackgroundComponent(
            gameState = viewModel.gameState,
            flag = viewModel.gameState.floorRecomposeFlag
        )
        BackgroundComponent(
            gameState = viewModel.gameState,
            flag = viewModel.gameState.floorRecomposeFlag
        )
        PlayerComponent(
            player = viewModel.gameState.player,
            flag = viewModel.gameState.playerRecomposeFlag
        )
        AnchorButton(
            player = viewModel.gameState.player,
            modifier = Modifier.align(Alignment.BottomEnd)
        )
        ExitButton(
            screenSelect = viewModel.screenSelect,
            modifier = Modifier.align(Alignment.TopStart).alpha(0.7f)
        )
    }
}

@Composable
fun GameIntro(viewModel: GameViewModel){
    Box(modifier = Modifier.fillMaxSize()){
        Text(
            text = "The year is 2100, mankind is faced with the threat of an alien invasion. " +
                    "To combat this a special operation squad, codenamed Scepter, is tasked with " +
                    "retrieving mecha schematics developed by an ancient race that lived on earth " +
                    "long ago. Your mission is to navigate to the top of an ancient ruin in order t" +
                    "o retrieve the schematics...",
            modifier = Modifier
                .align(Alignment.Center)
                .padding(5.dp)
        )

        Button(
            onClick = {viewModel.screenSelect.intro = false},
            modifier = Modifier.align(Alignment.BottomEnd)
        ){
            Text("Continue")
        }
    }
}

@Composable
fun GameEnd(viewModel: GameViewModel, scoresViewModel: ScoresViewModel){
    scoresViewModel.setScoresByUser("catastrophe", viewModel.gameState.time.toInt())
    scoresViewModel.getScoresByGame("catastrophe")
    Box(modifier = Modifier.fillMaxSize()){
        Text(
            text = "After retrieving schematics you setup camp outside the tower for the night. " +
                    "Next morning you begin the journey back to Scepter HQ",
            modifier = Modifier
                .align(Alignment.Center)
                .padding(5.dp)
        )

        Button(
            onClick = {
                viewModel.screenSelect.end = false
            },
            modifier = Modifier.align(Alignment.BottomEnd)
        ){
            Text("Continue")
        }
    }
}

@Composable
fun PlayerComponent(player: Player, flag: MutableLiveData<Boolean?>){
    val redraw: Boolean? by flag.observeAsState()

    player.Draw(redraw)
}

@Composable
fun BackgroundComponent(gameState: GameState, flag: MutableLiveData<Boolean?>){
    val redraw: Boolean? by flag.observeAsState()

    gameState.currFloorNonCollide.forEach {
       it.Draw(redrawFlag = redraw)
    }

    gameState.currFloorCollide.forEach {
        it.Draw(redrawFlag = redraw)
    }
}

@Composable
fun AnchorButton(player: Player, modifier: Modifier){
    val interactionSource = remember { MutableInteractionSource()}
    player.anchored = interactionSource.collectIsPressedAsState().value


    Button(
        onClick = {},
        interactionSource = interactionSource,
        modifier = modifier.alpha(0.7f)
    ){
        Text("anchor")
    }
}

@Composable
fun ExitButton(screenSelect: ScreenSelect, modifier: Modifier){
    Button(
        onClick = {
            screenSelect.game = false
            screenSelect.menu = true
        },
        modifier = modifier
    ){
        Text("X")
    }
}


@Composable
fun LockScreenOrientation(orientation: Int) {
    val context = LocalContext.current
    DisposableEffect(Unit) {
        val activity = context.findActivity()
        val originalOrientation = activity!!.requestedOrientation
        activity.requestedOrientation = orientation
        onDispose {
            activity.requestedOrientation = originalOrientation
        }
    }
}

// Based on: https://stackoverflow.com/questions/64675386/how-to-get-activity-in-compose
fun Context.findActivity(): Activity? {
    if(this is Activity){
        return this
    }

    else if(this is ContextWrapper){
        return baseContext.findActivity()
    }

    else
        return null
}
