package com.example.polymorphgames.catastrophe.game

import android.content.Context
import android.content.res.Configuration
import android.hardware.Sensor
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.polymorphgames.common.SensorData
import com.example.polymorphgames.common.SensorDataManager
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class GameViewModel(context: Context, configuration: Configuration): ViewModel(){
    private object PrivateGameViewModelConstants{
        const val MS_PER_FRAME: Long = 17
    }

    private var time: Long = GameLogic.time()
    private var theta: Float = 0f
    private var accelerometerData: SensorData = SensorData()
    private var gyroscopeData: SensorData = SensorData()

    var gameState: GameState
    var screenSelect = ScreenSelect()

    init{
        gameState =  GameState(
            configuration.screenWidthDp / 1f,
            configuration.screenHeightDp / 1f
        )

        SensorDataManager.register(context, Sensor.TYPE_LINEAR_ACCELERATION, accelerometerData)
        SensorDataManager.register(context, Sensor.TYPE_GYROSCOPE, gyroscopeData)

        viewModelScope.launch { gameLoop(context) }
    }

    private suspend fun gameLoop(context: Context){
        if(screenSelect.menu) {
            while (screenSelect.menu) {delay(PrivateGameViewModelConstants.MS_PER_FRAME)}
            exitMenu()
        }

        if(screenSelect.intro){
            while(screenSelect.intro){delay(PrivateGameViewModelConstants.MS_PER_FRAME)}
            exitIntro()
        }

        if(screenSelect.game) {
            while (screenSelect.game && !gameState.complete) {
                update()
                redrawPlayer()
                delay(PrivateGameViewModelConstants.MS_PER_FRAME)
            }

            if(gameState.complete){
                screenSelect.end = true
            }

            exitGame()
        }

        if(screenSelect.end){
            while(screenSelect.end){delay(PrivateGameViewModelConstants.MS_PER_FRAME)}
            exitEnd()
        }

        gameLoop(context)
    }

    private fun update(){
        val newTime = GameLogic.time()
        val deltaTime = (newTime - time) / 1000f
        time = newTime

        // too much time has passed between frames skip to avoid errors
        if(deltaTime > 0.099f) {
            return
        }

        gameState.time += deltaTime
        theta = GameLogic.tilt(theta, gyroscopeData, deltaTime)
        GameLogic.gravity(gameState.player, deltaTime)
        GameLogic.anchor(gameState.player)


        GameLogic.walk(gameState.player, theta)
        GameLogic.jump(gameState.player, accelerometerData, theta)
        GameLogic.floorFunction(gameState, deltaTime)
        GameLogic.applyVelocity(gameState.player, deltaTime)


        GameLogic.collision(gameState.player, gameState.currFloorCollide)
    }

    private fun redrawPlayer(){gameState.playerRecomposeFlag.postValue(!gameState.playerRecomposeFlag.value!!)}

    private fun exitMenu(){
        screenSelect.menu = false
        screenSelect.game = true
        screenSelect.recompose.postValue(!screenSelect.recompose.value!!)
    }
    private fun exitIntro(){
        screenSelect.intro = false
        screenSelect.recompose.postValue(!screenSelect.recompose.value!!)
    }

    private fun exitGame(){
        screenSelect.game = false
        screenSelect.menu = true
        screenSelect.recompose.postValue(!screenSelect.recompose.value!!)
    }

    private fun exitEnd(){
        screenSelect.menu = true
        screenSelect.game = false
        screenSelect.intro = true
        screenSelect.end = false
        screenSelect.recompose.postValue(!screenSelect.recompose.value!!)
    }
}