package com.example.polymorphgames.catastrophe.game

import androidx.lifecycle.MutableLiveData
import com.example.polymorphgames.common.Polygon
import com.example.polymorphgames.common.Drawable
import com.example.polymorphgames.catastrophe.drawables.Player
import com.example.polymorphgames.catastrophe.floors.*
import com.example.polymorphgames.common.DP

data class GameState(
    val WIDTH: DP,
    val HEIGHT: DP,

    var complete: Boolean = false,

    val fileName: String = "GameState.txt",

    val player: Player = Player(offset = Pair(10f, 10f)),
    var playerRecomposeFlag: MutableLiveData<Boolean?> = MutableLiveData<Boolean?>(false),

    var floorNum: Int = 3,
    val floors: ArrayList<Floor> = arrayListOf(
        EmptyFloor(),
        Floor1(),
        Floor2(),
        Floor3(),
        EmptyFloor(),
    ),

    var prevFloorCollide: ArrayList<Polygon> = floors[floorNum-1].initCollide(WIDTH, HEIGHT),
    var currFloorCollide: ArrayList<Polygon> = floors[floorNum].initCollide(WIDTH, HEIGHT),
    var nextFloorCollide: ArrayList<Polygon> = floors[floorNum+1].initCollide(WIDTH, HEIGHT),
    var prevFloorNonCollide: ArrayList<Drawable> = floors[floorNum-1].initNonCollide(WIDTH, HEIGHT),
    var currFloorNonCollide: ArrayList<Drawable> = floors[floorNum].initNonCollide(WIDTH, HEIGHT),
    var nextFloorNonCollide: ArrayList<Drawable> = floors[floorNum+1].initNonCollide(WIDTH, HEIGHT),
    var floorRecomposeFlag: MutableLiveData<Boolean?> = MutableLiveData<Boolean?>(false),

    var time: Double = 0.0,
    var gameStart: Boolean = true,
    var gameEnd: Boolean = false
)