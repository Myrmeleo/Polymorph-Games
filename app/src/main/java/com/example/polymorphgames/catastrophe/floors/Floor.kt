package com.example.polymorphgames.catastrophe.floors

import com.example.polymorphgames.R
import com.example.polymorphgames.catastrophe.drawables.NoCollide
import com.example.polymorphgames.common.Polygon
import com.example.polymorphgames.common.Drawable
import com.example.polymorphgames.catastrophe.drawables.Player
import com.example.polymorphgames.catastrophe.drawables.Terrain
import com.example.polymorphgames.common.DP

abstract class Floor protected constructor(){
    open fun initCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Polygon>{
        val ret = ArrayList<Polygon>()

        for(i in 0..15){
            ret.add(
                Terrain(
                    offset = Pair(0f, (i/15.5f)*HEIGHT),
                    dimen = Pair(0.05f*WIDTH, (1/15f)*HEIGHT),
                    resourceId = R.drawable.md_1,
                    descriptor = "Left Wall"
                )
            )

            ret.add(
                Terrain(
                    offset = Pair(0.95f*WIDTH, (i/15.5f)*HEIGHT),
                    dimen = Pair(0.05f*WIDTH, (1/15f)*HEIGHT),
                    resourceId = R.drawable.md_1,
                    descriptor = "Right Wall"
                )
            )
        }

        return ret
    }

    abstract fun initNonCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Drawable>

    open fun floorFunction(player: Player, WIDTH: DP, HEIGHT: DP): FloorCode {
        if(player.offset.second + player.dimen.second/2 < 0f)
            return FloorCode.UP_FLOOR

        if(player.offset.second + player.dimen.second/2 >= HEIGHT)
            return FloorCode.DOWN_FLOOR

        return FloorCode.NO_EVENT
    }

    protected fun initFillBackground(WIDTH: DP, HEIGHT: DP, recourseId: Int): ArrayList<Drawable> {
        val background = ArrayList<Drawable>()

        val tilesPerWidth = 5
        for(widthCount in 0 until tilesPerWidth){
            for(heightCount in 0 until ((HEIGHT / (WIDTH / tilesPerWidth)) + 0.5f).toInt()+1){
                background.add(
                    NoCollide(
                        offset = Pair(
                            (WIDTH / tilesPerWidth)*widthCount,
                            (WIDTH / tilesPerWidth) * heightCount
                        ),
                        dimen = Pair(WIDTH / tilesPerWidth, WIDTH / tilesPerWidth),
                        resourceId = recourseId,
                        descriptor = "background"
                    )
                )
            }
        }

        return background
    }
}