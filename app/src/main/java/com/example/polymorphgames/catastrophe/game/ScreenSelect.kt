package com.example.polymorphgames.catastrophe.game

import androidx.lifecycle.MutableLiveData

data class ScreenSelect(
    val fileName: String = "ScreenSelect.txt",

    var menu: Boolean = true,
    var game: Boolean = false,
    var intro: Boolean = true,
    var end: Boolean = false,


    var recompose: MutableLiveData<Boolean?> = MutableLiveData(true)
)
