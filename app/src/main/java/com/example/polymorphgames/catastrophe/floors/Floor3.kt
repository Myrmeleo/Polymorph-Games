package com.example.polymorphgames.catastrophe.floors

import com.example.polymorphgames.R
import com.example.polymorphgames.catastrophe.drawables.NoCollide
import com.example.polymorphgames.common.Polygon
import com.example.polymorphgames.common.Drawable
import com.example.polymorphgames.catastrophe.drawables.Player
import com.example.polymorphgames.catastrophe.drawables.Terrain
import com.example.polymorphgames.common.DP
import kotlin.collections.ArrayList

open class Floor3: Floor(){
    override fun initCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Polygon> {
        val ret = super.initCollide(WIDTH, HEIGHT)

        val triangleCorners = ArrayList<Pair<Float, Float>>()
        triangleCorners.add(Pair(0.3f*WIDTH, 0.4f*HEIGHT))
        triangleCorners.add(Pair(0f, 0.4f*HEIGHT))
        triangleCorners.add(Pair(0f, 0f))

        for(i in 0 .. ((WIDTH / (HEIGHT / 15)) + 0.5f).toInt()){
            ret.add(
                Terrain(
                    offset = Pair(i * HEIGHT / 15, 0f),
                    dimen = Pair(HEIGHT/15, 0.05f*WIDTH),
                    resourceId = R.drawable.md_3,
                    descriptor = "ceiling"
                )
            )
        }

        ret.add(
            Terrain(
                offset = Pair(0.72f * WIDTH, 0.485f*WIDTH),
                dimen = Pair(0.06f*WIDTH, HEIGHT - 0.485f*WIDTH),
                resourceId = R.drawable.md_2,
                descriptor = "Wall"
            )
        )

        for(i in 0..5){
            ret.add(
                Terrain(
                    offset = Pair(0.9f*WIDTH, -i * 0.13f*HEIGHT + 0.98f*HEIGHT),
                    dimen = Pair(0.05f*WIDTH, 0.02f*HEIGHT),
                    resourceId = R.drawable.md_6,
                    descriptor = "scaffold"
                )
            )
        }

        ret.add(
            Terrain(
                offset = Pair(0.05f*WIDTH, 0.3f*HEIGHT),
                dimen = Pair(0.05f*WIDTH, 0.4f*HEIGHT),
                resourceId = R.drawable.md_2,
                descriptor = "goal scaffold"
            )
        )

        return ret
    }

    override fun initNonCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Drawable> {
        val background = initFillBackground(WIDTH, HEIGHT, R.drawable.md_8)
        background.add(
            NoCollide(
                offset = Pair(0.05f*WIDTH, 0.3f*HEIGHT - 0.05f*WIDTH),
                dimen = Pair(0.05f*WIDTH, 0.05f*WIDTH),
                resourceId = R.drawable.md_schematics,
                descriptor = "schematic"
            )
        )
        return background
    }

    override fun floorFunction(player: Player, WIDTH: DP, HEIGHT: DP): FloorCode {
        var code = super.floorFunction(player, WIDTH, HEIGHT)
        if(!player.hasSchematic && code == FloorCode.NO_EVENT){
            val offset = player.offset
            val dimen = player.dimen
            if(((offset.first >= 0.05f*WIDTH && offset.first <= 0.1f*WIDTH) ||
                (offset.first + dimen.first >= 0.05f*WIDTH && offset.first + dimen.first <= 0.1f*WIDTH)) &&
               ((offset.second >= 0.3f*HEIGHT - 0.05*WIDTH && offset.second <= 0.3f*HEIGHT) ||
                (offset.second + dimen.second >= 0.3f*HEIGHT - 0.05*WIDTH && offset.second + dimen.second <= 0.3f*HEIGHT))){
                code = FloorCode.SCHEMATIC_ACQUIRED
            }
        }

        return code
    }
}