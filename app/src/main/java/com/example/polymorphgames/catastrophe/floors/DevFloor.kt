package com.example.polymorphgames.catastrophe.floors

import com.example.polymorphgames.R
import com.example.polymorphgames.catastrophe.drawables.Player
import com.example.polymorphgames.common.Polygon
import com.example.polymorphgames.common.Drawable
import com.example.polymorphgames.catastrophe.drawables.Terrain
import com.example.polymorphgames.common.DP
import java.util.*
import kotlin.collections.ArrayList

class DevFloor(): Floor(){
    override fun initCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Polygon> {
        val ret = ArrayList<Polygon>()

        val triangleCorners = ArrayList<Pair<Float, Float>>()
        triangleCorners.addAll(Arrays.asList(
            Pair(0f, 20f),
            Pair(20f, 0f),
            Pair(40f, 20f)
        ))

        Collections.addAll(
            ret,
            Terrain(
                offset = Pair(405f, 0f),
                dimen = Pair(100f, 600f),
                resourceId = R.drawable.md_1,
                descriptor = "dev"
            ),
            Terrain(
                offset = Pair(180f, 540f),
                dimen = Pair(220f, 10f),
                resourceId = R.drawable.md_1,
                descriptor = "dev"
            ),
            Terrain(
                offset = Pair(0f, 0f),
                dimen = Pair(100f, 1000f),
                resourceId = R.drawable.md_1,
                descriptor = "dev"
            ),
            Terrain(
                offset = Pair(235f, 421f),
                dimen = Pair(40f, 20f),
                corners = triangleCorners,
                resourceId = R.drawable.md_2,
                descriptor = "dev"
            ),
            Terrain(
                offset = Pair(288f, 400f),
                dimen = Pair(40f, 20f),
                corners = triangleCorners,
                resourceId = R.drawable.md_2,
                descriptor = "dev"
            )
        )
        return ret
    }

    override fun initNonCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Drawable> {
        return ArrayList()
    }

    override fun floorFunction(player: Player, WIDTH: DP, HEIGHT: DP): FloorCode {
        var code = super.floorFunction(player, WIDTH, HEIGHT)
        if(code == FloorCode.DOWN_FLOOR){
            code = FloorCode.LOOP_UP
        }
        else if(code == FloorCode.UP_FLOOR){
            code = FloorCode.LOOP_DOWN
        }
        return code
    }
}