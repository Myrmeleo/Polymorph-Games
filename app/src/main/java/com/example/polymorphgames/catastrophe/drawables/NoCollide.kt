package com.example.polymorphgames.catastrophe.drawables

import com.example.polymorphgames.common.DP
import com.example.polymorphgames.common.Drawable

class NoCollide(
    offset: Pair<DP, DP>,
    dimen: Pair<DP, DP>,
    resourceId: Int,
    descriptor: String
): Drawable(
    offset = offset,
    dimen = dimen,
    resourceId = resourceId,
    descriptor = descriptor
) {
}