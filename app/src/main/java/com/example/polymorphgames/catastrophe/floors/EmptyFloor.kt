package com.example.polymorphgames.catastrophe.floors

import com.example.polymorphgames.R
import com.example.polymorphgames.common.Polygon
import com.example.polymorphgames.common.Drawable
import com.example.polymorphgames.catastrophe.drawables.Player
import com.example.polymorphgames.catastrophe.drawables.Terrain
import com.example.polymorphgames.common.DP
import java.util.*
import kotlin.collections.ArrayList

class EmptyFloor(): Floor(){
    override fun initCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Polygon> {
        return ArrayList()
    }

    override fun initNonCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Drawable> {
        return ArrayList()
    }

}