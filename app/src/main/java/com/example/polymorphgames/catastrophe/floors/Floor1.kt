package com.example.polymorphgames.catastrophe.floors

import com.example.polymorphgames.R
import com.example.polymorphgames.catastrophe.drawables.NoCollide
import com.example.polymorphgames.common.Polygon
import com.example.polymorphgames.common.Drawable
import com.example.polymorphgames.catastrophe.drawables.Player
import com.example.polymorphgames.catastrophe.drawables.Terrain
import com.example.polymorphgames.common.DP
import kotlin.collections.ArrayList

class Floor1: Floor(){
    override fun initCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Polygon> {
        val ret = super.initCollide(WIDTH, HEIGHT)

        val triangleCorners = ArrayList<Pair<Float, Float>>()
        triangleCorners.add(Pair(0f, 0.2f*HEIGHT))
        triangleCorners.add(Pair(0.2f*HEIGHT, 0f))
        triangleCorners.add(Pair(0.2f*HEIGHT, 0.2f*HEIGHT))

        for(i in 0..9){
            ret.add(
                Terrain(
                    offset = Pair((i/10f)*WIDTH, 0.95f*HEIGHT),
                    dimen = Pair(0.1f*WIDTH, 0.05f*HEIGHT),
                    resourceId = R.drawable.md_7,
                    descriptor = "Floor"
                )
            )
        }

        ret.add( 0,
            Terrain(
                offset = Pair(0.55f*WIDTH, 0.95f*HEIGHT - 0.2f*HEIGHT),
                dimen = Pair(0.2f*HEIGHT, 0.2f*HEIGHT),
                corners = triangleCorners,
                resourceId = R.drawable.md_t1,
                descriptor = "railing"
            )
        )

        ret.add( 0,
            Terrain(
                offset = Pair(0.55f*WIDTH + 0.2f*HEIGHT + 3f, 0.95f*HEIGHT - 0.2f*HEIGHT),
                dimen = Pair(WIDTH - (0.55f*WIDTH + 0.2f*HEIGHT), 0.1f*HEIGHT),
                resourceId = R.drawable.md_4,
                descriptor = "scaffold"
            )
        )

        ret.add(0,
            Terrain(
                offset = Pair(0.05f*WIDTH, 0.7f*HEIGHT),
                dimen = Pair(0.5f*WIDTH, 0.04f*HEIGHT),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(0,
            Terrain(
                offset = Pair(0.25f*WIDTH, 0.57f*HEIGHT),
                dimen = Pair(0.05f*WIDTH, 0.05f*WIDTH),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(0,
            Terrain(
                offset = Pair(0.35f*WIDTH, 0.47f*HEIGHT),
                dimen = Pair(0.05f*WIDTH, 0.05f*WIDTH),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(0,
            Terrain(
                offset = Pair(0.45f*WIDTH, 0.37f*HEIGHT),
                dimen = Pair(0.05f*WIDTH, 0.05f*WIDTH),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(0,
            Terrain(
                offset = Pair(0.55f*WIDTH, 0.27f*HEIGHT),
                dimen = Pair(0.05f*WIDTH, 0.05f*WIDTH),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(0,
            Terrain(
                offset = Pair(0.65f*WIDTH, 0.17f*HEIGHT),
                dimen = Pair(0.05f*WIDTH, 0.05f*WIDTH),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(0,
            Terrain(
                offset = Pair(0.75f*WIDTH, 0.11f*HEIGHT),
                dimen = Pair(0.05f*WIDTH, 0.05f*WIDTH),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(0,
            Terrain(
                offset = Pair(0.85f*WIDTH, 0.05f*HEIGHT),
                dimen = Pair(0.05f*WIDTH, 0.05f*WIDTH),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        return ret
    }

    override fun initNonCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Drawable> {
        val background = initFillBackground(WIDTH, HEIGHT, R.drawable.md_8)

        background.add(
            NoCollide(
                offset = Pair(0.1f*WIDTH, 0.85f*HEIGHT),
                dimen = Pair(0.05f*HEIGHT, 0.1f*HEIGHT),
                resourceId = R.drawable.md_door,
                descriptor = "Door"
            )
        )

        return background
    }

    override fun floorFunction(player: Player, WIDTH: DP, HEIGHT: DP): FloorCode {
        var code = super.floorFunction(player, WIDTH, HEIGHT)
        if(code == FloorCode.NO_EVENT){
            val offset = player.offset
            if(offset.first >= 0.1f*WIDTH && offset.first <= 0.1*WIDTH + 0.05*HEIGHT &&
               offset.second >= 0.85f*HEIGHT && offset.second <= 0.95f*HEIGHT)
            code = FloorCode.DOOR
        }

        return code
    }
}