package com.example.polymorphgames.catastrophe.drawables

import androidx.compose.runtime.Composable
import com.example.polymorphgames.R
import com.example.polymorphgames.common.DP
import com.example.polymorphgames.common.Polygon


class Player(
    offset: Pair<DP, DP> = Pair(600f, 140f),
    dimen: Pair<DP, DP> = Pair(25f, 50f),
    corners: ArrayList<Pair<DP, DP>> = ArrayList(),
    resourceId: Int = R.drawable.md_mc_2,
    descriptor: String = "the player",

    var hasSchematic: Boolean = false,
    var airborn: Boolean = true,
    var anchored: Boolean = false,
    var velocityX: DP = 0f,
    var velocityY: DP = 0f,
    var velocityYMax: DP = 400f,
    var rightDirection: Boolean = false,
    var walkFrame: Int = 1
):
Polygon(
    offset = offset,
    dimen = dimen,
    corners = corners,
    resourceId = resourceId,
    descriptor = descriptor
){

    init{
        if(corners.isEmpty()) {
            val topLeft = Pair(first = 0f, second = 0f)
            val topRight = Pair(first = dimen.first, second = 0f)
            val botLeft = Pair(first = 0f, second = dimen.second)
            val botRight = Pair(first = dimen.first, second = dimen.second)

            corners.add(topLeft)
            corners.add(topRight)
            corners.add(botRight)
            corners.add(botLeft)

            calculateNorms()
        }
    }

    @Composable
    override fun Draw(redrawFlag: Boolean?){
        if(velocityY == 0f){
            if(velocityX < 0f){
                if(walkFrame / 10f < 1){
                    resourceId = R.drawable.md_mc_3
                }
                else if(walkFrame / 10f < 2){
                    resourceId = R.drawable.md_mc_1
                }
                else if(walkFrame / 10f < 3){
                    resourceId = R.drawable.md_mc_5
                }
                else if(walkFrame / 10f < 4){
                    resourceId = R.drawable.md_mc_1
                    walkFrame = -1
                }

                ++walkFrame
                rightDirection = false
            }
            else if(velocityX > 0f){
                if(walkFrame / 10f < 1){
                    resourceId = R.drawable.md_mc_4
                }
                else if(walkFrame / 10f < 2){
                    resourceId = R.drawable.md_mc_2
                }
                else if(walkFrame / 10f < 3){
                    resourceId = R.drawable.md_mc_6
                }
                else if(walkFrame / 10f < 4){
                    resourceId = R.drawable.md_mc_2
                    walkFrame = -1
                }

                ++walkFrame
                rightDirection = true
            }
            else{
                resourceId = if (rightDirection) R.drawable.md_mc_2 else R.drawable.md_mc_1
            }
        }

        super.Draw(redrawFlag)
    }

    fun translate(deltaTime: Float){
        if(velocityY > velocityYMax) velocityY = velocityYMax

        super.translate(
            velocityX * deltaTime,
            velocityY * deltaTime
        )
    }
}