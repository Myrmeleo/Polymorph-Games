package com.example.polymorphgames.catastrophe.floors

import com.example.polymorphgames.R
import com.example.polymorphgames.common.Polygon
import com.example.polymorphgames.common.Drawable
import com.example.polymorphgames.catastrophe.drawables.Terrain
import com.example.polymorphgames.common.DP
import kotlin.collections.ArrayList

class Floor2 : Floor(){
    override fun initCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Polygon> {
        val ret = super.initCollide(WIDTH, HEIGHT)

        val triangleCorners1 = ArrayList<Pair<Float, Float>>()
        triangleCorners1.add(Pair(0f, 0f))
        triangleCorners1.add(Pair(0.1f*WIDTH, 0f))
        triangleCorners1.add(Pair(0.1f*WIDTH, 0.05f*HEIGHT))

        val triangleCorners2 = ArrayList<Pair<Float, Float>>()
        triangleCorners2.add(Pair(0f, 0.05f*HEIGHT))
        triangleCorners2.add(Pair(0.1f*WIDTH, 0f))
        triangleCorners2.add(Pair(0.1f*WIDTH, 0.05f*HEIGHT))

        ret.add(0,
            Terrain(
                offset = Pair(0.6f*WIDTH, 0.95f*HEIGHT),
                dimen = Pair(0.2f*WIDTH, 0.05f*HEIGHT),
                resourceId = R.drawable.md_4,
                descriptor = "scaffold"
            )
        )

        ret.add(0,
            Terrain(
                offset = Pair(0.3f*WIDTH, 0.85f*HEIGHT),
                dimen = Pair(0.05f*WIDTH, 0.05f*HEIGHT),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(
            Terrain(
                offset = Pair(0.05f*WIDTH, 0.75f*HEIGHT),
                dimen = Pair(0.04f*WIDTH, 0.1f*HEIGHT),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(
            Terrain(
                offset = Pair(0.35f*WIDTH, 0.65f*HEIGHT),
                corners = triangleCorners1,
                dimen = Pair(0.1f*WIDTH, 0.05f*HEIGHT),
                resourceId = R.drawable.md_t4,
                descriptor = "slope"
            )
        )

        ret.add(
            Terrain(
                offset = Pair(0.50f*WIDTH, 0.55f*HEIGHT),
                corners = triangleCorners2,
                dimen = Pair(0.1f*WIDTH, 0.05f*HEIGHT),
                resourceId = R.drawable.md_t1,
                descriptor = "slope"
            )
        )

        ret.add(
            Terrain(
                offset = Pair(0.7f*WIDTH, 0.45f*HEIGHT),
                corners = triangleCorners2,
                dimen = Pair(0.1f*WIDTH, 0.05f*HEIGHT),
                resourceId = R.drawable.md_t1,
                descriptor = "slope"
            )
        )

        ret.add(
            Terrain(
                offset = Pair(0.91f*WIDTH, 0.33f*HEIGHT),
                dimen = Pair(0.04f*WIDTH, 0.1f*HEIGHT),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(
            Terrain(
                offset = Pair(0.71f*WIDTH, 0.2f*HEIGHT),
                dimen = Pair(0.05f*WIDTH, 0.05f*HEIGHT),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(
            Terrain(
                offset = Pair(0.6f*WIDTH, 0.09f*HEIGHT),
                dimen = Pair(0.05f*WIDTH, 0.05f*HEIGHT),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        ret.add(
            Terrain(
                offset = Pair(0.8f*WIDTH, 0.09f*HEIGHT),
                dimen = Pair(0.2f*WIDTH, 0.025f*HEIGHT),
                resourceId = R.drawable.md_5,
                descriptor = "scaffold"
            )
        )

        return ret
    }

    override fun initNonCollide(WIDTH: DP, HEIGHT: DP): ArrayList<Drawable> {
        return initFillBackground(WIDTH, HEIGHT, R.drawable.md_8)
    }
}