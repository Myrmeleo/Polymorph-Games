package com.example.polymorphgames.catastrophe.floors

enum class FloorCode(val value: Int) {
    NO_EVENT(0),
    UP_FLOOR(1),
    DOWN_FLOOR(2),
    SCHEMATIC_ACQUIRED(3),
    DOOR(4),
    LOOP_UP(5),
    LOOP_DOWN(6)
}