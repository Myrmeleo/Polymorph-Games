package com.example.polymorphgames

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent

import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.fillMaxSize
//import com.example.polymorphgames.catastrophe.sections.game.CatastropheGameLaunch
import com.example.polymorphgames.catastrophe.game.CatastropheGameLaunch
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.lifecycle.ViewModelProvider
import com.example.polymorphgames.alloutdefense.AODActivity
import com.example.polymorphgames.dhjava.AlienDefenceActivity
import com.example.polymorphgames.picturePerfect.StartScreenActivity
import com.example.polymorphgames.ui.classes.StateManager
import com.example.polymorphgames.ui.screens.LoginScreen
import com.example.polymorphgames.ui.theme.PolymorphGamesTheme
import com.example.polymorphgames.ui.screens.MainScreen
import com.example.polymorphgames.ui.viewModels.RatingsViewModel
import com.example.polymorphgames.ui.viewModels.ScoresViewModel
import com.example.polymorphgames.ui.viewModels.UserViewModel
import com.google.android.gms.common.internal.Constants
import com.google.firebase.functions.FirebaseFunctionsException
import java.io.Serializable


var stateManager = StateManager()
private lateinit var userViewModel: UserViewModel
private lateinit var ratingsViewModel: RatingsViewModel
private lateinit var scoresViewModel: ScoresViewModel
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userViewModel = ViewModelProvider(this)[UserViewModel::class.java]
        ratingsViewModel = ViewModelProvider(this)[RatingsViewModel::class.java]
        scoresViewModel = ViewModelProvider(this)[ScoresViewModel::class.java]
        setContent {
            PolymorphGamesTheme(isSystemInDarkTheme(), userViewModel.colorBlindSetting.value) {
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background) {

                    if(userViewModel.isLoggedIn.value){
                        if (stateManager.catastrophe.value) {
                            CatastropheGameLaunch(stateManager.catastrophe, scoresViewModel)
                        }
                        else if (stateManager.alienDefence.value) {
                            val intent = Intent(this, AlienDefenceActivity::class.java)
                            startActivity(intent)
                            stateManager.alienDefence.value = false
                        }
                        else if (stateManager.picturePerfect.value) {
                            val intent = Intent(this, StartScreenActivity::class.java)
                            startActivity(intent)
                            stateManager.picturePerfect.value = false

                        }
                        else if (stateManager.allOutDefence.value) {
                            val intent = Intent(this, AODActivity::class.java)
                            startActivity(intent)
                            stateManager.allOutDefence.value = false
                        }
                        else {
                            MainScreen(
                                stateManager,
                                logout = {
                                    userViewModel.signOut()
                                    scoresViewModel.clearScores()
                                },
                                userViewModel,
                                ratingsViewModel,
                                scoresViewModel,
                                this
                            )
                        }
                    }
                    else {
                        LoginScreen(userViewModel)
                    }

                }
            }
        }
    }
}