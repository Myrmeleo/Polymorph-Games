# Polymorph Games  
CS 446 Software Design and Architectures Final Project  
By: Michael DaRocha, Ricky Li, Kawalpreet Deol, Shanay Save, Shadman Hassan, Leo Huang  
IDs: mldaroch, rjlli, k4deol, ssave, shassan, h277huan  

The project is a collection of games. Each game is connected to a central library. The games are independent of each other.
